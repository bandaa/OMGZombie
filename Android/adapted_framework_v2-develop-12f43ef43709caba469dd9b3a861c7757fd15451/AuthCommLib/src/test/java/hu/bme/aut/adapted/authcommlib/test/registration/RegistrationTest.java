package hu.bme.aut.adapted.authcommlib.test.registration;

/*
@RunWith(RobolectricDaggerTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 24)
public class RegistrationTest {

    private RegistrationPresenter registrationPresenter;
    private Settings settings;
    private RegistrationScreen registrationScreen;

    @Before
    public void setup() throws Exception {
        registrationPresenter = new RegistrationPresenter();
        setTestInjector(registrationPresenter);
        Store.init(RuntimeEnvironment.application.getApplicationContext());
        settings = new Settings();
        settings.deleteSessionKey();
        registrationScreen = mock(RegistrationScreen.class);
        registrationPresenter.attachScreen(registrationScreen);
    }

    @Test
    public void testSuccesfulRegistration() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_VALID,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).finishRegistration(MockConstants.UNREGISTERED_USERNAME);
    }

    @Test
    public void testRegistrationFailure_user_already_exists() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_VALID,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).finishRegistration(MockConstants.UNREGISTERED_USERNAME);
    }

    @Test
    public void testPasswordMismatchError() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD + MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_VALID,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).showPasswordMismatchError();
    }

    @Test
    public void testEmtpyFieldError(){
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                "",
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_VALID,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).showEmptyFieldError();
    }

    @Test
    public void testWrongBirthYearError_Old() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_WRONG_OLD,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).showWrongBirthYearError();
    }

    @Test
    public void testWrongBirthYearError_Young() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_WRONG_YOUNG,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_VALID,
                null);
        verify(registrationScreen).showWrongBirthYearError();
    }

    @Test
    public void testZipCodeError() {
        registrationPresenter.register(
                MockConstants.UNREGISTERED_USERNAME,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.UNREGISTERED_PASSWORD,
                MockConstants.EMAIL_VALID,
                MockConstants.PHONE_VALID,
                MockConstants.SEX_VALID,
                MockConstants.BIRTHYEAR_VALID,
                MockConstants.BIRTHMONTH_VALID,
                MockConstants.BIRTHDAY_VALID,
                MockConstants.ZIPCODE_WRONG,
                null);
        verify(registrationScreen).showZipCodeError();
    }

    @Test public void testOnRegistrationEvent_throwable(){
        RegistrationEvent registrationEvent = new RegistrationEvent();
        registrationEvent.setRequest(new LoginUserRegistrationRequest());
        registrationEvent.setResponse(new UserRegistrationResponse());
        registrationEvent.setThrowable(new Throwable());
        registrationPresenter.onRegistrationEvent(registrationEvent);
        verify(registrationScreen).showRegistrationError(null);
    }

    @Test public void testOnRegistrationEvent_not200(){
        RegistrationEvent registrationEvent = new RegistrationEvent();
        registrationEvent.setRequest(new LoginUserRegistrationRequest());
        registrationEvent.setResponse(new UserRegistrationResponse());
        registrationEvent.setCode(302);
        registrationPresenter.onRegistrationEvent(registrationEvent);
        verify(registrationScreen).showRegistrationError(null);
    }

    @After
    public void tearDown(){
        registrationPresenter.detachScreen();
    }

}*/
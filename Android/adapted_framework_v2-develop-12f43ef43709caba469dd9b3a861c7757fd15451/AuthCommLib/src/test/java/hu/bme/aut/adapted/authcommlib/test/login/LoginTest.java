package hu.bme.aut.adapted.authcommlib.test.login;

/*
@RunWith(RobolectricDaggerTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 24)
public class LoginTest {


    private LoginPresenter loginPresenter;
    private Settings settings;
    private LoginScreen loginScreen;

    @Before
    public void setup() throws Exception {
        loginPresenter = new LoginPresenter();
        setTestInjector(loginPresenter);
        Store.init(RuntimeEnvironment.application.getApplicationContext());
        settings = new Settings();
        settings.deleteSessionKey();
        loginScreen = mock(LoginScreen.class);
        loginPresenter.attachScreen(loginScreen);
    }

    @Test
    public void testSuccesfulLogin() {
        loginPresenter.login(MockConstants.REGISTERED_USERNAME, MockConstants.REGISTERED_PASSWORD);
        verify(loginScreen).navigateToGame();
        Assert.assertEquals(settings.getSessionKey(), MockConstants.REGISTERED_SESSIONKEY);
    }

    @Test
    public void testLoginFailure(){
        loginPresenter.login(MockConstants.UNREGISTERED_USERNAME, MockConstants.UNREGISTERED_PASSWORD);
        verify(loginScreen).showLoginError();
    }

    @Test public void testOnLoginEvent_throwable(){
        LoginEvent loginEvent = new LoginEvent();
        loginEvent.setRequest(new LoginRequest());
        loginEvent.setResponse(new LoginResponse());
        loginEvent.setThrowable(new Throwable());
        loginPresenter.onLoginEvent(loginEvent);
        verify(loginScreen).showLoginError();
    }

    @Test public void testOnLoginEvent_not200(){
        LoginEvent loginEvent = new LoginEvent();
        loginEvent.setRequest(new LoginRequest());
        loginEvent.setResponse(new LoginResponse(false,"message",1,"sessionKey",false));
        loginEvent.setCode(302);
        loginPresenter.onLoginEvent(loginEvent);
        verify(loginScreen).showLoginError();
    }

    @Test public void testOnLoginEvent_200_notsucceeded(){
        LoginEvent loginEvent = new LoginEvent();
        loginEvent.setRequest(new LoginRequest());
        loginEvent.setResponse(new LoginResponse(false,"message",1,"sessionKey",false));
        loginEvent.setCode(200);
        loginPresenter.onLoginEvent(loginEvent);
        verify(loginScreen).showLoginError();
    }


    @After
    public void tearDown(){
        loginPresenter.detachScreen();
    }

}
*/
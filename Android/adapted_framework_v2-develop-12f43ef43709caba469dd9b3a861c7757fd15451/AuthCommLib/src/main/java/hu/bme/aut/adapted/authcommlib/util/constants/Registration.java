package hu.bme.aut.adapted.authcommlib.util.constants;

public class Registration {
    public static final int DISABILITY_DYSLEXIA = 1;
    public static final int DISABILITY_DYSGRAPHIA = 2;
    public static final int DISABILITY_DYSCALCULIA = 3;
    public static final int DISABILITY_ATTENTION = 4;
    public static final int DISABILITY_HYPERACTIVITY = 5;
    public static final int DISABILITY_AUTISM = 6;
    public static final int DISABILITY_NONE = 7;

    public static final int MINIMUM_AGE = 5;
    public static final int MAXIMUM_AGE = 120;
}

package hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event;

import hu.bme.aut.adapted.commonlib.util.Event;

public class LoginEvent extends Event {
    private String sessionKey;

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }
}

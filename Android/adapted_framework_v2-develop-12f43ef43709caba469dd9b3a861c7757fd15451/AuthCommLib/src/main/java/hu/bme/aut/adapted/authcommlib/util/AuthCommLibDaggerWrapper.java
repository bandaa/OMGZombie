package hu.bme.aut.adapted.authcommlib.util;

import hu.bme.aut.adapted.authcommlib.AuthCommLibInjector;

public class AuthCommLibDaggerWrapper {
    public static AuthCommLibInjector injector;

    public static void setInjector(AuthCommLibInjector injector) {
        AuthCommLibDaggerWrapper.injector = injector;
    }
}

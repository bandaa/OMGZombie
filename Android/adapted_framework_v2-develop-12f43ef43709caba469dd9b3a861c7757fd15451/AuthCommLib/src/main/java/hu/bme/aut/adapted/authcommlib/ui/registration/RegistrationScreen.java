package hu.bme.aut.adapted.authcommlib.ui.registration;

import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthScreen;

public interface RegistrationScreen extends BaseAuthScreen {
    void finishRegistration(String userName);

    void showEmptyFieldError();

    void showPasswordMismatchError();

    void showWrongBirthYearError();

    void showZipCodeError();

    void showUsernameExistsError();

    void showRegistrationError();
}

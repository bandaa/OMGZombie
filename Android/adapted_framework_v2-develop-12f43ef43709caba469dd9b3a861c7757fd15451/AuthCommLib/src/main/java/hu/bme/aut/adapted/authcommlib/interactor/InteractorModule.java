package hu.bme.aut.adapted.authcommlib.interactor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.AuthMessengerInteractor;

@Module
public class InteractorModule {
    @Provides
    @Singleton
    public AuthMessengerInteractor provideAuthMessengerInteractor() {
        return new AuthMessengerInteractor();
    }
}

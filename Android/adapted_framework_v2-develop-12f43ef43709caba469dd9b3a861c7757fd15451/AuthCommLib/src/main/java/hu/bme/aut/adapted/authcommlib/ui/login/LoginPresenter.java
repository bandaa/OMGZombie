package hu.bme.aut.adapted.authcommlib.ui.login;

import org.greenrobot.eventbus.Subscribe;

import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event.LoginEvent;
import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthPresenter;

import static hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper.injector;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class LoginPresenter extends BaseAuthPresenter<LoginScreen> {
    @Override
    protected void inject() {
        injector.inject(this);
    }

    public void login(final String username, final String password) {
        backgroundHandler.post(() -> authMessengerInteractor.login(username, password));
    }

    @Subscribe(threadMode = MAIN)
    public void onLoginEvent(LoginEvent event) {
        if (event.getThrowable() != null) {
            if (screen != null) screen.showLoginError();
            return;
        }

        if (screen != null) screen.navigateToGame(event.getSessionKey());
    }
}

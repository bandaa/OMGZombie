package hu.bme.aut.adapted.authcommlib.ui;


import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.authcommlib.ui.login.LoginPresenter;
import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationPresenter;

@Module
public class UIModule {
    @Provides
    public LoginPresenter provideLoginPresenter() {
        return new LoginPresenter();
    }

    @Provides
    public RegistrationPresenter provideRegistrationPresenter() {
        return new RegistrationPresenter();
    }
}

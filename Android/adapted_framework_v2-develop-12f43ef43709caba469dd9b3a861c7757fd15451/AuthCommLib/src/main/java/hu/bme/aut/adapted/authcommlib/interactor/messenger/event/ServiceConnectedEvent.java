package hu.bme.aut.adapted.authcommlib.interactor.messenger.event;

import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;
import hu.bme.aut.adapted.commonlib.util.Event;

public class ServiceConnectedEvent extends Event {
    private Settings settings;

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }
}

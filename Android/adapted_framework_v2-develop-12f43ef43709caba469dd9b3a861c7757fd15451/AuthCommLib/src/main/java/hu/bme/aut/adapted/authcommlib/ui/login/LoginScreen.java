package hu.bme.aut.adapted.authcommlib.ui.login;


import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthScreen;

public interface LoginScreen extends BaseAuthScreen {
    void navigateToGame(String sessionKey);

    void showLoginError();
}

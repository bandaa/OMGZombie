package hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event;

import hu.bme.aut.adapted.commonlib.util.Event;

public class RegistrationEvent extends Event {
    private String username;
    private String message;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

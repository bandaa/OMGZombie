package hu.bme.aut.adapted.authcommlib.ui.baseauth;

import android.os.Handler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.AuthMessengerInteractor;
import hu.bme.aut.adapted.authcommlib.interactor.messenger.event.ServiceConnectedEvent;
import hu.bme.aut.adapted.commonlib.ui.Presenter;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;

import static org.greenrobot.eventbus.ThreadMode.MAIN;

public abstract class BaseAuthPresenter<S extends BaseAuthScreen> extends Presenter<S> {
    @Inject
    protected AuthMessengerInteractor authMessengerInteractor;

    @Inject
    @Ipc
    protected Handler backgroundHandler;

    @Inject
    protected EventBus bus;

    private boolean initialized;

    @Override
    public void attachScreen(S screen) {
        super.attachScreen(screen);
        inject();
        bus.register(this);

        connect();
    }

    @Override
    public void detachScreen() {
        disconnect();

        bus.unregister(this);
        super.detachScreen();
    }

    protected abstract void inject();

    private void connect() {
        backgroundHandler.post(() -> authMessengerInteractor.connect());
    }

    private void disconnect() {
        backgroundHandler.post(() -> {
            backgroundHandler.removeCallbacksAndMessages(null);
            authMessengerInteractor.disconnect();
        });
    }

    private void initialize() {
        if (screen != null && !initialized) {
            initialized = true;
            screen.frameworkInitialized();
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onServiceConnectedEvent(ServiceConnectedEvent event) {
        if (event.getThrowable() != null) {
            return;
        }

        initialize();
    }
}

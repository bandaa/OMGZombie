package hu.bme.aut.adapted.authcommlib;

import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.AuthMessengerInteractor;
import hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity;
import hu.bme.aut.adapted.authcommlib.ui.login.LoginPresenter;
import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationActivity;
import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationPresenter;

public interface AuthCommLibInjector {
    void inject(LoginActivity loginActivity);

    void inject(RegistrationActivity registrationActivity);

    void inject(LoginPresenter loginPresenter);

    void inject(RegistrationPresenter registrationPresenter);

    void inject(AuthMessengerInteractor authMessengerInteractor);
}

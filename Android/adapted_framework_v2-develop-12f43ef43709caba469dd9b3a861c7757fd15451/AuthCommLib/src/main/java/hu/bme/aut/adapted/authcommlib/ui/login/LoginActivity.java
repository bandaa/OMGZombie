package hu.bme.aut.adapted.authcommlib.ui.login;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hu.bme.aut.adapted.authcommlib.R;
import hu.bme.aut.adapted.authcommlib.R2;
import hu.bme.aut.adapted.authcommlib.annotation.RequireLogin;
import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthActivity;
import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationActivity;
import io.fabric.sdk.android.Fabric;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static hu.bme.aut.adapted.authcommlib.annotation.RequireLogin.SecurityLevel.OPTIONAL;
import static hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationActivity.EXTRA_REGISTRATION_USERNAME;
import static hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationActivity.REQUEST_CODE_REGISTRATION;
import static hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper.injector;

public class LoginActivity extends BaseAuthActivity implements LoginScreen {
    public static final int REQUEST_CODE_LOGIN = 1;

    public static final String EXTRA_LOGIN_SECURITY_LEVEL = "login_security_level";
    public static final String EXTRA_LOGIN_ROLE = "login_role";
    public static final String EXTRA_LOGIN_SESSION_KEY = "login_sessionKey";
    public static final String EXTRA_LOGIN_ANONYMOUS = "login_anonymous";

    @Inject
    LoginPresenter presenter;

    @BindView(R2.id.RegistrationButton)
    Button registerButton;

    @BindView(R2.id.LoginButton)
    Button loginButton;

    @BindView(R2.id.AnonymousLoginButton)
    Button anonymousLoginButton;

    @BindView(R2.id.UserNameET)
    public EditText nameET;

    @BindView(R2.id.PasswordET)
    public EditText passwordET;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        injector.inject(this);
        Fabric.with(this, new Crashlytics());

        RequireLogin.SecurityLevel securityLevel = (RequireLogin.SecurityLevel) getIntent().getSerializableExtra(EXTRA_LOGIN_SECURITY_LEVEL);
        RequireLogin.Role role = (RequireLogin.Role) getIntent().getSerializableExtra(EXTRA_LOGIN_ROLE);

        anonymousLoginButton.setVisibility(securityLevel == OPTIONAL ? VISIBLE : GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachScreen(this);
    }

    @Override
    protected void onPause() {
        presenter.detachScreen();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_REGISTRATION && resultCode == RESULT_OK && data != null) {
            registrationFinished(data.getStringExtra(EXTRA_REGISTRATION_USERNAME));
        }
    }

    @Override
    public void navigateToGame(String sessionKey) {
        showMessage("LOGIN SUCCESFUL");
        setResult(RESULT_OK, new Intent()
                .putExtra(EXTRA_LOGIN_SESSION_KEY, sessionKey)
                .putExtra(EXTRA_LOGIN_ANONYMOUS, false));
        finish();
    }

    @Override
    public void showLoginError() {
        showMessage(getString(R.string.login_error));
    }

    @OnClick(R2.id.LoginButton)
    void login() {
        presenter.login(nameET.getText().toString(), passwordET.getText().toString());
    }

    @OnClick(R2.id.RegistrationButton)
    void navigateToRegistration() {
        startActivityForResult(new Intent(this, RegistrationActivity.class),
                REQUEST_CODE_REGISTRATION);
    }

    @OnClick(R2.id.AnonymousLoginButton)
    void anonymousLogin() {
        setResult(RESULT_OK, new Intent().putExtra(EXTRA_LOGIN_ANONYMOUS, true));
        finish();
    }

    private void registrationFinished(String userName) {
        nameET.setText(userName);
        passwordET.setText("");
    }
}

package hu.bme.aut.adapted.authcommlib.ui.registration;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hu.bme.aut.adapted.authcommlib.R;
import hu.bme.aut.adapted.authcommlib.R2;
import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthActivity;

import static hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper.injector;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_ATTENTION;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_AUTISM;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_DYSCALCULIA;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_DYSGRAPHIA;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_DYSLEXIA;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_HYPERACTIVITY;

public class RegistrationActivity extends BaseAuthActivity implements RegistrationScreen {
    public static final int REQUEST_CODE_REGISTRATION = 1;

    public static final String EXTRA_REGISTRATION_USERNAME = "registration_username";

    private static final SparseIntArray DISABILITY_MAP = new SparseIntArray();

    static {
        DISABILITY_MAP.put(R.id.DyslexiaCB, DISABILITY_DYSLEXIA);
        DISABILITY_MAP.put(R.id.DysgraphiaCB, DISABILITY_DYSGRAPHIA);
        DISABILITY_MAP.put(R.id.DyscalculiaCB, DISABILITY_DYSCALCULIA);
        DISABILITY_MAP.put(R.id.AttentionCB, DISABILITY_ATTENTION);
        DISABILITY_MAP.put(R.id.HyperactivityCB, DISABILITY_HYPERACTIVITY);
        DISABILITY_MAP.put(R.id.AutismCB, DISABILITY_AUTISM);
    }

    @BindView(R2.id.UserNameET)
    EditText userNameET;

    @BindView(R2.id.PasswordET)
    EditText passwordET;

    @BindView(R2.id.PasswordConfirmET)
    EditText passwordConfirmET;

    @BindView(R2.id.EmailET)
    EditText emailET;

    @BindView(R2.id.PhoneNumberET)
    EditText phoneNumberET;

    @BindView(R2.id.SexSpinner)
    Spinner sexSpinner;

    @BindView(R2.id.BirthDateDP)
    DatePicker birthDateDP;

    @BindView(R2.id.ZipCodeET)
    EditText zipCodeET;

    @BindView(R2.id.DisabilitiesLayout)
    ViewGroup disabilitiesLayout;

    @Inject
    RegistrationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        injector.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachScreen(this);
    }

    @Override
    protected void onPause() {
        presenter.detachScreen();
        super.onPause();
    }

    private List<Integer> getDisabilityList() {
        final List<Integer> disabilityList = new ArrayList<>();

        for (int i = 0; i < disabilitiesLayout.getChildCount(); i++) {
            final CheckBox checkBox = (CheckBox) disabilitiesLayout.getChildAt(i);

            if (checkBox.isChecked()) {
                final int disability = DISABILITY_MAP.get(checkBox.getId(), -1);

                if (disability != -1) {
                    disabilityList.add(disability);
                }
            }
        }

        Collections.sort(disabilityList);
        return disabilityList;
    }

    @OnClick(R2.id.RegistrationButton)
    public void register() {
        presenter.register(userNameET.getText().toString().trim(),
                passwordET.getText().toString().trim(),
                passwordConfirmET.getText().toString().trim(),
                emailET.getText().toString().trim(),
                phoneNumberET.getText().toString().trim(),
                sexSpinner.getSelectedItemPosition() + 1,
                birthDateDP.getYear(),
                birthDateDP.getMonth(),
                birthDateDP.getDayOfMonth(),
                zipCodeET.getText().toString().trim(),
                getDisabilityList());
    }

    @Override
    public void finishRegistration(String userName) {
        final Intent intent = new Intent();
        intent.putExtra(EXTRA_REGISTRATION_USERNAME, userName);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void showEmptyFieldError() {
        showMessage(getString(R.string.registration_empty_field));
    }

    @Override
    public void showPasswordMismatchError() {
        showMessage(getString(R.string.password_mismatch));
    }

    @Override
    public void showWrongBirthYearError() {
        showMessage(getString(R.string.wrong_birthdate));
    }

    @Override
    public void showZipCodeError() {
        showMessage(getString(R.string.wrong_zipcode));
    }

    @Override
    public void showUsernameExistsError() {
        showMessage(getString(R.string.registration_error_username_exists));
    }

    @Override
    public void showRegistrationError() {
        showMessage(getString(R.string.registration_error));
    }
}

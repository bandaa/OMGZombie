package hu.bme.aut.adapted.authcommlib.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static hu.bme.aut.adapted.authcommlib.annotation.RequireLogin.Role.USER;
import static hu.bme.aut.adapted.authcommlib.annotation.RequireLogin.SecurityLevel.REQUIRED;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Use this annotation to require login in activities that implement
 * {@link hu.bme.aut.adapted.gamelib.ui.base.GameBase}. These activities
 * will redirect the user to the login activity is no active session is
 * available. Use the values of {link {@link SecurityLevel} to indicate whether
 * or not authentication is mandatory.
 *
 * @author Soma
 *
 */
@Target(TYPE)
@Retention(RUNTIME)
@Documented
@Inherited
public @interface RequireLogin {
	/**
	 * This enum indicates whether or not authentication is mandatory. If
	 * {@link SecurityLevel#OPTIONAL} is supplied, the user will have the option
	 * to return to the original activity without logging in.
	 * 
	 * @author Soma
	 *
	 */
	enum SecurityLevel {
		REQUIRED, OPTIONAL
	}
	
	enum Role {
		USER, SUPERVISOR
	}

	/**
	 * Determine whether authentication is mandatory. If
	 * {@link SecurityLevel#OPTIONAL} is supplied, the user will have the option
	 * to return to the original activity without logging in, while in case
	 * {@link SecurityLevel#REQUIRED}, no such option will be present.
	 * 
	 * @return
	 */
	SecurityLevel securitylevel() default REQUIRED;
	
	/**
	 * Determine the authenticated users role. If
	 * {@link Role#USER} is supplied, the user will have authenticate as a
	 * normal user, while in case 
	 * {@link Role#SUPERVISOR}, will have authenticate as a supervisor.
	 * 
	 * @return
	 */
	Role role() default USER;
}

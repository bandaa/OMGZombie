package hu.bme.aut.adapted.authcommlib.ui.baseauth;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hu.bme.aut.adapted.authcommlib.R;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

public class BaseAuthActivity extends AppCompatActivity implements BaseAuthScreen {
	private View snackbar;

	@Override
	public void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		snackbar = findViewById(android.R.id.content);
    }

	protected final void showMessage(String message) {
		Snackbar.make(snackbar, message, LENGTH_LONG).setAction(R.string.snackbar_ok, null).show();
	}

	@Override
	public final void frameworkInitialized() {
	}
}

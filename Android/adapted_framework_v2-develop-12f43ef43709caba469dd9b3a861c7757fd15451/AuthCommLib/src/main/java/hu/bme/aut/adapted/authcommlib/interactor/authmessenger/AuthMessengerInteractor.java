package hu.bme.aut.adapted.authcommlib.interactor.authmessenger;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import hu.bme.aut.adapted.authcommlib.exception.LoginFailedException;
import hu.bme.aut.adapted.authcommlib.exception.RegistrationFailedException;
import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event.LoginEvent;
import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event.RegistrationEvent;
import hu.bme.aut.adapted.authcommlib.interactor.messenger.MessengerInteractor;
import hu.bme.aut.adapted.commonlib.exception.ServiceDisconnectedException;
import hu.bme.aut.adapted.commonlib.ipc.IAuth;
import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;

import static hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper.injector;

public class AuthMessengerInteractor extends MessengerInteractor<IAuth> implements IAuthCallback {
    private final IAuthCallback callback = new IAuthCallback.Stub() {
        @Override
        public void onLogin(String sessionKey) {
            backgroundHandler.post(() -> AuthMessengerInteractor.this.onLogin(sessionKey));
        }

        @Override
        public void onRegister(boolean success, String username, String message) {
            backgroundHandler.post(() -> AuthMessengerInteractor.this.onRegister(
                    success, username, message));
        }
    };

    public AuthMessengerInteractor() {
        super(IAuth.class);
    }

    @Override
    protected void inject() {
        injector.inject(this);
    }

    @Override
    protected IAuth createInterface(IBinder service) {
        return IAuth.Stub.asInterface(service);
    }

    public void login(String username, String password) {
        if (framework != null) {
            try {
                framework.login(this, username, password);
            } catch (RemoteException e) {
                LoginEvent event = new LoginEvent();
                event.setThrowable(e);
                bus.post(event);
            }
        } else {
            LoginEvent event = new LoginEvent();
            event.setThrowable(new ServiceDisconnectedException());
            bus.post(event);
        }
    }

    public void register(String username,
                         String password,
                         String email,
                         String phoneNumber,
                         int sex,
                         int birthYear,
                         int birthMonth,
                         int birthDay,
                         int zipCode,
                         String disabilities) {
        if (framework != null) {
            try {
                framework.register(this,
                        username,
                        password,
                        email,
                        phoneNumber,
                        sex,
                        birthYear,
                        birthMonth,
                        birthDay,
                        zipCode,
                        disabilities);
            } catch (RemoteException e) {
                RegistrationEvent event = new RegistrationEvent();
                event.setThrowable(e);
                bus.post(event);
            }
        } else {
            RegistrationEvent event = new RegistrationEvent();
            event.setThrowable(new ServiceDisconnectedException());
            bus.post(event);
        }
    }

    @Override
    public void onLogin(String sessionKey) {
        boolean success = sessionKey != null;

        Log.d("AuthCommLib", "on login: success: " + success + " (session key: " + sessionKey + ")");

        LoginEvent event = new LoginEvent();

        if (success) {
            event.setSessionKey(sessionKey);
        } else {
            event.setThrowable(new LoginFailedException());
        }

        bus.post(event);
    }

    @Override
    public void onRegister(boolean success, String username, String message) {
        Log.d("AuthCommLib", "on register: success: " + success);

        RegistrationEvent event = new RegistrationEvent();
        event.setUsername(username);
        event.setMessage(message);

        if (!success) {
            event.setThrowable(new RegistrationFailedException());
        }

        bus.post(event);
    }

    @Override
    public IBinder asBinder() {
        return callback.asBinder();
    }
}

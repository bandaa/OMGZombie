package hu.bme.aut.adapted.authcommlib.ui.registration;

import android.text.TextUtils;

import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;
import java.util.List;

import hu.bme.aut.adapted.authcommlib.interactor.authmessenger.event.RegistrationEvent;
import hu.bme.aut.adapted.authcommlib.ui.baseauth.BaseAuthPresenter;

import static hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper.injector;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.DISABILITY_NONE;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.MAXIMUM_AGE;
import static hu.bme.aut.adapted.authcommlib.util.constants.Registration.MINIMUM_AGE;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class RegistrationPresenter extends BaseAuthPresenter<RegistrationScreen> {
    public static final String MSG_USERNAME_EXISTS = "User name already exists!";

    @Override
    protected void inject() {
        injector.inject(this);
    }

    public void register(String userName,
                         String password,
                         String passwordConfirm,
                         String email,
                         String phoneNumber,
                         int sex,
                         int birthYear,
                         int birthMonth,
                         int birthDay,
                         String zipCode,
                         List<Integer> disabilityList) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        Integer zipCodeInteger;

        try {
            zipCodeInteger = Integer.parseInt(zipCode);
        } catch (NumberFormatException e) {
            zipCodeInteger = null;
        }

        if (userName.isEmpty() || password.isEmpty() || email.isEmpty() || zipCode.isEmpty()) {
            if (screen != null) screen.showEmptyFieldError();
        } else if (!password.contentEquals(passwordConfirm)) {
            if (screen != null) screen.showPasswordMismatchError();
        } else if (birthYear > currentYear - MINIMUM_AGE || birthYear < currentYear - MAXIMUM_AGE) {
            if (screen != null) screen.showWrongBirthYearError();
        } else if (zipCodeInteger == null || zipCode.length() != 4) {
            if (screen != null) screen.showZipCodeError();
        } else {
            int zipCodeInt = zipCodeInteger;
            String disabilities;

            if (disabilityList != null && !disabilityList.isEmpty()) {
                disabilities = TextUtils.join(";", disabilityList) + ";";
            } else {
                disabilities = Integer.toString(DISABILITY_NONE) + ";";
            }

            backgroundHandler.post(() -> authMessengerInteractor.register(
                    userName,
                    password,
                    email,
                    phoneNumber,
                    sex,
                    birthYear,
                    birthMonth,
                    birthDay,
                    zipCodeInt,
                    disabilities));
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onRegistrationEvent(RegistrationEvent event) {
        if (event.getThrowable() != null) {
            if (MSG_USERNAME_EXISTS.equals(event.getMessage())) {
                if (screen != null) screen.showUsernameExistsError();
            } else {
                if (screen != null) screen.showRegistrationError();
            }

            return;
        }

        if (screen != null) screen.finishRegistration(event.getUsername());
    }
}

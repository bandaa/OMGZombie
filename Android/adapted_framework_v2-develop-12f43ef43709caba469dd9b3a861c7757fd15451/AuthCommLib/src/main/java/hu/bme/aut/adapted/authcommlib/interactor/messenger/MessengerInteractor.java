package hu.bme.aut.adapted.authcommlib.interactor.messenger;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.authcommlib.interactor.messenger.event.ServiceConnectedEvent;
import hu.bme.aut.adapted.commonlib.exception.ServiceConnectionFailedException;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;

import static android.content.Context.BIND_AUTO_CREATE;
import static hu.bme.aut.adapted.commonlib.util.Constants.FRAMEWORK_SERVICE_COMPONENT_NAME;

public abstract class MessengerInteractor<I extends IInterface> {
    @Inject
    Context appContext;

    @Inject
    protected EventBus bus;

    @Inject
    @Ipc
    protected Handler backgroundHandler;

    private Class<I> interfaceClass;
    protected I framework;

    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            backgroundHandler.postAtFrontOfQueue(() -> {
                framework = createInterface(service);
                ServiceConnectedEvent event = new ServiceConnectedEvent();
                onConnect(event);
                bus.post(event);
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            backgroundHandler.postAtFrontOfQueue(() -> framework = null);
        }
    };

    protected MessengerInteractor(Class<I> interfaceClass) {
        this.interfaceClass = interfaceClass;
        inject();
    }

    public void connect() {
        boolean success = appContext.bindService(new Intent()
                .setComponent(FRAMEWORK_SERVICE_COMPONENT_NAME)
                .setAction(interfaceClass.getName()), connection, BIND_AUTO_CREATE);

        if (!success) {
            ServiceConnectedEvent event = new ServiceConnectedEvent();
            event.setThrowable(new ServiceConnectionFailedException());
            bus.post(event);
        }
    }

    public void disconnect() {
        onDisconnect();
        appContext.unbindService(connection);
        framework = null;
        backgroundHandler.removeCallbacksAndMessages(null);
    }

    protected abstract void inject();

    protected abstract I createInterface(IBinder service);

    protected void onConnect(ServiceConnectedEvent event) {
    }

    protected void onDisconnect() {
    }
}

package hu.bme.aut.adapted.commonlib.ipc;

import hu.bme.aut.adapted.commonlib.ipc.IGame;
import hu.bme.aut.adapted.commonlib.ipc.payload.EventType;
import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.RewardType;
import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;

interface IFramework {
    Settings registerGame(in IGame game, int gameId);

    void unregisterGame(in IGame game);

    int startGameplay(int gameId, String sessionKey);

    void stopGameplay(int gameId);

    int registerEventType(int gameId, in EventType eventType);

    int registerRewardType(int gameId, in RewardType rewardType);

    void sendGameEvent(int gameplayId, in GameEvent event);

    void sendScreenshot(int gameplayId, in byte[] screenshot, boolean large);

    oneway void connectNeurosky();

    oneway void disconnectNeurosky();

    oneway void connectZephyr();

    oneway void disconnectZephyr();

    oneway void connectEmotiv();

    oneway void disconnectEmotiv();

    oneway void updateSensorStatus();

    oneway void startUpload();

    boolean test(inout Bundle bundle);
}

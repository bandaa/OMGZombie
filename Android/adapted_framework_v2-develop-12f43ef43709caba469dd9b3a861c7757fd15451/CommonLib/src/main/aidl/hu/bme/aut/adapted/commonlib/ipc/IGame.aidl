package hu.bme.aut.adapted.commonlib.ipc;

interface IGame {
    oneway void takeScreenshot();
    oneway void sensorStatusChanged(String name, String value);
    oneway void sendAttention(int value);
    oneway void sendMeditation(int value);
    oneway void sendHeartRate(int value);
}

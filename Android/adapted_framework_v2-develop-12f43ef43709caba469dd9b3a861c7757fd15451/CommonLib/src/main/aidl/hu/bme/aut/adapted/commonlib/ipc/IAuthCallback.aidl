package hu.bme.aut.adapted.commonlib.ipc;

interface IAuthCallback {
    oneway void onLogin(String sessionKey);

    oneway void onRegister(boolean success, String username, String message);
}

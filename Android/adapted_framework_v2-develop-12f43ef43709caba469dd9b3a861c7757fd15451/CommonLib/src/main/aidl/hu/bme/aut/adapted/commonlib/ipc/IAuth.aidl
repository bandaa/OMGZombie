package hu.bme.aut.adapted.commonlib.ipc;

import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;

interface IAuth {
    oneway void login(IAuthCallback callback, String username, String password);

    oneway void register(IAuthCallback callback,
                         String username,
                         String password,
                         String email,
                         String phoneNumber,
                         int sex,
                         int birthYear,
                         int birthMonth,
                         int birthDay,
                         int zipCode,
                         String disabilities);
}

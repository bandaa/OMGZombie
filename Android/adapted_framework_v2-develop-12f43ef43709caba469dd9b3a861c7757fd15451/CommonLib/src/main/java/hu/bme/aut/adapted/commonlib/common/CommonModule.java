package hu.bme.aut.adapted.commonlib.common;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.commonlib.util.di.Hpv;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;
import hu.bme.aut.adapted.commonlib.util.di.Mock;
import hu.bme.aut.adapted.commonlib.util.di.Network;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

@Module
public class CommonModule {
    private static final String NETWORK_THREAD_NAME = "networkThread";
    private static final String IPC_THREAD_NAME = "ipcThread";
    private static final String HPV_THREAD_NAME = "hpvThread";
    private static final String MOCK_THREAD_NAME = "mockThread";

    private final Context context;

    public CommonModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    public Executor provideExecutor() {
        return Executors.newFixedThreadPool(1);
    }

    private Looper newLooper(String threadName) {
        HandlerThread thread = new HandlerThread(threadName, THREAD_PRIORITY_BACKGROUND);
        thread.start();
        return thread.getLooper();
    }

    @Provides
    @Singleton
    @Network
    public Looper provideNetworkLooper() {
        return newLooper(NETWORK_THREAD_NAME);
    }

    @Provides
    @Singleton
    @Ipc
    public Looper provideIpcLooper() {
        return newLooper(IPC_THREAD_NAME);
    }

    @Provides
    @Singleton
    @Hpv
    public Looper provideHpvLooper() {
        return newLooper(HPV_THREAD_NAME);
    }


    @Provides
    @Singleton
    @Mock
    public Looper provideMockLooper() {
        return newLooper(MOCK_THREAD_NAME);
    }

    @Provides
    @Network
    public Handler provideNetworkHandler(@Network Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Ipc
    public Handler provideIpcHandler(@Ipc Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Hpv
    public Handler provideHpvHandler(@Hpv Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Mock
    public Handler provideMockHandler(@Mock Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }
}

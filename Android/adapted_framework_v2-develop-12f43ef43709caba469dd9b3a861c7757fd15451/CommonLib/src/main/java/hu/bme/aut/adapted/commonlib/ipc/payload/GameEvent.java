package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonObject;

import static hu.bme.aut.adapted.commonlib.util.GsonHelper.gson;

public class GameEvent implements Parcelable {
    private long timeStamp;
    private float goodnessValue;
    private int eventTypeId;
    private JsonObject parameters;

    public GameEvent(long timeStamp, float goodnessValue, int eventTypeId, JsonObject parameters) {
        this.timeStamp = timeStamp;
        this.goodnessValue = goodnessValue;
        this.eventTypeId = eventTypeId;
        this.parameters = parameters;
    }

    protected GameEvent(Parcel in) {
        timeStamp = in.readLong();
        goodnessValue = in.readFloat();
        eventTypeId = in.readInt();
        parameters = gson.fromJson(in.readString(), JsonObject.class);
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public float getGoodnessValue() {
        return goodnessValue;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public JsonObject getParameters() {
        return parameters;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(timeStamp);
        dest.writeFloat(goodnessValue);
        dest.writeInt(eventTypeId);
        dest.writeString(gson.toJson(parameters));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GameEvent> CREATOR = new Creator<GameEvent>() {
        @Override
        public GameEvent createFromParcel(Parcel in) {
            return new GameEvent(in);
        }

        @Override
        public GameEvent[] newArray(int size) {
            return new GameEvent[size];
        }
    };
}

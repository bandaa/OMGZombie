package hu.bme.aut.adapted.commonlib.util;

import android.content.ComponentName;

public class Constants {
    public static final ComponentName FRAMEWORK_SERVICE_COMPONENT_NAME =
            new ComponentName("hu.bme.aut.adapted.framework",
                    "hu.bme.aut.adapted.framework.service.FrameworkService");

    public static class Broadcast {

        public static class Sensors {

            public static final String EXTRA_STATUS = "hu.bme.aut.adapted.sensor.extra.status";

            public enum Status {
                OFFLINE, UNRESPONSIVE, LOW_BATTERY, BAD_SIGNAL, CONNECTING, ONLINE
            }

            public static class NeuroSkyEEG {

                public static final String ACTION_RESPONSE_STATUS = "hu.bme.aut.adapted.sensor.neurosky.response.status";
                public static final String ACTION_COMMAND_CONNECT = "hu.bme.aut.adapted.sensor.neurosky.command.connect";
                public static final String ACTION_COMMAND_STATUS = "hu.bme.aut.adapted.sensor.neurosky.command.status";

            }

            public static class HXM {

                public static final String ACTION_RESPONSE_STATUS = "hu.bme.aut.adapted.sensor.hxm.response.status";
                public static final String ACTION_COMMAND_CONNECT = "hu.bme.aut.adapted.hxm.sensor.command.connect";
                public static final String ACTION_COMMAND_STATUS = "hu.bme.aut.adapted.hxm.sensor.command.status";
            }

            public static class EmotivEEG {

                public static final String ACTION_RESPONSE_STATUS = "hu.bme.aut.adapted.sensor.emotiv.response.status";
            }
        }

        public static class MentalState {

            public static final String ACTION_ATTENTION = "hu.bme.aut.adapted.mentalstate.attention";
            public static final String ACTION_MEDITATION = "hu.bme.aut.adapted.mentalstate.meditation";
            public static final String ACTION_SUCCESS = "hu.bme.aut.adapted.mentalstate.success";

        }

        public static class Physiology {

            public static final String ACTION_HEARTRATE = "hu.bme.aut.adapted.physiology.heartrate";

        }

        public static class Classification {


            public static class SVM {
                public static final String TRAIN_START = "hu.bme.aut.adapted.classification.svm.train.start";
                public static final String ACTION_SVM = "hu.bme.aut.adapted.classification.svm.action";
                public static final String ACTION_RESPONSE_STATUS = "hu.bme.adapted.classification.svm.status";
                public static final String TRAIN_STOP = "hu.bme.aut.adapted.classification.svm.train.stop";
            }
        }

    }

}


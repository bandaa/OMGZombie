package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;
import android.os.Parcelable;

public class ParamType implements Parcelable {
    private String name;
    private String displayName;
    private String type;
    private String minValue;
    private String maxValue;
    private boolean dynamic;

    public ParamType(String name, String displayName, String type, String minValue, String maxValue, boolean dynamic) {
        this.name = name;
        this.displayName = displayName;
        this.type = type;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.dynamic = dynamic;
    }

    protected ParamType(Parcel in) {
        name = in.readString();
        displayName = in.readString();
        type = in.readString();
        minValue = in.readString();
        maxValue = in.readString();
        dynamic = in.readByte() != 0;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getType() {
        return type;
    }

    public String getMinValue() {
        return minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public boolean isDynamic() {
        return dynamic;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(displayName);
        dest.writeString(type);
        dest.writeString(minValue);
        dest.writeString(maxValue);
        dest.writeByte((byte) (dynamic ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ParamType> CREATOR = new Creator<ParamType>() {
        @Override
        public ParamType createFromParcel(Parcel in) {
            return new ParamType(in);
        }

        @Override
        public ParamType[] newArray(int size) {
            return new ParamType[size];
        }
    };
}

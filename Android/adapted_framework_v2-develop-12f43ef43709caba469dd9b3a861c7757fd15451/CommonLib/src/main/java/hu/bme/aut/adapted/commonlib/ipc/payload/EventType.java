package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;

import java.util.List;

public class EventType extends TypeDescriptor {
    private Integer rewardTypeId;

    public EventType(String name, String displayName, List<ParamType> paramTypes, boolean dynamic,
                     Integer rewardTypeId) {
        super(name, displayName, paramTypes, dynamic);
        this.rewardTypeId = rewardTypeId;
    }

    protected EventType(Parcel in) {
        super(in);

        boolean hasRewardTypeId = in.readByte() != 0;
        int rewardTypeId = in.readInt();
        this.rewardTypeId = hasRewardTypeId ? rewardTypeId : null;
    }

    public Integer getRewardTypeId() {
        return rewardTypeId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte((byte) (rewardTypeId != null ? 1 : 0));
        dest.writeInt(rewardTypeId != null ? rewardTypeId : 0);
    }

    public static final Creator<EventType> CREATOR = new Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel in) {
            return new EventType(in);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };
}

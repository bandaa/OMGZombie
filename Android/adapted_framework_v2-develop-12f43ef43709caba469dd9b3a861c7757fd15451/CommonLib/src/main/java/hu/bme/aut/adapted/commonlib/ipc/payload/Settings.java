package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;
import android.os.Parcelable;

public class Settings implements Parcelable {
    private final boolean enableAdmin;
    private final boolean automaticLock;
    private final boolean lockTaskModeEnabled;
    private final boolean blockHomeButton;
    private final boolean blockBackButton;
    private final boolean wakelock;
    private final boolean hideNavigationBar;
    private final boolean fullBrightness;
    private final boolean hideActionBar;
    private final boolean keepInFront;
    private final boolean soundEnabled;

    public Settings(boolean enableAdmin, boolean automaticLock, boolean lockTaskModeEnabled,
                    boolean blockHomeButton, boolean blockBackButton, boolean wakelock,
                    boolean hideNavigationBar, boolean fullBrightness, boolean hideActionBar,
                    boolean keepInFront, boolean soundEnabled) {
        this.enableAdmin = enableAdmin;
        this.automaticLock = automaticLock;
        this.lockTaskModeEnabled = lockTaskModeEnabled;
        this.blockHomeButton = blockHomeButton;
        this.blockBackButton = blockBackButton;
        this.wakelock = wakelock;
        this.hideNavigationBar = hideNavigationBar;
        this.fullBrightness = fullBrightness;
        this.hideActionBar = hideActionBar;
        this.keepInFront = keepInFront;
        this.soundEnabled = soundEnabled;
    }

    protected Settings(Parcel in) {
        enableAdmin = in.readByte() != 0;
        automaticLock = in.readByte() != 0;
        lockTaskModeEnabled = in.readByte() != 0;
        blockHomeButton = in.readByte() != 0;
        blockBackButton = in.readByte() != 0;
        wakelock = in.readByte() != 0;
        hideNavigationBar = in.readByte() != 0;
        fullBrightness = in.readByte() != 0;
        hideActionBar = in.readByte() != 0;
        keepInFront = in.readByte() != 0;
        soundEnabled = in.readByte() != 0;
    }

    public boolean isEnableAdmin() {
        return enableAdmin;
    }

    public boolean isAutomaticLock() {
        return automaticLock;
    }

    public boolean isLockTaskModeEnabled() {
        return lockTaskModeEnabled;
    }

    public boolean isBlockHomeButton() {
        return blockHomeButton;
    }

    public boolean isBlockBackButton() {
        return blockBackButton;
    }

    public boolean isWakelock() {
        return wakelock;
    }

    public boolean isHideNavigationBar() {
        return hideNavigationBar;
    }

    public boolean isFullBrightness() {
        return fullBrightness;
    }

    public boolean isHideActionBar() {
        return hideActionBar;
    }

    public boolean isKeepInFront() {
        return keepInFront;
    }

    public boolean isSoundEnabled() {
        return soundEnabled;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (enableAdmin ? 1 : 0));
        dest.writeByte((byte) (automaticLock ? 1 : 0));
        dest.writeByte((byte) (lockTaskModeEnabled ? 1 : 0));
        dest.writeByte((byte) (blockHomeButton ? 1 : 0));
        dest.writeByte((byte) (blockBackButton ? 1 : 0));
        dest.writeByte((byte) (wakelock ? 1 : 0));
        dest.writeByte((byte) (hideNavigationBar ? 1 : 0));
        dest.writeByte((byte) (fullBrightness ? 1 : 0));
        dest.writeByte((byte) (hideActionBar ? 1 : 0));
        dest.writeByte((byte) (keepInFront ? 1 : 0));
        dest.writeByte((byte) (soundEnabled ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Settings> CREATOR = new Creator<Settings>() {
        @Override
        public Settings createFromParcel(Parcel in) {
            return new Settings(in);
        }

        @Override
        public Settings[] newArray(int size) {
            return new Settings[size];
        }
    };
}

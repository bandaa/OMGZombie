package hu.bme.aut.adapted.commonlib.util;

import android.os.Handler;
import android.os.RemoteException;

public class RemoteHelper {
    public interface VoidCall {
        void run() throws RemoteException;
    }

    public interface Call<V> {
        V run() throws RemoteException;
    }

    private static class ResultHolder<T> {
        public final Object lock = new Object();
        public volatile T result;
        public volatile Exception e;
    }

    public static <R> R localExecute(Call<R> call, Handler handler) throws RemoteException {
        ResultHolder<R> holder = new ResultHolder<>();

        synchronized (holder.lock) {
            handler.post(() -> {
                try {
                    holder.result = call.run();
                } catch (RemoteException | RuntimeException e) {
                    holder.e = e;
                }

                synchronized (holder.lock) {
                    holder.lock.notifyAll();
                }

                if (holder.e instanceof RemoteException) {
                    throw new RuntimeException(holder.e);
                } else if (holder.e instanceof RuntimeException) {
                    throw (RuntimeException) holder.e;
                }
            });

            try {
                holder.lock.wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        if (holder.e instanceof RemoteException) {
            throw (RemoteException) holder.e;
        } else if (holder.e instanceof RuntimeException) {
            throw (RuntimeException) holder.e;
        }

        return holder.result;
    }

    public static <R> R remoteExecute(Call<R> call) throws RemoteException {
        try {
            return call.run();
        } catch (RuntimeException e) {
            throw new RemoteException();
        }
    }

    public static void remoteExecute(VoidCall call) throws RemoteException {
        remoteExecute((Call<Void>) () -> {
            call.run();
            return null;
        });
    }
}

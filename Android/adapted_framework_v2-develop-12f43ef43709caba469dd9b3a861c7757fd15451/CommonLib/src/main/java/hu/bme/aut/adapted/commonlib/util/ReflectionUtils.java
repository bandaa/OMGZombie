package hu.bme.aut.adapted.commonlib.util;

import com.google.gson.JsonObject;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Name;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;
import hu.bme.aut.adapted.commonlib.annotation.RewardClass;
import hu.bme.aut.adapted.commonlib.ipc.payload.EventType;
import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.ParamType;
import hu.bme.aut.adapted.commonlib.ipc.payload.RewardType;

import static hu.bme.aut.adapted.commonlib.util.GsonHelper.gson;

public class ReflectionUtils {
    public static String getName(Class<?> clazz) {
        if (clazz.isAnnotationPresent(Name.class)) {
            return clazz.getAnnotation(Name.class).value();
        } else {
            return clazz.getSimpleName();
        }
    }

    private static String getTypeDescriptorName(Class<?> clazz) {
        return clazz != null ? clazz.getCanonicalName() : null;
    }

    private static String getDisplayName(AnnotatedElement element, String defaultValue) {
        if (element.isAnnotationPresent(DisplayName.class)) {
            return element.getAnnotation(DisplayName.class).value();
        } else {
            return defaultValue;
        }
    }

    private static String getDisplayName(Class<?> clazz) {
        return getDisplayName(clazz, clazz.getSimpleName());
    }

    private static String getDisplayName(Field field) {
        return getDisplayName(field, field.getName());
    }

    private static List<ParamType> createParamTypes(Class<?> clazz) {
        List<ParamType> paramTypes = new ArrayList<>();

        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Parameter.class)) {
                Parameter annotation = field.getAnnotation(Parameter.class);
                paramTypes.add(new ParamType(field.getName(), getDisplayName(field), getTypeDescriptorName(field.getType()), annotation.minValue(), annotation.maxValue(), false));
            }
        }

        return paramTypes;
    }

    public static Class<?> getRewardClass(Class<?> clazz) {
        if (clazz.isAnnotationPresent(RewardClass.class)) {
            return clazz.getAnnotation(RewardClass.class).value();
        } else {
            return null;
        }
    }

    public static JsonObject getParameters(Object object) {
        return gson.toJsonTree(object).getAsJsonObject();
    }

    public static EventType createEventType(Class<?> clazz, Integer rewardTypeId) {
        return new EventType(getTypeDescriptorName(clazz), getDisplayName(clazz), createParamTypes(clazz), false, rewardTypeId);
    }

    public static RewardType createRewardType(Class<?> clazz) {
        return new RewardType(getTypeDescriptorName(clazz), getDisplayName(clazz), createParamTypes(clazz), false);
    }

    public static GameEvent createGameEvent(Object object, long timeStamp, int eventTypeId) {
        // TODO fail-fast hibakezelés, ha az "object" paraméter egy JSON-primitív típus (int, String, stb.)
        // TODO Jelenleg IllegalStateException-t dob
        return new GameEvent(timeStamp, 0, eventTypeId, getParameters(object));
    }
}

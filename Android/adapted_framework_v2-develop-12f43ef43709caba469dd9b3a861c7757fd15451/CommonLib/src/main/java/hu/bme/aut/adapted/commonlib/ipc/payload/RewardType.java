package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;

import java.util.List;

public class RewardType extends TypeDescriptor {
    public RewardType(String name, String displayName, List<ParamType> paramTypes, boolean dynamic) {
        super(name, displayName, paramTypes, dynamic);
    }

    protected RewardType(Parcel in) {
        super(in);
    }

    public static final Creator<RewardType> CREATOR = new Creator<RewardType>() {
        @Override
        public RewardType createFromParcel(Parcel in) {
            return new RewardType(in);
        }

        @Override
        public RewardType[] newArray(int size) {
            return new RewardType[size];
        }
    };
}

package hu.bme.aut.adapted.commonlib.ipc.payload;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public abstract class TypeDescriptor implements Parcelable {
    private String name;
    private String displayName;
    private List<ParamType> paramTypes;
    private boolean dynamic;

    protected TypeDescriptor(String name, String displayName, List<ParamType> paramTypes, boolean dynamic) {
        this.name = name;
        this.displayName = displayName;
        this.paramTypes = paramTypes;
        this.dynamic = dynamic;
    }

    protected TypeDescriptor(Parcel in) {
        name = in.readString();
        displayName = in.readString();
        paramTypes = in.createTypedArrayList(ParamType.CREATOR);
        dynamic = in.readByte() != 0;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public List<ParamType> getParamTypes() {
        return paramTypes;
    }

    public boolean isDynamic() {
        return dynamic;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(displayName);
        dest.writeTypedList(paramTypes);
        dest.writeByte((byte) (dynamic ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }
}

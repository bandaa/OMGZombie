package hu.bme.aut.adapted.commonlib.util;

public abstract class Event {
    private Throwable throwable;

    public Event() {
    }

    public Event(Throwable throwable) {
        this.throwable = throwable;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}

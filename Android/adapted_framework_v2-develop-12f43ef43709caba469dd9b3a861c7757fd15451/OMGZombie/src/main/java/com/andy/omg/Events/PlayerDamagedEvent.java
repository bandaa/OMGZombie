package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Player damaged")
public class PlayerDamagedEvent {

    @Parameter(minValue = "1", maxValue = "100")
    @DisplayName("Before")
    final float beforeHealt;

    @Parameter(minValue = "1", maxValue = "20")
    @DisplayName("Loss")
    final float healtLoss;

    @Parameter(minValue = "0", maxValue = "99")
    @DisplayName("After")
    final float afterHealt;

    public PlayerDamagedEvent(float beforeHealt, float healtLoss, float afterHealt) {
        this.beforeHealt = beforeHealt;
        this.healtLoss = healtLoss;
        this.afterHealt = afterHealt;
    }

    public float getBeforeHealt() {
        return beforeHealt;
    }

    public float getHealtLoss() {
        return healtLoss;
    }

    public float getAfterHealt() {
        return afterHealt;
    }
}

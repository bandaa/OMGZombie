package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Waypoint reached")
public class WayPointReachedEvent {

    @Parameter
    @DisplayName("Position")
    final String position;

    @Parameter
    @DisplayName("Name")
    final String name;

    public WayPointReachedEvent(String position, String name) {
        this.position = position;
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }
}

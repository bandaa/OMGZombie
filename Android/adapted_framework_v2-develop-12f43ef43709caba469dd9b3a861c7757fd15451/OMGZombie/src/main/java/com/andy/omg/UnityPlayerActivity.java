package com.andy.omg;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;

import com.andy.omg.Events.DrawingFaultEvent;
import com.andy.omg.Events.FrequentPointerClickEvent;
import com.andy.omg.Events.JumpScareEvent;
import com.andy.omg.Events.MissionCompletedEvent;
import com.andy.omg.Events.MissionFailedEvent;
import com.andy.omg.Events.MissionStartedEvent;
import com.andy.omg.Events.PlayerDamagedEvent;
import com.andy.omg.Events.PlayerDieEvent;
import com.andy.omg.Events.UniqueEvent;
import com.andy.omg.Events.WayPointReachedEvent;
import com.unity3d.player.UnityPlayer;

import hu.bme.aut.adapted.authcommlib.annotation.RequireLogin;
import hu.bme.aut.adapted.gamelib.annotation.HasDashboard;
import hu.bme.aut.adapted.gamelib.ui.base.GameBase;

@HasDashboard
@RequireLogin(securitylevel = RequireLogin.SecurityLevel.REQUIRED, role = RequireLogin.Role.USER)
public class UnityPlayerActivity extends GameBase
{
    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
    private OmgEventHandler omgHandler;
    //private Timer timer;

    // Setup activity layout
    @Override protected void onCreate (Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

        mUnityPlayer = new UnityPlayer(this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();

        omgHandler = new OmgEventHandler();

        addSensorListener(omgHandler);
/*
        //test
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                omgHandler.meditationChanged(new Random().nextInt(100) + 1);
            }
        }, 0, new Random().nextInt(10000)+5000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                omgHandler.heartRateChanged(new Random().nextInt(100) + 1);
            }
        }, 0, new Random().nextInt(10000)+5000);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                omgHandler.attentionChanged(new Random().nextInt(100) + 1);
            }
        }, 0, new Random().nextInt(10000)+5000);
        */
    }

    @Override protected void onNewIntent(Intent intent)
    {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    // Quit Unity
    @Override protected void onDestroy ()
    {
        mUnityPlayer.quit();
        //removeSensorListener(omgHandler);
        Log.d("test", "destoryed");
        //timer.cancel();
        //timer.purge();
        super.onDestroy();
    }

    // Pause Unity
    @Override protected void onPause()
    {
        super.onPause();
        mUnityPlayer.pause();

    }

    // Resume Unity
    @Override protected void onResume()
    {
        super.onResume();
        mUnityPlayer.resume();
    }

    // Low Memory Unity
    @Override public void onLowMemory()
    {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    // Trim Memory Unity
    @Override public void onTrimMemory(int level)
    {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL)
        {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }

    @Override
    public void frameworkInitialized() {
        startGameplay();
    }

    /*unity event handlers*/

    public void wayPointReached(String position, String name){
        sendGameEvent(new WayPointReachedEvent(position, name));
    }

    public void jumpScareVoice(String name){
        sendGameEvent(new JumpScareEvent(name));
    }

    public void playerDamaged(float healthLoss, float healthLeft){
        sendGameEvent(new PlayerDamagedEvent(healthLeft + healthLoss, healthLoss, healthLeft));
    }

    public void playerDead(String position, String name){
        sendGameEvent(new PlayerDieEvent(position, name));
    }

    public void drawingFault(String direction, String shape){
        sendGameEvent(new DrawingFaultEvent(direction, shape));
    }

    public void missionStarted(String mission){
        sendGameEvent(new MissionStartedEvent(mission));
    }

    public void missionCompleted(String mission){
        sendGameEvent(new MissionCompletedEvent(mission));
    }

    public void missionFailed(String mission, int zombiesKilled){
        sendGameEvent(new MissionFailedEvent(mission, zombiesKilled));
    }

    public void frequentPointerClick(int limit, int duration){
        String detailsString = "User clicked more than " + limit + " within " + duration + " seconds";
        sendGameEvent(new FrequentPointerClickEvent(detailsString));
    }

    public void uniqueEvent(String event){
        sendGameEvent(new UniqueEvent(event));
    }


}

package com.andy.omg;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Andy on 2017. 10. 31..
 */
public class OmgApplication  extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        GameLibDaggerWrapper.initInjector(this, 132);
    }
}

package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Jump scare")
public class JumpScareEvent {

    @Parameter
    @DisplayName("Name")
    final String name;

    public JumpScareEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}

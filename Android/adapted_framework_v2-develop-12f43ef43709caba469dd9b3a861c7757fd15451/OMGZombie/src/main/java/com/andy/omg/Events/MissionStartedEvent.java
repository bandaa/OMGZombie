package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Mission started")
public class MissionStartedEvent  {

    @Parameter
    @DisplayName("Mission")
    final String mission;

    public MissionStartedEvent(String mission) {
        this.mission = mission;
    }

    public String getMission() {
        return mission;
    }
}

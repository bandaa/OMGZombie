package com.andy.omg;

import android.util.Log;

import com.unity3d.player.UnityPlayer;

import hu.bme.aut.adapted.commonlib.util.Constants;
import hu.bme.aut.adapted.gamelib.util.SensorListener;


public class OmgEventHandler implements SensorListener {

    @Override
    public void sensorStatusChanged(String name, Constants.Broadcast.Sensors.Status status) {
        Log.d("sensor", "status changed:" + name + status);
        UnityPlayer.UnitySendMessage("AdaptedEventHandler", "setSensorStatus", status.toString());
    }

    @Override
    public void attentionChanged(int value) {
        Log.d("sensor", "attention changed" + value);
        UnityPlayer.UnitySendMessage("AdaptedEventHandler", "setAttention", value+"");

    }

    @Override
    public void meditationChanged(int value) {
        float fear = (1 - ((float)value / 100)) * 100;
        Log.d("sensor", "meditation changed" + fear);
        UnityPlayer.UnitySendMessage("AdaptedEventHandler", "setFearLevel", fear+"");
    }

    @Override
    public void heartRateChanged(int value) {
        Log.d("sensor", "heart rate changed" + value);
        UnityPlayer.UnitySendMessage("AdaptedEventHandler", "setHeartRate", value+"");

    }

}

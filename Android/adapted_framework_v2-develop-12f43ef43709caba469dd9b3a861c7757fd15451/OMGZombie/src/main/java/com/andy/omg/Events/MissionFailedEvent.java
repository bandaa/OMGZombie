package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Mission failed")
public class MissionFailedEvent {

    @Parameter
    @DisplayName("Mission name")
    final String mission;

    @Parameter
    final int zombiesKilled;

    public MissionFailedEvent(String mission, int zombiesKilled) {
        this.mission = mission;
        this.zombiesKilled = zombiesKilled;
    }

    public String getMission() {
        return mission;
    }

    public int getZombiesKilled() {
        return zombiesKilled;
    }
}

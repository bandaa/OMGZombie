package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Player die")
public class PlayerDieEvent {

    @Parameter
    @DisplayName("Position")
    final String position;

    @Parameter
    @DisplayName("Waypoint")
    final String waypoint;

    public PlayerDieEvent(String position, String waypoint) {
        this.position = position;
        this.waypoint = waypoint;
    }

    public String getPosition() {
        return position;
    }

    public String getWaypoint() {
        return waypoint;
    }
}

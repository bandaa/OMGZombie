package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Mission completed")
public class MissionCompletedEvent {

    @Parameter
    @DisplayName("Mission name")
    final String mission;

    public MissionCompletedEvent(String mission) {
        this.mission = mission;
    }

    public String getMission() {
        return mission;
    }
}

package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Frequent clicking")
public class FrequentPointerClickEvent {

    @Parameter
    final String details;

    public FrequentPointerClickEvent(String details) {
        this.details = details;
    }

    public String getDetails() {
        return details;
    }
}

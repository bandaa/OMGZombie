package com.andy.omg.Events;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Drawing fault")
public class DrawingFaultEvent {

    @Parameter
    @DisplayName("Fault direction")
    final String direction;

    @Parameter()
    @DisplayName("Shape")
    final String shapeForm;

    public DrawingFaultEvent(String direction, String shapeForm) {
        this.direction = direction;
        this.shapeForm = shapeForm;
    }

    public String getDirection() {
        return direction;
    }

    public String getShapeForm() {
        return shapeForm;
    }
}

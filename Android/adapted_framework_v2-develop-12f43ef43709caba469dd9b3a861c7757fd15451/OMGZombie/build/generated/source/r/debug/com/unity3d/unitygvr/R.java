/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.unity3d.unitygvr;

public final class R {
    public static final class color {
        public static final int alignment_marker_color = 0x7f0d0007;
        public static final int white_transparent = 0x7f0d0055;
    }
    public static final class dimen {
        public static final int alignment_marker_height = 0x7f080063;
        public static final int alignment_marker_thickness = 0x7f080064;
        public static final int transition_bottom_bar_height = 0x7f08001a;
    }
    public static final class drawable {
        public static final int quantum_ic_close_white_24 = 0x7f020087;
        public static final int quantum_ic_settings_white_24 = 0x7f020088;
        public static final int rippleable = 0x7f020089;
        public static final int transition = 0x7f02008e;
        public static final int vr_icon_back = 0x7f02008f;
        public static final int vr_icon_front = 0x7f020090;
    }
    public static final class id {
        public static final int back_button = 0x7f0e00de;
        public static final int divider = 0x7f0e00e4;
        public static final int transition_bottom_frame = 0x7f0e00e2;
        public static final int transition_frame = 0x7f0e00dd;
        public static final int transition_icon = 0x7f0e00e0;
        public static final int transition_question_text = 0x7f0e00e3;
        public static final int transition_switch_action = 0x7f0e00e5;
        public static final int transition_text = 0x7f0e00e1;
        public static final int transition_top_frame = 0x7f0e00df;
        public static final int ui_alignment_marker = 0x7f0e00e7;
        public static final int ui_back_button = 0x7f0e00e6;
        public static final int ui_settings_button = 0x7f0e00e8;
    }
    public static final class layout {
        public static final int back_button = 0x7f03001d;
        public static final int settings_button = 0x7f030042;
        public static final int transition_view = 0x7f030045;
        public static final int ui_layer = 0x7f030046;
        public static final int ui_layer_with_portrait_support = 0x7f030047;
    }
    public static final class string {
        public static final int app_name = 0x7f07007c;
        public static final int back_button_content_description = 0x7f070026;
        public static final int cancel_button = 0x7f070027;
        public static final int dialog_button_got_it = 0x7f070028;
        public static final int dialog_button_open_help_center = 0x7f070029;
        public static final int dialog_message_incompatible_phone = 0x7f07002a;
        public static final int dialog_message_no_cardboard = 0x7f07002b;
        public static final int dialog_title = 0x7f07002c;
        public static final int dialog_title_incompatible_phone = 0x7f07002d;
        public static final int dialog_title_vr_core_not_enabled = 0x7f07002e;
        public static final int dialog_title_vr_core_not_installed = 0x7f07002f;
        public static final int dialog_title_warning = 0x7f070030;
        public static final int dialog_vr_core_not_enabled = 0x7f070031;
        public static final int dialog_vr_core_not_installed = 0x7f070032;
        public static final int go_to_playstore_button = 0x7f070033;
        public static final int go_to_vr_listeners_settings_button = 0x7f070034;
        public static final int gvr_vr_mode_component = 0x7f070086;
        public static final int no_browser_text = 0x7f070035;
        public static final int place_your_phone_into_cardboard = 0x7f070036;
        public static final int place_your_viewer_into_viewer_format = 0x7f070037;
        public static final int settings_button_content_description = 0x7f070038;
        public static final int setup_button = 0x7f070039;
        public static final int switch_viewer_action = 0x7f07003a;
        public static final int switch_viewer_prompt = 0x7f07003b;
    }
    public static final class style {
        public static final int GvrDialogTheme = 0x7f090083;
        public static final int NoSystemUI = 0x7f0900e5;
        public static final int UiButton = 0x7f09013e;
        public static final int VrActivityTheme = 0x7f09013f;
    }
}

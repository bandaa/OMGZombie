package hu.bme.aut.randomgame.test;


import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.runner.RunWith;

import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationActivity;
import hu.bme.aut.randomgame.util.EspressoTest;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class RegistrationTest extends EspressoTest<RegistrationActivity> {
    public RegistrationTest() {
        super(RegistrationActivity.class);
    }
    /*
    private int UserNameETId;
    private int PasswordETId;
    private int RegistrationButtonId;

    private Settings settings;

    public RegistrationTest() {
        super(RegistrationActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        setTestInjector();
        activityRule.launchActivity(new Intent());
        Activity activity = activityRule.getActivity();
        Store.init(activity.getApplicationContext());
        settings = new Settings();
        settings.deleteSessionKey();
        UserNameETId = R.id.UserNameET;
        PasswordETId = R.id.PasswordET;
        RegistrationButtonId = R.id.RegistrationButton;
    }

    @Test
    public void testSuccesfulRegistration() {
        onView(withId(UserNameETId)).perform(typeText(MockConstants.REGISTERED_USERNAME), closeSoftKeyboard());
        onView(withId(PasswordETId)).perform(typeText(MockConstants.REGISTERED_PASSWORD), closeSoftKeyboard());
        onView(withId(RegistrationButtonId)).perform(click());

        EspressoUtils.matchToolbarTitle(context.getString(R.string.app_name));
    }

    @Test
    public void testRegistrationFailure() {
        onView(withId(UserNameETId)).perform(typeText(MockConstants.UNREGISTERED_USERNAME), closeSoftKeyboard());
        onView(withId(PasswordETId)).perform(typeText(MockConstants.UNREGISTERED_PASSWORD), closeSoftKeyboard());
        onView(withId(RegistrationButtonId)).perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.registration_error)))
                .check(matches(isDisplayed()));
    }
    */
}


package hu.bme.aut.randomgame;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.authcommlib.ui.UIModule;
import hu.bme.aut.adapted.authcommlib.ui.login.LoginPresenter;
import hu.bme.aut.adapted.authcommlib.ui.registration.RegistrationPresenter;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;
import hu.bme.aut.adapted.commonlib.util.di.Network;

@Module
public class AndroidTestModule {
    private final UIModule UIModule;
    //private final NetworkModule networkModule;

    public AndroidTestModule() {

        this.UIModule = new UIModule();
        //this.networkModule = new NetworkModule();
    }

    @Provides
    public LoginPresenter provideLoginPresenter() {
        return UIModule.provideLoginPresenter();
    }

    @Provides
    public RegistrationPresenter provideRegistrationPresenter() {
        return UIModule.provideRegistrationPresenter();
    }

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    @Network
    public Executor provideNetworkExecutor() {
        //return networkModule.provideNetworkExecutor();
        // TODO
        return null;
    }

    @Provides
    @Singleton
    @Ipc
    public Looper provideIpcLooper() {
        // TODO
        return null;
    }

    @Provides
    @Ipc
    public Handler provideIpcHandler(@Ipc Looper looper) {
        // TODO
        return null;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        // TODO
        return null;
    }
}

package hu.bme.aut.randomgame;

import javax.inject.Singleton;

import dagger.Component;
import hu.bme.aut.adapted.authcommlib.AuthCommLibInjector;
import hu.bme.aut.adapted.authcommlib.interactor.InteractorModule;

@Singleton
//@Component(modules = {MockNetworkModule.class, AndroidTestModule.class, InteractorModule.class})
@Component(modules = {AndroidTestModule.class, InteractorModule.class})
public interface AndroidTestComponent extends AuthCommLibInjector {
}

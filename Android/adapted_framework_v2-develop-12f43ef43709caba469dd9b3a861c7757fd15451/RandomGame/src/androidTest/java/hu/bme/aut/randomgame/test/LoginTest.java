package hu.bme.aut.randomgame.test;


import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.runner.RunWith;

import hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity;
import hu.bme.aut.randomgame.util.EspressoTest;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginTest extends EspressoTest<LoginActivity> {
    public LoginTest() {
        super(LoginActivity.class);
    }

    /*
    private int UserNameETId;
    private int PasswordETId;
    private int LoginButtonId;

    private Settings settings;

    public LoginTest() {
        super(LoginActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        setTestInjector();
        activityRule.launchActivity(new Intent());
        Activity activity = activityRule.getActivity();
        Store.init(activity.getApplicationContext());
        settings = new Settings();
        settings.deleteSessionKey();
        UserNameETId = R.id.UserNameET;
        PasswordETId = R.id.PasswordET;
        LoginButtonId = R.id.LoginButton;
    }

    @Test
    public void testSuccesfulLogin() {
        onView(withId(UserNameETId)).perform(typeText(MockConstants.REGISTERED_USERNAME), closeSoftKeyboard());
        onView(withId(PasswordETId)).perform(typeText(MockConstants.REGISTERED_PASSWORD), closeSoftKeyboard());
        onView(withId(LoginButtonId)).perform(click());

        EspressoUtils.matchToolbarTitle(context.getString(R.string.app_name));
    }

    @Test
    public void testLoginFailure() {
        onView(withId(UserNameETId)).perform(typeText(MockConstants.UNREGISTERED_USERNAME), closeSoftKeyboard());
        onView(withId(PasswordETId)).perform(typeText(MockConstants.UNREGISTERED_PASSWORD), closeSoftKeyboard());
        onView(withId(LoginButtonId)).perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.login_error)))
                .check(matches(isDisplayed()));
    }
    */
}

package hu.bme.aut.randomgame.mock;


public class MockConstants {

    public static final String REGISTERED_USERNAME = "registered";
    public static final String REGISTERED_PASSWORD = "registered";

    public static final String UNREGISTERED_USERNAME = "unregistered";
    public static final String UNREGISTERED_PASSWORD = "unregistered";

    public static final String REGISTERED_SESSIONKEY = "registered111token222id333";

    //REGISTRATION VALUES
    public static final String EMAIL_VALID = "email@email.com";
    public static final String PHONE_VALID = "06123456789";
    public static final int SEX_VALID = 1;
    public static final String ZIPCODE_VALID = "1111";
    public static final String ZIPCODE_WRONG = "12345";
    public static final int BIRTHYEAR_VALID = 1990;
    public static final int BIRTHYEAR_WRONG_OLD = 1800;
    public static final int BIRTHYEAR_WRONG_YOUNG = 2016;
    public static final int BIRTHMONTH_VALID = 10;
    public static final int BIRTHDAY_VALID = 10;
}

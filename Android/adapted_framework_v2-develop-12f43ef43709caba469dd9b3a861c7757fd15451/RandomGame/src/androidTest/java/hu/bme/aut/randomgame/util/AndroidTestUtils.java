package hu.bme.aut.randomgame.util;


import hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper;
import hu.bme.aut.randomgame.AndroidTestComponent;
import hu.bme.aut.randomgame.AndroidTestModule;
import hu.bme.aut.randomgame.DaggerAndroidTestComponent;

public class AndroidTestUtils {
    public static void setTestInjector() {
        AndroidTestComponent androidTestComponent = DaggerAndroidTestComponent.builder().androidTestModule(new AndroidTestModule()).build();
        AuthCommLibDaggerWrapper.setInjector(androidTestComponent);
    }
}

package hu.bme.aut.randomgame.mock;


//@Module
public class MockNetworkModule {
    /*
    private NetworkModule networkModule = new NetworkModule();

    @Provides
    @Singleton
    public OkHttpClient.Builder provideOkHttpClientBuilder(final Settings settings) {
        return networkModule.provideOkHttpClientBuilder(settings);
    }


    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(OkHttpClient.Builder builder) {

        builder.interceptors().add(1, new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                return MockHttpServer.call(request);
            }
        });

        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient client) {
        return networkModule.provideRetrofit(client);
    }

    @Provides
    @Singleton
    public AuthApi provideAuthApi(Retrofit retrofit) {
        return networkModule.provideAuthApi(retrofit);
    }

    @Provides
    @Singleton
    public DataApi provideDataApi(Retrofit retrofit) {
        return networkModule.provideDataApi(retrofit);
    }

    // FIXME
    /*
    @Provides
    @Singleton
    public ClassificationApi provideClassificationApi(Retrofit retrofit) {
        return networkModule.provideClassificationApi(retrofit);
    }
    */
}

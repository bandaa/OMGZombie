package hu.bme.aut.randomgame;


import android.os.Bundle;
import android.widget.Button;

import hu.bme.aut.adapted.gamelib.annotation.HasDashboard;
import hu.bme.aut.adapted.authcommlib.annotation.RequireLogin;
import hu.bme.aut.adapted.gamelib.ui.base.GameBase;

@HasDashboard
@RequireLogin
public class MainActivity extends GameBase {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button sendTestEventButton = (Button) findViewById(R.id.btn_sendTestEvent);
        sendTestEventButton.setOnClickListener(v -> sendGameEvent(new TestEvent(6, "bla-bla")));

        Button startUploadButton = (Button) findViewById(R.id.btn_startUpload);
        startUploadButton.setOnClickListener(v -> startUpload());

        Button testButton = (Button) findViewById(R.id.btn_test);
        testButton.setOnClickListener(v -> test());
    }

    @Override
    public void frameworkInitialized() {
        startGameplay();
    }
}

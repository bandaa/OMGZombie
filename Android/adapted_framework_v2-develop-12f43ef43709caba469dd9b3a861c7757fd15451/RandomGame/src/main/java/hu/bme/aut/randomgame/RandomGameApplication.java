package hu.bme.aut.randomgame;

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper;
import io.fabric.sdk.android.Fabric;

public class RandomGameApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        GameLibDaggerWrapper.initInjector(this, 1);
    }
}

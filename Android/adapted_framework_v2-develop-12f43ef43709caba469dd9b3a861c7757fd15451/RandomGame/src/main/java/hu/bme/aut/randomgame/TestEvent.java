package hu.bme.aut.randomgame;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;
import hu.bme.aut.adapted.commonlib.annotation.RewardClass;

@DisplayName("Teszt esemény")
@RewardClass(TestReward.class)
public class TestEvent {
    @Parameter(minValue = "1", maxValue = "10")
    final int aNumber;

    @Parameter
    @DisplayName("Egy kis szöveg")
    final String someText;

    public TestEvent(int aNumber, String someText) {
        this.aNumber = aNumber;
        this.someText = someText;
    }

    public int getaNumber() {
        return aNumber;
    }

    public String getSomeText() {
        return someText;
    }
}

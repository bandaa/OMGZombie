package hu.bme.aut.randomgame;

import hu.bme.aut.adapted.commonlib.annotation.DisplayName;
import hu.bme.aut.adapted.commonlib.annotation.Parameter;

@DisplayName("Teszt jutalom")
public class TestReward {
    @Parameter(minValue = "1.0", maxValue = "5.0")
    @DisplayName("Érték")
    final double value;

    public TestReward(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}

package hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events;

import hu.bme.aut.adapted.commonlib.util.Event;

public class SendHeartRateEvent extends Event {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public SendHeartRateEvent() {
    }

    public SendHeartRateEvent(int value) {
        this.value = value;
    }
}

package hu.bme.aut.adapted.gamelib.util;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;

public interface SensorListener {
    void sensorStatusChanged(String name, Status status);

    void attentionChanged(int value);

    void meditationChanged(int value);

    void heartRateChanged(int value);
}

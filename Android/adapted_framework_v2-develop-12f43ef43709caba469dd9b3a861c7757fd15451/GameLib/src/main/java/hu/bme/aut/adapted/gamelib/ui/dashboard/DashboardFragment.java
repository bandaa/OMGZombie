package hu.bme.aut.adapted.gamelib.ui.dashboard;

import android.app.Fragment;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Classification.SVM;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.EmotivEEG;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.HXM;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.NeuroSkyEEG;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.gamelib.R;
import hu.bme.aut.adapted.gamelib.R2;

import static android.widget.Toast.LENGTH_SHORT;
import static hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper.injector;

public class DashboardFragment extends Fragment implements DashboardScreen {
    @Inject
    DashboardPresenter presenter;

    @BindView(R2.id.NeuroSkyStatus)
    ImageView neuroskyView;

    @BindView(R2.id.EmotivStatus)
    ImageView emotivView;

    @BindView(R2.id.HXMStatus)
    ImageView hxmView;

    @BindView(R2.id.classificationTrain)
    ImageView trainView;

    @BindView(R2.id.dashProgressAttention)
    ProgressBar attentionProgress;

    @BindView(R2.id.dashProgressMeditation)
    ProgressBar meditationProgress;

    @BindView(R2.id.dashProgressHeartRate)
    ProgressBar heartRateProgress;

    private Unbinder unbinder;

    private Map<String, Status> statusMap;

    private Status getStatus(String key) {
        Status status = statusMap.get(key);

        if (status == null) {
            return Status.OFFLINE;
        }

        return status;
    }

    private void setStatus(String key, Status status) {
        statusMap.put(key, status);
    }

    private interface Listener {
        void action();
    }

    private void addListener(String key, View view, Listener offlineListener, Listener onlineListener) {
        view.setOnLongClickListener(v -> {
            if (getActivity() != null) {
                Status status = getStatus(key);

                switch (status) {
                    case OFFLINE:
                        offlineListener.action();
                        break;
                    case ONLINE:
                        onlineListener.action();
                        break;
                    default:
                        break;
                }
            }

            return true;
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        injector.inject(this);

        statusMap = new HashMap<>();

        addListener(NeuroSkyEEG.ACTION_RESPONSE_STATUS, neuroskyView,
                presenter::connectNeurosky, presenter::disconnectNeurosky);

        addListener(EmotivEEG.ACTION_RESPONSE_STATUS, emotivView,
                presenter::connectEmotiv, presenter::disconnectEmotiv);

        addListener(HXM.ACTION_RESPONSE_STATUS, hxmView,
                presenter::connectZephyr, presenter::disconnectZephyr);

        updateAllStatus();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachScreen(this);
    }

    @Override
    public void onPause() {
        presenter.detachScreen();
        super.onPause();
    }

    public void updateSensorStatus() {
        presenter.updateSensorStatus();
    }

    private void updateAllStatus() {
        updateSensorStatusColor(neuroskyView, getStatus(NeuroSkyEEG.ACTION_RESPONSE_STATUS));
        updateSensorStatusColor(emotivView, getStatus(EmotivEEG.ACTION_RESPONSE_STATUS));
        updateSensorStatusColor(hxmView, getStatus(HXM.ACTION_RESPONSE_STATUS));
        updateSensorStatusColor(trainView, getStatus(SVM.ACTION_RESPONSE_STATUS));
    }

    private void updateSensorStatusColor(ImageView sensorView, Status status) {
        int backgroundResId;

        switch (status) {
            case ONLINE:
                backgroundResId = R.color.dashboard_online;
                break;
            case CONNECTING:
                backgroundResId = R.drawable.dashboard_connecting;
                break;
            case BAD_SIGNAL:
            case LOW_BATTERY:
                backgroundResId = R.color.dashboard_warning;
                break;
            default:
                backgroundResId = R.color.dashboard_offline;
                break;
        }

        sensorView.setBackgroundResource(backgroundResId);

        Drawable background = sensorView.getBackground();

        if (background instanceof AnimationDrawable) {
            ((AnimationDrawable) background).stop();
            ((AnimationDrawable) background).start();
        }
    }

    @Override
    public void sensorStatusChanged(String name, Status status) {
        String sensorName = null;
        ImageView imageView = null;

        switch (name) {
            case NeuroSkyEEG.ACTION_RESPONSE_STATUS:
                sensorName = NeuroSkyEEG.class.getSimpleName();
                imageView = neuroskyView;
                break;
            case EmotivEEG.ACTION_RESPONSE_STATUS:
                sensorName = EmotivEEG.class.getSimpleName();
                imageView = emotivView;
                break;
            case HXM.ACTION_RESPONSE_STATUS:
                sensorName = HXM.class.getSimpleName();
                imageView = hxmView;
                break;
        }

        if (imageView != null) {
            updateSensorStatusColor(imageView, status);
        }

        setStatus(name, status);

        if (sensorName != null) {
            Toast.makeText(getActivity(), sensorName + " status: " + status, LENGTH_SHORT).show();
        }
    }

    @Override
    public void attentionChanged(int value) {
        attentionProgress.setProgress(value);
    }

    @Override
    public void meditationChanged(int value) {
        meditationProgress.setProgress(value);
    }

    @Override
    public void heartRateChanged(int value) {
        heartRateProgress.setProgress(value);
    }
}

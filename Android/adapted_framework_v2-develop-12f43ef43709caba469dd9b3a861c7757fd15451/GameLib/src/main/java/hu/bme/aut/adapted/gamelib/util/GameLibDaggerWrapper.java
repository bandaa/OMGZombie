package hu.bme.aut.adapted.gamelib.util;

import android.content.Context;

import hu.bme.aut.adapted.authcommlib.util.AuthCommLibDaggerWrapper;
import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.gamelib.DaggerGameLibComponent;
import hu.bme.aut.adapted.gamelib.GameLibComponent;
import hu.bme.aut.adapted.gamelib.game.GameModule;

public class GameLibDaggerWrapper {
    public static GameLibComponent injector;

    public static void initInjector(Context context, int gameId) {
        injector = DaggerGameLibComponent.builder()
                .commonModule(new CommonModule(context))
                .gameModule(new GameModule(gameId))
                .build();

        // init injector for AuthCommLib
        AuthCommLibDaggerWrapper.setInjector(injector);
    }

    public static void setInjector(GameLibComponent gameLibComponent) {
        injector = gameLibComponent;
    }
}

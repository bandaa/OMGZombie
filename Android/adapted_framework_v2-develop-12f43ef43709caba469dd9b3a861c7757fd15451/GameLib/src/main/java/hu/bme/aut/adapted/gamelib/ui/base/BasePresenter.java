package hu.bme.aut.adapted.gamelib.ui.base;

import android.graphics.Bitmap;
import android.util.Log;

import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import hu.bme.aut.adapted.authcommlib.interactor.messenger.event.ServiceConnectedEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendScreenshotEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.StartGameplayEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.StopGameplayEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.TakeScreenshotEvent;
import hu.bme.aut.adapted.gamelib.ui.common.CommonPresenter;

import static android.graphics.Bitmap.CompressFormat.JPEG;
import static hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper.injector;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class BasePresenter extends CommonPresenter<BaseScreen> {
    private static final String TAG = "BasePresenter";

    private boolean initialized;
    private String sessionKey;
    private boolean anonymouslyLoggedIn;

    @Override
    public void attachScreen(BaseScreen screen) {
        super.attachScreen(screen);
        connect();
    }

    @Override
    public void detachScreen() {
        disconnect();
        super.detachScreen();
    }

    @Override
    protected final void inject() {
        injector.inject(this);
    }

    public void connect() {
        backgroundHandler.post(() -> gameMessengerInteractor.connect());
    }

    public void disconnect() {
        backgroundHandler.post(() -> {
            backgroundHandler.removeCallbacksAndMessages(null);
            gameMessengerInteractor.disconnect();
        });
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public void setAnonymouslyLoggedIn(boolean anonymouslyLoggedIn) {
        this.anonymouslyLoggedIn = anonymouslyLoggedIn;
    }

    public void sendScreenshot(Bitmap bitmap, boolean large) {
        backgroundHandler.post(() -> {
            byte[] screenshot = scaleScreenshot(bitmap, large);
            gameMessengerInteractor.sendScreenshot(screenshot, large);
        });
    }

    public byte[] scaleScreenshot(Bitmap screenshot, boolean large) {
        byte[] bytes;

        if (!large) {
            double rateX = 800.0 / screenshot.getWidth();
            double rateY = 600.0 / screenshot.getHeight();
            double rate = Math.min(rateX, rateY);

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(screenshot,
                    (int) (screenshot.getWidth() * rate),
                    (int) (screenshot.getHeight() * rate), false);
            bytes = processBitmap(scaledBitmap);
            screenshot.recycle();
        } else {
            bytes = processBitmap(screenshot);
        }

        return bytes;
    }

    private byte[] processBitmap(Bitmap bitmap) {
        ByteArrayOutputStream os = null;
        byte[] bytes;

        try {
            os = new ByteArrayOutputStream();
            bitmap.compress(JPEG, 100, os);
            bytes = os.toByteArray();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException ignored) {
                }
            }
        }

        bitmap.recycle();

        return bytes;
    }

    public void sendGameEvent(Object object) {
        long timeStamp = System.currentTimeMillis();
        backgroundHandler.post(() -> gameMessengerInteractor.sendGameEvent(object, timeStamp));
    }

    public void startGamePlay() {
        backgroundHandler.post(() -> gameMessengerInteractor.startGameplay(sessionKey));
    }

    public void stopGamePlay() {
        backgroundHandler.post(() -> gameMessengerInteractor.stopGameplay());
    }

    public void startUpload() {
        backgroundHandler.post(() -> gameMessengerInteractor.startUpload());
    }

    public void test() {
        backgroundHandler.post(() -> gameMessengerInteractor.test());
    }

    private void initialize(Settings settings) {
        if (screen != null) {
            screen.applySettings(settings);

            if (!initialized) {
                initialized = true;
                screen.frameworkInitialized();
            }

            screen.frameworkConnected2();
            screen.frameworkConnected();
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onServiceConnectedEvent(ServiceConnectedEvent event) {
        if (event.getThrowable() != null) {
            Log.e(TAG, "Failed to connect to framework service!");
            return;
        }

        if (sessionKey != null || anonymouslyLoggedIn) {
            initialize(event.getSettings());
        } else if (screen != null) {
            if (!screen.navigateToLogin()) {
                initialize(event.getSettings());
            }
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onSendScreenshotEvent(SendScreenshotEvent event) {
        if (screen != null) screen.screenshotFinished();
    }

    @Subscribe(threadMode = MAIN)
    public void onStartGameplayEvent(StartGameplayEvent event) {
        if (event.getThrowable() != null) {
            Log.e(TAG, "Failed to start gameplay!");
            return;
        }

        if (screen != null) screen.gameplayStarted();
    }

    @Subscribe(threadMode = MAIN)
    public void onStopGameplayEvent(StopGameplayEvent event) {
        if (event.getThrowable() != null) {
            Log.e(TAG, "Failed to stop gameplay!");
            return;
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onTakeScreenshotEvent(TakeScreenshotEvent event) {
        Log.d("SCREENSHOT", "\"take screenshot\" event received by presenter...");
        if (screen != null) screen.takeScreenshot();
    }
}

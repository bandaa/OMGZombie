package hu.bme.aut.adapted.gamelib.game;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GameModule {
    private final int gameId;

    public GameModule(int gameId) {
        this.gameId = gameId;
    }

    @Provides
    @Singleton
    @Named("gameId")
    public int provideGameId() {
        return gameId;
    }
}

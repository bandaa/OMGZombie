package hu.bme.aut.adapted.gamelib.ui.dashboard;

import hu.bme.aut.adapted.gamelib.ui.common.CommonPresenter;

import static hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper.injector;

public class DashboardPresenter extends CommonPresenter<DashboardScreen> {
    @Override
    public void attachScreen(DashboardScreen screen) {
        super.attachScreen(screen);
        addSensorListener(screen);
    }

    @Override
    public void detachScreen() {
        removeSensorListener(screen);
        super.detachScreen();
    }

    @Override
    protected final void inject() {
        injector.inject(this);
    }
}

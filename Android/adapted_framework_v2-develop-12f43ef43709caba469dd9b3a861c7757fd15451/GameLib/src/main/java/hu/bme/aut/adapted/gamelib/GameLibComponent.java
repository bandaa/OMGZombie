package hu.bme.aut.adapted.gamelib;

import javax.inject.Singleton;

import dagger.Component;
import hu.bme.aut.adapted.authcommlib.AuthCommLibInjector;
import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.gamelib.game.GameModule;
import hu.bme.aut.adapted.gamelib.interactor.InteractorModule;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.GameMessengerInteractor;
import hu.bme.aut.adapted.gamelib.registry.RegistryModule;
import hu.bme.aut.adapted.gamelib.ui.UIModule;
import hu.bme.aut.adapted.gamelib.ui.base.BasePresenter;
import hu.bme.aut.adapted.gamelib.ui.base.GameBase;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardFragment;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardPresenter;

@Singleton
@Component(modules = {CommonModule.class, UIModule.class, InteractorModule.class, RegistryModule.class, GameModule.class})
public interface GameLibComponent extends AuthCommLibInjector {
    void inject(GameBase gameBase);

    void inject(DashboardFragment dashboardFragment);

    void inject(BasePresenter basePresenter);

    void inject(DashboardPresenter dashboardPresenter);

    void inject(GameMessengerInteractor gameMessengerInteractor);
}

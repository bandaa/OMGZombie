package hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events;

import hu.bme.aut.adapted.commonlib.util.Event;

public class SendScreenshotEvent extends Event {
    private byte[] screenshot;
    private boolean large;

    public SendScreenshotEvent() {
        super();
    }

    public SendScreenshotEvent(int gameId, byte[] screenshot, boolean large, Throwable throwable) {
        super(throwable);
        this.screenshot = screenshot;
        this.large = large;
    }

    public byte[] getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(byte[] screenshot) {
        this.screenshot = screenshot;
    }

    public boolean isLarge() {
        return large;
    }

    public void setLarge(boolean large) {
        this.large = large;
    }
}

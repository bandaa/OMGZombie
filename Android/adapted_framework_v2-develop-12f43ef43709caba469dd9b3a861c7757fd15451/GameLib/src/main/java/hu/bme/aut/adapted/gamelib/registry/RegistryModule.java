package hu.bme.aut.adapted.gamelib.registry;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RegistryModule {
    @Provides
    @Singleton
    public Registry provideRegistry() {
        return new Registry();
    }
}

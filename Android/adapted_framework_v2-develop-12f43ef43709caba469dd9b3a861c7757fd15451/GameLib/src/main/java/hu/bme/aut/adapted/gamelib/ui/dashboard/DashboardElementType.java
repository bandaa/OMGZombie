package hu.bme.aut.adapted.gamelib.ui.dashboard;

/**
 * Created by dorka on 2016.04.06..
 */
public enum DashboardElementType {
    Gauge, Number, Text
}

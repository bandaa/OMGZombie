package hu.bme.aut.adapted.gamelib.ui.base;

import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;

public interface BaseScreen {
    boolean navigateToLogin();

    void startGameplay();

    void stopGameplay();

    void applySettings(Settings settings);

    void frameworkInitialized();

    void frameworkConnected2();

    void frameworkConnected();

    void gameplayStarted();

    void takeScreenshot();

    void screenshotFinished();
}

package hu.bme.aut.adapted.gamelib.interactor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.GameMessengerInteractor;

@Module
public class InteractorModule extends hu.bme.aut.adapted.authcommlib.interactor.InteractorModule {
    @Provides
    @Singleton
    public GameMessengerInteractor provideMessengerInteractor() {
        return new GameMessengerInteractor();
    }
}

package hu.bme.aut.adapted.gamelib.ui.dashboard;

/**
 * Created by dorka on 2016.04.06..
 */
public class DashboardElement {
    private String action;
    private String label;
    private DashboardElementType type;

    public DashboardElement(String action, String label, DashboardElementType type) {
        this.action = action;
        this.label = label;
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public String getLabel() {
        return label;
    }

    public DashboardElementType getType() {
        return type;
    }
}

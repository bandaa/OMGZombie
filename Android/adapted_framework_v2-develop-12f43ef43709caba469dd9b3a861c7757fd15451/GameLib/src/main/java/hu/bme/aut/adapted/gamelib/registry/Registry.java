package hu.bme.aut.adapted.gamelib.registry;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private final Map<Class<?>, Integer> eventTypeIds = new HashMap<>();
    private final Map<Class<?>, Integer> rewardTypeIds = new HashMap<>();

    Registry() {
    }

    public void addIdForEventClass(Class<?> clazz, int id) {
        eventTypeIds.put(clazz, id);
    }

    public void addIdForRewardClass(Class<?> clazz, int id) {
        rewardTypeIds.put(clazz, id);
    }

    public Integer getIdForEventClass(Class<?> clazz) {
        return eventTypeIds.get(clazz);
    }

    public Integer getIdForRewardClass(Class<?> clazz) {
        return rewardTypeIds.get(clazz);
    }
}

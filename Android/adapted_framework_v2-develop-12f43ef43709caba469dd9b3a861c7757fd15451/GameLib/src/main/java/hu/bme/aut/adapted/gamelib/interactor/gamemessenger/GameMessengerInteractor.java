package hu.bme.aut.adapted.gamelib.interactor.gamemessenger;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import javax.inject.Inject;
import javax.inject.Named;

import hu.bme.aut.adapted.authcommlib.interactor.messenger.MessengerInteractor;
import hu.bme.aut.adapted.authcommlib.interactor.messenger.event.ServiceConnectedEvent;
import hu.bme.aut.adapted.commonlib.exception.ServiceDisconnectedException;
import hu.bme.aut.adapted.commonlib.ipc.IFramework;
import hu.bme.aut.adapted.commonlib.ipc.payload.EventType;
import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.RewardType;
import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.gamelib.exception.NoGameplayStartedException;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendAttentionEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendHeartRateEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendMeditationEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendScreenshotEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SensorStatusChangedEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.StartGameplayEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.StopGameplayEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.TakeScreenshotEvent;
import hu.bme.aut.adapted.gamelib.ipc.FrameworkRemote;
import hu.bme.aut.adapted.gamelib.ipc.Game;
import hu.bme.aut.adapted.gamelib.registry.Registry;

import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createEventType;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createGameEvent;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createRewardType;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.getRewardClass;
import static hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper.injector;

public class GameMessengerInteractor extends MessengerInteractor<IFramework> {
    private static final String TAG = "GameLib";

    @Inject
    Registry registry;

    @Inject
    @Named("gameId")
    int gameId;

    private Integer gameplayId;

    private final Game game;

    public GameMessengerInteractor() {
        super(IFramework.class);
        game = new Game(backgroundHandler) {
            @Override
            public void takeScreenshot() {
                Log.d("SCREENSHOT", "\"take screenshot\" event received by interactor...");
                bus.post(new TakeScreenshotEvent());
            }

            @Override
            public void sensorStatusChanged(String name, String value) {
                bus.post(new SensorStatusChangedEvent(name, Status.valueOf(value)));
            }

            @Override
            public void sendAttention(int value) {
                bus.post(new SendAttentionEvent(value));
            }

            @Override
            public void sendMeditation(int value) {
                bus.post(new SendMeditationEvent(value));
            }

            @Override
            public void sendHeartRate(int value) {
                bus.post(new SendHeartRateEvent(value));
            }
        };
    }

    @Override
    protected void inject() {
        injector.inject(this);
    }

    @Override
    protected IFramework createInterface(IBinder service) {
        return new FrameworkRemote(service);
    }

    @Override
    protected void onConnect(ServiceConnectedEvent event) {
        try {
            Log.d("REGISTER", "registering game...");
            Settings settings = framework.registerGame(game, gameId);
            event.setSettings(settings);
        } catch (RemoteException e) {
            event.setThrowable(e);
        }
    }

    @Override
    protected void onDisconnect() {
        if (framework != null) {
            try {
                Log.d("REGISTER", "unregistering game...");
                framework.unregisterGame(game);
            } catch (RemoteException ignored) {
            }
        }
    }

    public void startGameplay(String sessionKey) {
        StartGameplayEvent event = new StartGameplayEvent();

        if (framework != null) {
            try {
                gameplayId = framework.startGameplay(gameId, sessionKey);
                event.setGameplayId(gameplayId);
            } catch (RemoteException e) {
                event.setThrowable(e);
            }
        } else {
            event.setThrowable(new ServiceDisconnectedException());
        }

        bus.post(event);
    }

    public void stopGameplay() {
        StopGameplayEvent event = new StopGameplayEvent();
        event.setGameplayId(gameplayId);

        if (framework != null) {
            try {
                framework.stopGameplay(gameId);
                gameplayId = null;
            } catch (RemoteException e) {
                event.setThrowable(e);
            }
        } else {
            event.setThrowable(new ServiceDisconnectedException());
        }

        bus.post(event);
    }

    public void sendScreenshot(byte[] screenshot, boolean large) {
        SendScreenshotEvent event = new SendScreenshotEvent();
        event.setScreenshot(screenshot);
        event.setLarge(large);

        if (gameplayId != null) {
            if (framework != null) {
                try {
                    framework.sendScreenshot(gameplayId, screenshot, large);
                } catch (RemoteException e) {
                    event.setThrowable(e);
                }
            } else {
                event.setThrowable(new ServiceDisconnectedException());
            }
        } else {
            event.setThrowable(new NoGameplayStartedException());
        }

        bus.post(event);
    }

    public void sendGameEvent(Object object, long timeStamp) {
        if (gameplayId != null) {
            if (framework != null) {
                try {
                    Class<?> eventClass = object.getClass();
                    int eventTypeId = getEventTypeId(eventClass);
                    GameEvent event = createGameEvent(object, timeStamp, eventTypeId);
                    framework.sendGameEvent(gameplayId, event);

                    Log.d(TAG, "successfully sent event: " + eventClass.getSimpleName());
                } catch (RemoteException e) {
                    Log.e(TAG, "sending event: RemoteException");
                }
            } else {
                Log.e(TAG, "sending event: framework is null");
            }
        } else {
            Log.e(TAG, "sending event: no gameplay started");
        }
    }

    private int getEventTypeId(Class<?> eventClass) throws RemoteException {
        Integer eventTypeId = registry.getIdForEventClass(eventClass);

        if (eventTypeId == null) {
            Class<?> rewardClass = getRewardClass(eventClass);

            Integer rewardTypeId;

            if (rewardClass != null) {
                rewardTypeId = registry.getIdForRewardClass(rewardClass);

                if (rewardTypeId == null) {
                    rewardTypeId = registerRewardType(rewardClass);
                }
            } else {
                rewardTypeId = null;
            }

            eventTypeId = registerEventType(eventClass, rewardTypeId);
        }

        return eventTypeId;
    }

    private int registerEventType(Class<?> clazz, Integer rewardTypeId) throws RemoteException {
        EventType eventType = createEventType(clazz, rewardTypeId);
        int id = framework.registerEventType(gameId, eventType);
        registry.addIdForEventClass(clazz, id);
        return id;
    }

    private int registerRewardType(Class<?> clazz) throws RemoteException {
        RewardType rewardType = createRewardType(clazz);
        int id = framework.registerRewardType(gameId, rewardType);
        registry.addIdForRewardClass(clazz, id);
        return id;
    }

    public void connectNeurosky() {
        try {
            framework.connectNeurosky();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void disconnectNeurosky() {
        try {
            framework.disconnectNeurosky();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void connectZephyr() {
        try {
            framework.connectZephyr();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void disconnectZephyr() {
        try {
            framework.disconnectZephyr();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void connectEmotiv() {
        try {
            framework.connectEmotiv();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void disconnectEmotiv() {
        try {
            framework.disconnectEmotiv();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void updateSensorStatus() {
        try {
            framework.updateSensorStatus();
        } catch (RemoteException e) {
            // TODO
        }
    }

    public void startUpload() {
        if (framework != null) {
            try {
                framework.startUpload();
            } catch (RemoteException ignored) {
            }
        }
    }

    public void test() {
        Log.d(TAG, "test started");

        if (framework != null) {
            try {
                Bundle bundle = new Bundle();
                boolean result = framework.test(bundle);
                Log.d(TAG, "test " + (result ? "success" : "error"));
            } catch (RemoteException e) {
                Log.e(TAG, "test error: RemoteException");
            }
        } else {
            Log.e(TAG, "test error: framework is null");
        }

        Log.d(TAG, "test ended");
    }
}

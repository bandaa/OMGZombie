package hu.bme.aut.adapted.gamelib.ui;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.gamelib.ui.base.BasePresenter;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardPresenter;

@Module
public class UIModule extends hu.bme.aut.adapted.authcommlib.ui.UIModule {
    @Provides
    @Singleton
    public BasePresenter provideBasePresenter() {
        return new BasePresenter();
    }

    @Provides
    @Singleton
    public DashboardPresenter provideDashboardPresenter() {
        return new DashboardPresenter();
    }
}

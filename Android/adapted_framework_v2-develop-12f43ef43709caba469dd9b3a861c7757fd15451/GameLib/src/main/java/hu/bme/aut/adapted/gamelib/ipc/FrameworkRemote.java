package hu.bme.aut.adapted.gamelib.ipc;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import hu.bme.aut.adapted.commonlib.ipc.IFramework;
import hu.bme.aut.adapted.commonlib.ipc.IGame;
import hu.bme.aut.adapted.commonlib.ipc.payload.EventType;
import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.RewardType;
import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;

import static hu.bme.aut.adapted.commonlib.util.RemoteHelper.remoteExecute;

public class FrameworkRemote implements IFramework {
    private IFramework target;

    public FrameworkRemote(IBinder target) {
        this.target = IFramework.Stub.asInterface(target);
    }

    @Override
    public Settings registerGame(IGame game, int gameId) throws RemoteException {
        return remoteExecute(() -> target.registerGame(game, gameId));
    }

    @Override
    public void unregisterGame(IGame game) throws RemoteException {
        remoteExecute(() -> target.unregisterGame(game));
    }

    @Override
    public int startGameplay(int gameId, String sessionKey) throws RemoteException {
        return remoteExecute(() -> target.startGameplay(gameId, sessionKey));
    }

    @Override
    public void stopGameplay(int gameId) throws RemoteException {
        remoteExecute(() -> target.stopGameplay(gameId));
    }

    @Override
    public int registerEventType(int gameId, EventType eventType) throws RemoteException {
        return remoteExecute(() -> target.registerEventType(gameId, eventType));
    }

    @Override
    public int registerRewardType(int gameId, RewardType rewardType) throws RemoteException {
        return remoteExecute(() -> target.registerRewardType(gameId, rewardType));
    }

    @Override
    public void sendGameEvent(int gameplayId, GameEvent event) throws RemoteException {
        remoteExecute(() -> target.sendGameEvent(gameplayId, event));
    }

    @Override
    public void sendScreenshot(int gameplayId, byte[] screenshot, boolean large) throws RemoteException {
        remoteExecute(() -> target.sendScreenshot(gameplayId, screenshot, large));
    }

    @Override
    public void connectNeurosky() throws RemoteException {
        remoteExecute(() -> target.connectNeurosky());
    }

    @Override
    public void disconnectNeurosky() throws RemoteException {
        remoteExecute(() -> target.disconnectNeurosky());
    }

    @Override
    public void connectZephyr() throws RemoteException {
        remoteExecute(() -> target.connectZephyr());
    }

    @Override
    public void disconnectZephyr() throws RemoteException {
        remoteExecute(() -> target.disconnectZephyr());
    }

    public void connectEmotiv() throws RemoteException {
        remoteExecute(() -> target.connectEmotiv());
    }

    @Override
    public void disconnectEmotiv() throws RemoteException {
        remoteExecute(() -> target.disconnectEmotiv());
    }

    @Override
    public void updateSensorStatus() throws RemoteException {
        remoteExecute(() -> target.updateSensorStatus());
    }

    @Override
    public void startUpload() throws RemoteException {
        remoteExecute(() -> target.startUpload());
    }

    @Override
    public boolean test(Bundle bundle) throws RemoteException {
        return remoteExecute(() -> target.test(bundle));
    }

    @Override
    public IBinder asBinder() {
        return target.asBinder();
    }
}

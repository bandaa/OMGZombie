package hu.bme.aut.adapted.gamelib.ui.base;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;

import javax.inject.Inject;

import hu.bme.aut.adapted.authcommlib.annotation.RequireLogin;
import hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity;
import hu.bme.aut.adapted.commonlib.ipc.payload.Settings;
import hu.bme.aut.adapted.gamelib.R;
import hu.bme.aut.adapted.gamelib.annotation.HasDashboard;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardElement;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardElementType;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardFragment;
import hu.bme.aut.adapted.gamelib.ui.dashboard.DashboardLayout;
import hu.bme.aut.adapted.gamelib.util.SensorListener;
import io.fabric.sdk.android.Fabric;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static android.os.Build.VERSION_CODES.M;
import static android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
import static android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
import static hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity.EXTRA_LOGIN_ANONYMOUS;
import static hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity.EXTRA_LOGIN_ROLE;
import static hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity.EXTRA_LOGIN_SECURITY_LEVEL;
import static hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity.EXTRA_LOGIN_SESSION_KEY;
import static hu.bme.aut.adapted.authcommlib.ui.login.LoginActivity.REQUEST_CODE_LOGIN;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Classification.SVM.ACTION_SVM;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.MentalState.ACTION_ATTENTION;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.MentalState.ACTION_MEDITATION;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.MentalState.ACTION_SUCCESS;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Physiology.ACTION_HEARTRATE;
import static hu.bme.aut.adapted.commonlib.util.DeviceInfoUtils.deviceHasSoftKeys;
import static hu.bme.aut.adapted.gamelib.util.GameLibDaggerWrapper.injector;

public abstract class GameBase extends AppCompatActivity implements BaseScreen {
    private View rootView;
    private View contentView;
    private View dashboardView;
    private ViewGroup container;
    private boolean hasDashboard;
    private DashboardFragment dashboardFragment;

    private boolean hasSoftKeys;
    // TODO megkapni ezt az infot a supervisortól!
    private boolean sendLargeScreenshots = false;

    @Inject
    BasePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injector.inject(this);

        Fabric.with(this, new Crashlytics());

        hasSoftKeys = deviceHasSoftKeys(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        rootView = findViewById(android.R.id.content).getRootView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attachScreen(this);
    }

    @Override
    protected void onPause() {
        presenter.detachScreen();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_LOGIN && resultCode == RESULT_OK && data != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                /***Android 6.0 and higher need to request permission*****/
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            0);
                }
            }
            if (data.getBooleanExtra(EXTRA_LOGIN_ANONYMOUS, false)) {
                presenter.setAnonymouslyLoggedIn(true);
            } else {
                presenter.setSessionKey(data.getStringExtra(EXTRA_LOGIN_SESSION_KEY));
            }
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layoutResID, null);
        setContentView(view);
    }

    @Override
    public void setContentView(View view) {
        Toast.makeText(this, "CONTENT VIEW SET", Toast.LENGTH_SHORT).show();

        super.setContentView(R.layout.container);

        container = ((ViewGroup) findViewById(R.id.Container));

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

        if (getClass().isAnnotationPresent(HasDashboard.class)) {
            HasDashboard annotation = getClass().getAnnotation(HasDashboard.class);
            HasDashboard.Visibility visibility = annotation.value();
            int defaultVisibility = (visibility.equals(HasDashboard.Visibility.VISIBLE)) ?
                    View.VISIBLE : View.GONE;

            DashboardLayout dashboardRootView = (DashboardLayout) getLayoutInflater()
                    .inflate(R.layout.layout_dashboard_container, null);

            dashboardView = dashboardRootView
                    .findViewById(R.id.DashboardContent);

            dashboardView.setVisibility(defaultVisibility);

            dashboardRootView.addView(view, 1);
            contentView = dashboardRootView;

            hasDashboard = true;

            HashMap<String, DashboardElement> dashboardItems = new HashMap<String, DashboardElement>();
            getDashboardItems(dashboardItems);
            dashboardFragment = (DashboardFragment) getFragmentManager()
                    .findFragmentByTag("dashboardFragment");
        } else {
            contentView = view;
            hasDashboard = false;
        }

        container.addView(contentView, 0, layoutParams);
    }

    protected void getDashboardItems(HashMap<String, DashboardElement> items) {
        items.put(ACTION_ATTENTION,
                new DashboardElement(ACTION_ATTENTION, getResources().getString(R.string.dashboard_attention), DashboardElementType.Gauge));
        items.put(ACTION_MEDITATION,
                new DashboardElement(ACTION_MEDITATION, getResources().getString(R.string.dashboard_meditation), DashboardElementType.Gauge));
        items.put(ACTION_SUCCESS,
                new DashboardElement(ACTION_SUCCESS, getResources().getString(R.string.dashboard_success), DashboardElementType.Gauge));
        items.put(ACTION_HEARTRATE,
                new DashboardElement(ACTION_HEARTRATE, getResources().getString(R.string.dashboard_heartrate), DashboardElementType.Gauge));
        items.put(ACTION_SVM,
                new DashboardElement(ACTION_SVM, getResources().getString(R.string.dashboard_svm), DashboardElementType.Number));
    }

    @Override
    public final boolean navigateToLogin() {
        boolean hasAnnotation = getClass().isAnnotationPresent(RequireLogin.class);

        if (hasAnnotation) {
            RequireLogin annotation = getClass().getAnnotation(RequireLogin.class);

            Intent intent = new Intent(this, LoginActivity.class)
                    .putExtra(EXTRA_LOGIN_SECURITY_LEVEL, annotation.securitylevel())
                    .putExtra(EXTRA_LOGIN_ROLE, annotation.role());

            startActivityForResult(intent, REQUEST_CODE_LOGIN);
        }

        return hasAnnotation;
    }

    @Override
    public void frameworkInitialized() {
    }

    @Override
    public final void frameworkConnected2() {
        if (hasDashboard) {
            dashboardFragment.updateSensorStatus();
        }
    }

    @Override
    public void frameworkConnected() {
    }

    @Override
    public void gameplayStarted() {
    }

    @Override
    public void takeScreenshot() {
        Log.d("SCREENSHOT", "\"take screenshot\" event received by activity");

        rootView.setDrawingCacheEnabled(true);
        Bitmap bitmap = rootView.getDrawingCache();
        presenter.sendScreenshot(bitmap, sendLargeScreenshots);
    }

    @Override
    public final void screenshotFinished() {
        rootView.setDrawingCacheEnabled(false);
    }

    public final void sendGameEvent(Object object) {
        presenter.sendGameEvent(object);
    }

    public final void startUpload() {
        presenter.startUpload();
    }

    public final void test() {
        presenter.test();
    }

    @Override
    public final void startGameplay() {
        presenter.startGamePlay();
    }

    @Override
    public final void stopGameplay() {
        presenter.stopGamePlay();
    }

    @Override
    public final void applySettings(Settings settings) {
        applyLockTaskModeEnabledSetting(settings.isLockTaskModeEnabled());
        applyWakelockSetting(settings.isWakelock());
        applyHideNavigationBarSetting(settings.isHideNavigationBar());
        applyFullBrightnessSetting(settings.isFullBrightness());
        applyHideActionBarSetting(settings.isHideActionBar());
    }

    public final void connectNeurosky() {
        presenter.connectNeurosky();
    }

    public final void disconnectNeurosky() {
        presenter.disconnectNeurosky();
    }

    public final void connectZephyr() {
        presenter.connectZephyr();
    }

    public final void disconnectZephyr() {
        presenter.disconnectZephyr();
    }

    public final void addSensorListener(SensorListener listener) {
        presenter.addSensorListener(listener);
    }

    public final void removeSensorListener(SensorListener listener) {
        presenter.removeSensorListener(listener);
    }

    private void applyLockTaskModeEnabledSetting(boolean lockTaskModeEnabled) {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        if (SDK_INT >= M) {
            if (lockTaskModeEnabled && am.getLockTaskModeState() == ActivityManager.LOCK_TASK_MODE_NONE) {
                startLockTask();
            } else if (!lockTaskModeEnabled && am.getLockTaskModeState() != ActivityManager.LOCK_TASK_MODE_NONE) {
                stopLockTask();
            }
        } else if (SDK_INT >= LOLLIPOP) {
            if (lockTaskModeEnabled && !am.isInLockTaskMode()) {
                startLockTask();
            } else if (!lockTaskModeEnabled && am.isInLockTaskMode()) {
                stopLockTask();
            }
        }
    }

    private void applyWakelockSetting(boolean wakelock) {
        if (wakelock) {
            getWindow().addFlags(FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(FLAG_KEEP_SCREEN_ON);
        }
    }

    private void applyHideNavigationBarSetting(boolean hideNavigationBar) {
        if (hasSoftKeys) {
            View decorView = getWindow().getDecorView();
            int visibility = decorView.getSystemUiVisibility();

            if (hideNavigationBar) {
                if ((visibility & SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                    decorView.setSystemUiVisibility(visibility | SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                }

                decorView.setOnSystemUiVisibilityChangeListener(newVisibility -> {
                    if ((newVisibility & SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
                        decorView.setSystemUiVisibility(newVisibility | SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    }
                });
            } else {
                if ((visibility & SYSTEM_UI_FLAG_HIDE_NAVIGATION) != 0) {
                    decorView.setSystemUiVisibility(visibility & ~SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                }

                decorView.setOnSystemUiVisibilityChangeListener(newVisibility -> {
                    if ((newVisibility & SYSTEM_UI_FLAG_HIDE_NAVIGATION) != 0) {
                        decorView.setSystemUiVisibility(newVisibility & ~SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                    }
                });
            }
        }
    }

    private void applyFullBrightnessSetting(boolean fullBrightness) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = fullBrightness ? 1 : -1;
        getWindow().setAttributes(lp);
    }

    private void applyHideActionBarSetting(boolean hideActionBar) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            if (hideActionBar) {
                actionBar.hide();
            } else {
                actionBar.show();
            }
        }
    }
}

package hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events;

import hu.bme.aut.adapted.commonlib.util.Event;

public class SendMeditationEvent extends Event {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public SendMeditationEvent() {
    }

    public SendMeditationEvent(int value) {
        this.value = value;
    }
}

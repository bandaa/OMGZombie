package hu.bme.aut.adapted.gamelib.ipc;

import android.os.Handler;
import android.os.Parcel;
import android.os.RemoteException;

import hu.bme.aut.adapted.commonlib.ipc.IGame;

import static hu.bme.aut.adapted.commonlib.util.RemoteHelper.localExecute;

public abstract class Game extends IGame.Stub {
    private final Handler handler;

    public Game(Handler handler) {
        this.handler = handler;
    }

    @Override
    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        if (code == INTERFACE_TRANSACTION) {
            return super.onTransact(code, data, reply, flags);
        }

        return localExecute(() -> super.onTransact(code, data, reply, flags), handler);
    }
}

package hu.bme.aut.adapted.gamelib.ui.dashboard;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class DashboardLayout extends LinearLayout {
	
	public DashboardLayout(Context context) {
		super(context);
	}
	
	public DashboardLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public DashboardLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	/**
	 * Initialize default properties, such as layout orientation.
	 */
	{
		setOrientation(VERTICAL);		
	}	
	
}

package hu.bme.aut.adapted.gamelib.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static hu.bme.aut.adapted.gamelib.annotation.HasDashboard.Visibility.VISIBLE;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface HasDashboard {

    enum Visibility {
        VISIBLE, HIDDEN
    }

    Visibility value() default VISIBLE;
}

package hu.bme.aut.adapted.gamelib.ui.common;

import android.os.Handler;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.ui.Presenter;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.GameMessengerInteractor;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendAttentionEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendHeartRateEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SendMeditationEvent;
import hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events.SensorStatusChangedEvent;
import hu.bme.aut.adapted.gamelib.util.SensorListener;

import static org.greenrobot.eventbus.ThreadMode.MAIN;

public abstract class CommonPresenter<S> extends Presenter<S> {
    @Inject
    protected GameMessengerInteractor gameMessengerInteractor;

    @Inject
    @Ipc
    protected Handler backgroundHandler;

    @Inject
    protected EventBus bus;

    private final List<SensorListener> sensorListeners = new ArrayList<>();

    @Override
    public void attachScreen(S screen) {
        super.attachScreen(screen);
        inject();
        bus.register(this);
    }

    @Override
    public void detachScreen() {
        bus.unregister(this);
        super.detachScreen();
    }

    protected abstract void inject();

    public void addSensorListener(SensorListener listener) {
        sensorListeners.add(listener);
    }

    public void removeSensorListener(SensorListener listener) {
        sensorListeners.remove(listener);
    }

    public void connectNeurosky() {
        backgroundHandler.post(() -> gameMessengerInteractor.connectNeurosky());
    }

    public void disconnectNeurosky() {
        backgroundHandler.post(() -> gameMessengerInteractor.disconnectNeurosky());
    }

    public void connectZephyr() {
        backgroundHandler.post(() -> gameMessengerInteractor.connectZephyr());
    }

    public void disconnectZephyr() {
        backgroundHandler.post(() -> gameMessengerInteractor.disconnectZephyr());
    }

    public void connectEmotiv() {
        backgroundHandler.post(() -> gameMessengerInteractor.connectEmotiv());
    }

    public void disconnectEmotiv() {
        backgroundHandler.post(() -> gameMessengerInteractor.disconnectEmotiv());
    }

    public void updateSensorStatus() {
        backgroundHandler.post(() -> gameMessengerInteractor.updateSensorStatus());
    }

    @Subscribe(threadMode = MAIN)
    public void onSensorStatusChangedEvent(SensorStatusChangedEvent event) {
        for (SensorListener listener : sensorListeners) {
            listener.sensorStatusChanged(event.getName(), event.getStatus());
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onSendAttentionEvent(SendAttentionEvent event) {
        for (SensorListener listener : sensorListeners) {
            listener.attentionChanged(event.getValue());
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onSendMeditationEvent(SendMeditationEvent event) {
        for (SensorListener listener : sensorListeners) {
            listener.meditationChanged(event.getValue());
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onSendHeartRateEvent(SendHeartRateEvent event) {
        for (SensorListener listener : sensorListeners) {
            listener.heartRateChanged(event.getValue());
        }
    }
}

package hu.bme.aut.adapted.gamelib.interactor.gamemessenger.events;

import hu.bme.aut.adapted.commonlib.util.Event;

public class StopGameplayEvent extends Event {
    private int gameplayId;

    public int getGameplayId() {
        return gameplayId;
    }

    public void setGameplayId(int gameplayId) {
        this.gameplayId = gameplayId;
    }
}

package com.emotiv.sdk;

public class IEE_PerformanceMetricAlgo_t {
    public static final int PM_EXCITEMENT = 0x0001;
    public static final int PM_RELAXATION = 0x0002;
    public static final int PM_STRESS = 0x0004;
    public static final int PM_ENGAGEMENT = 0x0008;
    public static final int PM_INTEREST = 0x0010;
    public static final int PM_FOCUS = 0x0020;
}

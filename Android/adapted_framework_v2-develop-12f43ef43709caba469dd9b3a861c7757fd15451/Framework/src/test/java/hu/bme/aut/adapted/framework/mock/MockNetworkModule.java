package hu.bme.aut.adapted.framework.mock;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.network.NetworkModule;
import hu.bme.aut.adapted.framework.network.auth.AuthApi;
import hu.bme.aut.adapted.framework.network.classification.ClassificationApi;
import hu.bme.aut.adapted.framework.network.game.DataApi;
import hu.bme.aut.adapted.framework.network.gameplay.GamePlayApi;
import hu.bme.aut.adapted.framework.settings.Settings;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

@Module
public class MockNetworkModule {

    private NetworkModule networkModule = new NetworkModule();

    @Provides
    @Singleton
    public OkHttpClient.Builder provideOkHttpClientBuilder(final Settings settings) {
        return networkModule.provideOkHttpClientBuilder(settings);
    }


    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(OkHttpClient.Builder builder) {

        builder.interceptors().add(1, new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                return MockHttpServer.call(request);
            }
        });

        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient client) {
        return networkModule.provideRetrofit(client);
    }

    @Provides
    @Singleton
    public AuthApi provideAuthApi(Retrofit retrofit) {
        return networkModule.provideAuthApi(retrofit);
    }

    @Provides
    @Singleton
    public DataApi provideDataApi(Retrofit retrofit) {
        return networkModule.provideDataApi(retrofit);
    }

    @Provides
    @Singleton
    public GamePlayApi provideGamePlayApi(Retrofit retrofit){
        return networkModule.provideGamePlayApi(retrofit);
    }

    @Provides
    @Singleton
    public ClassificationApi provideClassificationApi(Retrofit retrofit) {
        return networkModule.provideClassificationApi(retrofit);
    }


    @Provides
    @Singleton
    @Network
    public Executor provideNetworkExecutor() {
        return networkModule.provideNetworkExecutor();
    }
}

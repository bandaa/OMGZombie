package hu.bme.aut.adapted.framework;


import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.commonlib.util.di.Ipc;
import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.util.UiExecutor;

@Module
public class TestModule {

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    @Ipc
    public Looper provideIpcLooper() {
        return Looper.getMainLooper();
    }

    @Provides
    @Ipc
    public Handler provideIpcHandler(@Ipc Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Singleton
    @Network
    public Executor provideNetworkExecutor() {
        return new UiExecutor();
    }

    @Provides
    @Singleton
    public Context getContext() {
        return null;
    }
}

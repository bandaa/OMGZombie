package hu.bme.aut.adapted.framework.mock;


import android.net.Uri;

import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.model.auth.LoginResponse;
import hu.bme.aut.adapted.framework.model.auth.LoginUserRegistrationRequest;
import hu.bme.aut.adapted.framework.model.auth.UserRegistrationResponse;
import hu.bme.aut.adapted.framework.network.NetworkConfig;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;

import static hu.bme.aut.adapted.commonlib.util.GsonHelper.gson;

public class AuthMock {

    public static Response process(Request request) {
        Uri uri = Uri.parse(request.url().toString());

        String responseString = "";
        int responseCode = 0;
        Headers headers = request.headers();

        if (uri.getPath().equals(NetworkConfig.ENDPOINT_PREFIX + "UserLogin")) {
            LoginRequest loginRequest = gson.fromJson(MockHelper.bodyToString(request), LoginRequest.class);

            if (loginRequest.getUserName().equals(MockConstants.REGISTERED_USERNAME) && loginRequest.getPassword().equals((MockConstants.REGISTERED_PASSWORD))) {
                responseString = gson.toJson(new LoginResponse(true,"Success",1,MockConstants.REGISTERED_SESSIONKEY,true));
                responseCode = 200;
            } else {
                responseString = "Bad username or password!";
                responseCode = 401;
            }
        }

        if (uri.getPath().equals(NetworkConfig.ENDPOINT_PREFIX + "UserRegistration")) {
            LoginUserRegistrationRequest loginUserRegistrationRequest = gson.fromJson(MockHelper.bodyToString(request), LoginUserRegistrationRequest.class);

            if (loginUserRegistrationRequest.getUserName().equals(MockConstants.UNREGISTERED_USERNAME)){
                responseString = gson.toJson(new UserRegistrationResponse(true,"Success",1));
                responseCode = 200;
            } else if (loginUserRegistrationRequest.getUserName().equals(MockConstants.REGISTERED_USERNAME)){
                responseString = "Username already exists!";
                responseCode = 401;
            }
            else {
                responseString = "Server side error!";
                responseCode = 401;
            }
        }

        return MockHelper.makeResponse(request, headers, responseCode, responseString);
    }
}

package hu.bme.aut.adapted.framework;


import javax.inject.Singleton;

import dagger.Component;
import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.framework.interactor.InteractorModule;
import hu.bme.aut.adapted.framework.mock.MockNetworkModule;
import hu.bme.aut.adapted.framework.repository.RepositoryModule;
import hu.bme.aut.adapted.framework.sensors.SensorModule;

@Singleton
@Component(modules = {MockNetworkModule.class, SensorModule.class, InteractorModule.class, RepositoryModule.class, CommonModule.class})
public interface TestComponent extends FrameworkAppComponent {
}

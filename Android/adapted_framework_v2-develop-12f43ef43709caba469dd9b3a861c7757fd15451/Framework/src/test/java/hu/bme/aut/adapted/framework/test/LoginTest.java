package hu.bme.aut.adapted.framework.test;

import android.content.Context;
import android.os.RemoteException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;
import hu.bme.aut.adapted.framework.BuildConfig;
import hu.bme.aut.adapted.framework.interactor.auth.AuthInteractor;
import hu.bme.aut.adapted.framework.mock.MockConstants;
import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.settings.Settings;
import hu.bme.aut.adapted.framework.util.RobolectricDaggerTestRunner;
import hu.bme.aut.adapted.framework.util.https.Store;

import static hu.bme.aut.adapted.framework.TestHelper.setTestInjector;
import static junit.framework.Assert.assertEquals;

@RunWith(RobolectricDaggerTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 24)
public class LoginTest {

    private AuthInteractor authInteractor;
    private Settings settings;
    private IAuthCallback authCallback;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup(){
        Context context = RuntimeEnvironment.application.getApplicationContext();
        Store.init(context);
        setTestInjector(context);
        settings = new Settings();
        settings.setSessionKey(null);
        authInteractor = new AuthInteractor();
        authCallback = new IAuthCallback.Stub(){

            @Override
            public void onLogin(String sessionKey) throws RemoteException {

            }

            @Override
            public void onRegister(boolean success, String username, String message) throws RemoteException {

            }
        };
    }

    @Test
    public void testLoginSuccess(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUserName(MockConstants.REGISTERED_USERNAME);
        loginRequest.setPassword(MockConstants.REGISTERED_PASSWORD);
        authInteractor.login(authCallback,loginRequest);
        assertEquals(settings.getSessionKey(),MockConstants.REGISTERED_SESSIONKEY);
    }

    @Test
    public void testLoginFailure(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUserName(MockConstants.UNREGISTERED_USERNAME);
        loginRequest.setPassword(MockConstants.UNREGISTERED_PASSWORD);



        authInteractor.login(authCallback,loginRequest);

        //thrown.expect(UnsuccessfulRequestException.class);
    }

    @After
    public void tearDown(){

    }
}

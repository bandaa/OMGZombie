package hu.bme.aut.adapted.framework;


import android.content.Context;

import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowLog;

import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.framework.util.https.Store;

public class TestHelper {
    public static void setTestInjector(Context context) {
        ShadowLog.stream = System.out;
        //FrameworkApplication application = (FrameworkApplication) RuntimeEnvironment.application;
        FrameworkAppComponent injector = DaggerTestComponent.builder().commonModule(new CommonModule(context))
                .build();
        FrameworkApplication.setInjector(injector);
        //Stetho.initializeWithDefaults(application);
    }
}

package hu.bme.aut.adapted.framework.interactor.calculation;

import static java.lang.StrictMath.pow;
import static java.lang.StrictMath.sqrt;

/**
 * Created by Laura on 2015.11.23..
 */
public class ComplexNumber {
    public double real=0.0;
    public double imaginary=0.0;

    public ComplexNumber(){
        this.real=0.0;
        this.imaginary=0.0;
    }
    public ComplexNumber(double re, double im){
        this.real=re;
        this.imaginary=im;
    }

    public void setReal(double re){
        real=re;
    }
    public void setImaginary(double im){
        imaginary=im;
    }

    public double getReal(){
        return real;
    }

    public double getImaginary(){
        return imaginary;
    }

    public ComplexNumber multiply(ComplexNumber a){
        ComplexNumber result=new ComplexNumber();
        double resultReal= a.getReal()*this.getReal()-a.getImaginary()*this.getImaginary();
        double resultImaginary = a.getReal()*this.getImaginary()+a.getImaginary()*this.getReal();
        //System.out.println(resultReal);
        //System.out.println(resultImaginary);
        result.setReal(resultReal);
        result.setImaginary(resultImaginary);
        return result;
    }

    public double multiplyByConj() {
        double result = this.getImaginary()*this.getImaginary()+this.getReal()*this.getReal();
        return result;
    }

    public ComplexNumber add(ComplexNumber a) {
        ComplexNumber result = new ComplexNumber();
        result.setReal(a.getReal() + this.getReal());
        result.setImaginary(a.getImaginary() + this.getImaginary());
        return result;
    }

    public ComplexNumber conj (){
        imaginary=-1*imaginary;
        return this;
    }

    public ComplexNumber abs () {
        double localReal=this.getReal();
        double localImaginary=this.getImaginary();
        localReal=pow(localReal,2);
        localImaginary=pow(localImaginary,2);
        this.setReal(sqrt(localReal+localImaginary));
        this.setImaginary(0.0);
        return this;
    }
}
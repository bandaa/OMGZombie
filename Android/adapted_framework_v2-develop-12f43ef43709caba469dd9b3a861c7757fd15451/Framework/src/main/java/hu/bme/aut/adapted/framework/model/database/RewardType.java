package hu.bme.aut.adapted.framework.model.database;

import java.util.ArrayList;
import java.util.List;

import io.requery.Entity;
import io.requery.OneToMany;

@Entity
public abstract class RewardType extends TypeDescriptor
        implements ITypeDescriptor<RewardType, RewardParamType> {
    @OneToMany
    List<RewardParamType> paramTypes;

    @Override
    public void setGame(Game game) {
        super.setGame(game);

        if (!game.getRewardTypes().contains(this)) {
            game.getRewardTypes().add(this);
        }
    }

    @Override
    public List<RewardParamType> getParamTypes() {
        if (paramTypes == null) {
            paramTypes = new ArrayList<>();
        }

        return paramTypes;
    }
}

package hu.bme.aut.adapted.framework.repository;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.framework.model.database.Event;
import hu.bme.aut.adapted.framework.model.database.EventParamType;
import hu.bme.aut.adapted.framework.model.database.EventType;
import hu.bme.aut.adapted.framework.model.database.IParamType;
import hu.bme.aut.adapted.framework.model.database.ITypeDescriptor;
import hu.bme.aut.adapted.framework.model.database.RewardParamType;
import hu.bme.aut.adapted.framework.model.database.RewardType;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class EntityConverter {
    private static final String DYNAMIC_EVENT_TYPE_NAME = "GameDynamicEvent";

    @Inject
    Repository repository;

    protected EntityConverter() {
        injector.inject(this);
    }

    private <T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> TypeDescriptorResult<T, P>
    convertTypeDescriptor(
            hu.bme.aut.adapted.commonlib.ipc.payload.TypeDescriptor typeDescriptor, int gameId,
            TypeDescriptorGetter<T> getter, Creator<T> creator, ParamTypeListGetter<T, P> ptlGetter,
            Creator<P> ptCreator) {
        T oldEntity = getter.get(typeDescriptor.getName(), gameId);

        TypeDescriptorResult<T, P> result =
                new TypeDescriptorResult<>(oldEntity != null ? oldEntity : creator.create(),
                        oldEntity != null);

        if (oldEntity != null) {
            result.ptDelete.addAll(ptlGetter.get(oldEntity));
        }

        result.td.setName(typeDescriptor.getName());
        result.td.setGame(repository.getGame(gameId));
        result.td.setUploaded(false);

        for (hu.bme.aut.adapted.commonlib.ipc.payload.ParamType paramType :
                typeDescriptor.getParamTypes()) {
            convertParamType(paramType, ptCreator, result);
        }

        return result;
    }

    private <T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> void convertParamType(
            hu.bme.aut.adapted.commonlib.ipc.payload.ParamType paramType, Creator<P> creator,
            TypeDescriptorResult<T, P> result) {
        P oldEntity = findParamType(paramType.getName(), result.ptDelete);
        P entity = oldEntity != null ? oldEntity : creator.create();

        entity.setName(paramType.getName());
        entity.setType(paramType.getType());
        entity.setMinValue(paramType.getMinValue());
        entity.setMaxValue(paramType.getMaxValue());
        entity.setTypeDescriptor(result.td);

        if (oldEntity != null) {
            result.ptDelete.remove(entity);
            result.ptUpdate.add(entity);
        } else {
            result.ptInsert.add(entity);
        }
    }

    public TypeDescriptorResult<EventType, EventParamType> convertEventType(
            hu.bme.aut.adapted.commonlib.ipc.payload.EventType eventType, int gameId) {
        TypeDescriptorResult<EventType, EventParamType> result =
                convertTypeDescriptor(eventType, gameId, repository::getEventType,
                        repository::createEventType, repository::getEventParamTypes,
                        repository::createEventParamType);

        Integer rewardTypeId = eventType.getRewardTypeId();
        RewardType rewardType = rewardTypeId != null ?
                repository.getRewardType(rewardTypeId) : null;
        result.td.setRewardType(rewardType);

        return result;
    }

    public TypeDescriptorResult<RewardType, RewardParamType> convertRewardType(
            hu.bme.aut.adapted.commonlib.ipc.payload.RewardType rewardType, int gameId) {
        return convertTypeDescriptor(rewardType, gameId, repository::getRewardType,
                repository::createRewardType, repository::getRewardParamTypes,
                repository::createRewardParamType);
    }

    public Event convertDynamicEvent(GameEvent event, int gameplayId) {
        Event entity = repository.createEvent();

        entity.setEventTypeName(DYNAMIC_EVENT_TYPE_NAME);
        entity.setEventType(repository.getEventType(event.getEventTypeId()));
        entity.setGameplay(repository.getGameplay(gameplayId));
        entity.setTimeStamp(event.getTimeStamp());

        JsonObject parameters = event.getParameters();
        addMember(parameters, "goodnessValue", new JsonPrimitive(event.getGoodnessValue()));
        entity.setParameters(parameters);

        return entity;
    }

    private static <P extends IParamType<?, ?>> P findParamType(String name, List<P> paramTypes) {
        for (P paramType : paramTypes) {
            if (paramType.getName().equals(name)) {
                return paramType;
            }
        }

        return null;
    }

    private static void addMember(JsonObject object, String name, JsonElement value) {
        JsonElement oldValue = object.remove(name);

        if (oldValue != null) {
            addMember(object, name + '_', oldValue);
        }

        object.add(name, value);
    }

    private interface Creator<T> {
        T create();
    }

    private interface TypeDescriptorGetter<T extends ITypeDescriptor<T, ?>> {
        T get(String name, int gameId);
    }

    private interface ParamTypeListGetter<T extends ITypeDescriptor<T, P>,
            P extends IParamType<T, P>> {
        List<? extends P> get(T typeDescriptor);
    }

    public final class TypeDescriptorResult<T extends ITypeDescriptor<T, P>,
            P extends IParamType<T, P>> {
        public final T td;
        public final boolean update;
        public final List<P> ptInsert = new ArrayList<>();
        public final List<P> ptUpdate = new ArrayList<>();
        public final List<P> ptDelete = new ArrayList<>();

        public TypeDescriptorResult(T td, boolean update) {
            this.td = td;
            this.update = update;
        }
    }
}

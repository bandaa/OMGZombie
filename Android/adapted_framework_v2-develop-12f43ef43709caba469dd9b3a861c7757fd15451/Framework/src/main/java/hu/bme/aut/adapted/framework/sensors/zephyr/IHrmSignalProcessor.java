package hu.bme.aut.adapted.framework.sensors.zephyr;

interface IHrmSignalProcessor {

	public abstract double[] calcPSD(double[] values);

}
package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class EventDTO {
    @SerializedName("LocalId")
    private Integer localId;

    @SerializedName("ServerId")
    private Integer serverId;

    @SerializedName("TimeStamp")
    private Long timeStamp;

    @SerializedName("EventType")
    private String eventType;

    @SerializedName("TypeDescriptor")
    private String typeDescriptor;

    @SerializedName("EventParams")
    private JsonObject eventParams;

    public EventDTO() {
    }

    public EventDTO(Integer localId, Integer serverId, Long timeStamp, String eventType, String typeDescriptor, JsonObject eventParams) {
        this.localId = localId;
        this.serverId = serverId;
        this.timeStamp = timeStamp;
        this.eventType = eventType;
        this.typeDescriptor = typeDescriptor;
        this.eventParams = eventParams;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getTypeDescriptor() {
        return typeDescriptor;
    }

    public void setTypeDescriptor(String typeDescriptor) {
        this.typeDescriptor = typeDescriptor;
    }

    public JsonObject getEventParams() {
        return eventParams;
    }

    public void setEventParams(JsonObject eventParams) {
        this.eventParams = eventParams;
    }
}

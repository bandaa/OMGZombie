package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;


public class Group {

    @SerializedName("Id")
    private String id;

    @SerializedName("Name")
    private String name;

    public Group(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Group() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

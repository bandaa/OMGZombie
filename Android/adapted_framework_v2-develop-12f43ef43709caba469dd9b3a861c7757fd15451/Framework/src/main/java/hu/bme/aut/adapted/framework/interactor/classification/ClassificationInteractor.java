package hu.bme.aut.adapted.framework.interactor.classification;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.exception.UnexpectedErrorException;
import hu.bme.aut.adapted.framework.exception.UnsuccessfulRequestException;
import hu.bme.aut.adapted.framework.interactor.classification.event.SvmArrivedEvent;
import hu.bme.aut.adapted.framework.interactor.classification.event.SvmSentEvent;
import hu.bme.aut.adapted.framework.model.classification.SVMGetResponse;
import hu.bme.aut.adapted.framework.model.classification.SVMRequest;
import hu.bme.aut.adapted.framework.network.classification.ClassificationApi;
import retrofit2.Call;
import retrofit2.Response;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

/**
 * Created by dorka on 2017.03.13..
 */
public class ClassificationInteractor {
    @Inject
    ClassificationApi classificationApi;
    @Inject
    EventBus bus;

    public ClassificationInteractor() {
        injector.inject(this);
    }

    public void getSVMModel(String id) {
        Call<SVMGetResponse> call = classificationApi.getSVMModelForUser(id);
        Response<SVMGetResponse> response;
        SvmArrivedEvent event = new SvmArrivedEvent();
        try {
            response = call.execute();
            checkSuccess(response);
            event.setCode(response.code());
            event.setSvmModel(response.body().getSvmModel());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    public void sendSVMModel(SVMRequest request) {
        Call<hu.bme.aut.adapted.framework.model.Response> call = classificationApi.sendSVMModel(request);
        Response<hu.bme.aut.adapted.framework.model.Response> response;
        SvmSentEvent event = new SvmSentEvent();
        try {
            response = call.execute();
            checkSuccess(response);
            event.setCode(response.code());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    private <T extends hu.bme.aut.adapted.framework.model.Response> void checkSuccess(Response<T> response) throws UnexpectedErrorException, UnsuccessfulRequestException {
        if (response.code() != 200) throw new UnexpectedErrorException();
        if (!response.body().isSucceeded())
            throw new UnsuccessfulRequestException(response.body().getMessage());
    }

}

package hu.bme.aut.adapted.framework.model.auth;

import com.google.gson.annotations.SerializedName;

public class UserRegistrationRequest {
    @SerializedName("UserName")
    private String userName;
    @SerializedName("Sex")
    private int sex;
    @SerializedName("BirthYear")
    private int birthYear;
    @SerializedName("BirthMonth")
    private int birthMonth;
    @SerializedName("BirthDay")
    private int birthDay;
    @SerializedName("ZipCode")
    private int zipCode;
    @SerializedName("Disabilities")
    private String disabilities;

    public UserRegistrationRequest() {
    }

    public UserRegistrationRequest(String userName, int sex, int birthYear, int birthMonth, int birthDay, int zipCode, String disabilities) {
        this.userName = userName;
        this.sex = sex;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDay = birthDay;
        this.zipCode = zipCode;
        this.disabilities = disabilities;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getDisabilities() {
        return disabilities;
    }

    public void setDisabilities(String disabilities) {
        this.disabilities = disabilities;
    }
}

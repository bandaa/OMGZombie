package hu.bme.aut.adapted.framework.model.auth;


import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @SerializedName("UserName")
    private String userName;

    @SerializedName("Password")
    private String password;

    public LoginRequest(){}

    public LoginRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

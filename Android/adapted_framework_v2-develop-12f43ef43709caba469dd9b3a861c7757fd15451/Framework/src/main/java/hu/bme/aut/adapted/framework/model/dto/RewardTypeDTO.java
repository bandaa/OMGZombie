package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RewardTypeDTO {
    @SerializedName("Name")
    private String name;

    @SerializedName("ParamTypes")
    private List<ParamTypeDTO> paramTypes;

    public RewardTypeDTO() {
    }

    public RewardTypeDTO(String name, List<ParamTypeDTO> paramTypes) {
        this.name = name;
        this.paramTypes = paramTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ParamTypeDTO> getParamTypes() {
        return paramTypes;
    }

    public void setParamTypes(List<ParamTypeDTO> paramTypes) {
        this.paramTypes = paramTypes;
    }
}

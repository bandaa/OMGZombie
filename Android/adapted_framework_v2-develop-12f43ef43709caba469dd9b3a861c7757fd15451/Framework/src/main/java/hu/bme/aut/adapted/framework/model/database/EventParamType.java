package hu.bme.aut.adapted.framework.model.database;

import io.requery.Column;
import io.requery.Entity;
import io.requery.ManyToOne;

@Entity
public abstract class EventParamType extends ParamType
        implements IParamType<EventType, EventParamType> {
    @ManyToOne
    @Column(nullable = false)
    EventType typeDescriptor;

    @Override
    public EventType getTypeDescriptor() {
        return typeDescriptor;
    }

    @Override
    public void setTypeDescriptor(EventType typeDescriptor) {
        this.typeDescriptor = typeDescriptor;

        if (!typeDescriptor.getParamTypes().contains(this)) {
            typeDescriptor.getParamTypes().add(this);
        }
    }
}

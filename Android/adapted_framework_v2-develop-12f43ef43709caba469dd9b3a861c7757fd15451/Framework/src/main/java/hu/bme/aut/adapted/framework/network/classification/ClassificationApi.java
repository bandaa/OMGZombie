package hu.bme.aut.adapted.framework.network.classification;

import hu.bme.aut.adapted.framework.model.Response;
import hu.bme.aut.adapted.framework.model.classification.SVMGetResponse;
import hu.bme.aut.adapted.framework.model.classification.SVMRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by dorka on 2017.03.13..
 */

public interface ClassificationApi {
    @GET("SvmModel")
    Call<SVMGetResponse> getSVMModelForUser(@Path("id") String id);

    @POST("SvmModel")
    Call<Response> sendSVMModel(@Body SVMRequest request);
}

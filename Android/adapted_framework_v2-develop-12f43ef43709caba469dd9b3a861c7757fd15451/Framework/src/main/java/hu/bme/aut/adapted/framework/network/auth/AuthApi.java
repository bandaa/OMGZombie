package hu.bme.aut.adapted.framework.network.auth;

import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.model.auth.LoginResponse;
import hu.bme.aut.adapted.framework.model.auth.LoginUserRegistrationRequest;
import hu.bme.aut.adapted.framework.model.auth.UserRegistrationResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface AuthApi {
    @POST("UserLogin")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("UserRegistration")
    Call<UserRegistrationResponse> register(@Body LoginUserRegistrationRequest request);
}

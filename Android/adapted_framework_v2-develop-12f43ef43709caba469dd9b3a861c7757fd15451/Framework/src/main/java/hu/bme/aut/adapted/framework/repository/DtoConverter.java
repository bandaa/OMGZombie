package hu.bme.aut.adapted.framework.repository;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.model.database.Event;
import hu.bme.aut.adapted.framework.model.database.EventType;
import hu.bme.aut.adapted.framework.model.database.Game;
import hu.bme.aut.adapted.framework.model.database.Gameplay;
import hu.bme.aut.adapted.framework.model.database.ParamType;
import hu.bme.aut.adapted.framework.model.database.RewardType;
import hu.bme.aut.adapted.framework.model.dto.EventDTO;
import hu.bme.aut.adapted.framework.model.dto.EventTypeDTO;
import hu.bme.aut.adapted.framework.model.dto.GameDTO;
import hu.bme.aut.adapted.framework.model.dto.GamePlayDTO;
import hu.bme.aut.adapted.framework.model.dto.ParamTypeDTO;
import hu.bme.aut.adapted.framework.model.dto.RewardTypeDTO;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class DtoConverter {
    @Inject
    Repository repository;

    public DtoConverter() {
        injector.inject(this);
    }

    public List<GameDTO> convertGames(List <? extends Game> games) {
        List<GameDTO> list = new ArrayList<>();

        for (Game game : games) {
            List<EventTypeDTO> eventTypes = convertEventTypes(repository.getNotUploadedEventTypes(game));
            List<RewardTypeDTO> rewardTypes = convertRewardTypes(repository.getNotUploadedRewardTypes(game));
            List<GamePlayDTO> gameplays = convertGameplays(repository.getGameplays(game));

            if (!eventTypes.isEmpty() || !rewardTypes.isEmpty() || !gameplays.isEmpty()) {
                list.add(new GameDTO(game.getId(), eventTypes, rewardTypes, gameplays));
            }
        }

        return list;
    }

    private List<EventTypeDTO> convertEventTypes(List<? extends EventType> eventTypes) {
        List<EventTypeDTO> list = new ArrayList<>();

        for (EventType eventType : eventTypes) {
            RewardType rewardType = eventType.getRewardType();

            list.add(new EventTypeDTO(eventType.getName(),
                    rewardType != null ? rewardType.getName() : null,
                    convertParamTypes(repository.getEventParamTypes(eventType))));
        }

        return list;
    }

    private List<RewardTypeDTO> convertRewardTypes(List<? extends RewardType> rewardTypes) {
        List<RewardTypeDTO> list = new ArrayList<>();

        for (RewardType rewardType : rewardTypes) {
            list.add(new RewardTypeDTO(rewardType.getName(),
                    convertParamTypes(repository.getRewardParamTypes(rewardType))));
        }

        return list;
    }

    private List<ParamTypeDTO> convertParamTypes(List<? extends ParamType> paramTypes) {
        List<ParamTypeDTO> list = new ArrayList<>();

        for (ParamType paramType : paramTypes) {
            list.add(new ParamTypeDTO(paramType.getName(), paramType.getType(),
                    paramType.getMinValue(), paramType.getMaxValue()));
        }

        return list;
    }

    private List<GamePlayDTO> convertGameplays(List<? extends Gameplay> gameplays) {
        List<GamePlayDTO> list = new ArrayList<>();

        for (Gameplay gameplay : gameplays) {
            List<EventDTO> events = convertEvents(repository.getEvents(gameplay));

            if (!events.isEmpty()) {
                list.add(new GamePlayDTO(gameplay.getId(), gameplay.getServerId(),
                        gameplay.getSessionKey(), events));
            }
        }

        return list;
    }

    private List<EventDTO> convertEvents(List<? extends Event> events) {
        List<EventDTO> list = new ArrayList<>();

        for (Event event : events) {
            Log.d("FrameworkService", "converting event to DTO with id: " + event.getId());

            EventType eventType = event.getEventType();

            list.add(new EventDTO(event.getId(), null, event.getTimeStamp(),
                    event.getEventTypeName(), eventType != null ? eventType.getName() : null,
                    event.getParameters()));
        }

        return list;
    }
}

package hu.bme.aut.adapted.framework.interactor.upload.event;

import java.util.ArrayList;
import java.util.List;

import hu.bme.aut.adapted.framework.util.NetworkEvent;
import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadResponse;

/**
 * Created by dorka on 2017.03.31..
 */
public class GamePlayUploadEvent extends NetworkEvent {
    private GamePlayUploadResponse response;
    private final List<String> eventTypeNames = new ArrayList<>();
    private final List<String> rewardTypeNames = new ArrayList<>();

    public void setResponse(GamePlayUploadResponse response) {
        this.response = response;
    }

    public GamePlayUploadResponse getResponse() {
        return response;
    }

    public List<String> getEventTypeNames() {
        return eventTypeNames;
    }

    public List<String> getRewardTypeNames() {
        return rewardTypeNames;
    }
}

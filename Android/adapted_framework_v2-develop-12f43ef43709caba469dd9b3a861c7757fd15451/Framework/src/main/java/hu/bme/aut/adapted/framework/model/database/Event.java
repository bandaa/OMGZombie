package hu.bme.aut.adapted.framework.model.database;

import com.google.gson.JsonObject;

import hu.bme.aut.adapted.framework.util.JsonObjectConverter;
import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;

@Entity
public abstract class Event implements Persistable {
    @Key
    @Generated
    int id;

    @Column(nullable = false)
    String eventTypeName;

    @ManyToOne
    EventType eventType;

    @ManyToOne
    @Column(nullable = false)
    Gameplay gameplay;

    @Column(name = "timeStamp_", nullable = false)
    long timeStamp;

    @Convert(JsonObjectConverter.class)
    JsonObject parameters;

    public int getId() {
        return id;
    }

    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Gameplay getGameplay() {
        return gameplay;
    }

    public void setGameplay(Gameplay gameplay) {
        this.gameplay = gameplay;

        if (!gameplay.getEvents().contains(this)) {
            gameplay.getEvents().add(this);
        }
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JsonObject getParameters() {
        return parameters;
    }

    public void setParameters(JsonObject parameters) {
        this.parameters = parameters;
    }
}

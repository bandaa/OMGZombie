package hu.bme.aut.adapted.framework.interactor.upload;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.interactor.upload.event.GamePlayUploadEvent;
import hu.bme.aut.adapted.framework.model.dto.EventTypeDTO;
import hu.bme.aut.adapted.framework.model.dto.GameDTO;
import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadRequest;
import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadResponse;
import hu.bme.aut.adapted.framework.model.dto.RewardTypeDTO;
import hu.bme.aut.adapted.framework.network.gameplay.GamePlayApi;
import retrofit2.Call;
import retrofit2.Response;

import static hu.bme.aut.adapted.framework.util.ResponseChecker.checkSuccess;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class UploadInteractor {
    @Inject
    GamePlayApi gamePlayApi;

    @Inject
    EventBus bus;

    public UploadInteractor() {
        injector.inject(this);
    }

    public void uploadData(GamePlayUploadRequest request) {
        GamePlayUploadEvent event = new GamePlayUploadEvent();

        for (GameDTO game : request.getGames()) {
            for (EventTypeDTO eventType : game.getEventTypes()) {
                event.getEventTypeNames().add(eventType.getName());
            }

            for (RewardTypeDTO rewardType : game.getRewardTypes()) {
                event.getRewardTypeNames().add(rewardType.getName());
            }
        }

        Call<GamePlayUploadResponse> call = gamePlayApi.uploadGamePlay(request);
        Response<GamePlayUploadResponse> response;

        try {
            response = call.execute();
            checkSuccess(response);
            event.setCode(response.code());
            event.setResponse(response.body());
        } catch (Exception e) {
            event.setThrowable(e);
        }

        bus.post(event);
    }
}

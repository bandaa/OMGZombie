package hu.bme.aut.adapted.framework.model.auth;

import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

public class UserRegistrationResponse extends Response {
    @SerializedName("UserId")
    private int userId;

    public UserRegistrationResponse() {
        super();
    }

    public UserRegistrationResponse(boolean succeeded, String message, int userId) {
        super(succeeded, message);
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

package hu.bme.aut.adapted.framework.model.event.sensors.zephyr;

public class HeartRRIntervalGameEvent {
    private int rrInterval;

    public HeartRRIntervalGameEvent(int rrInterval) {
        this.rrInterval = rrInterval;
    }

    public int getRrInterval() {
        return rrInterval;
    }

}

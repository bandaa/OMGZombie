package hu.bme.aut.adapted.framework.interactor.storage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.framework.model.database.Event;
import hu.bme.aut.adapted.framework.model.database.Game;
import hu.bme.aut.adapted.framework.model.database.Gameplay;
import hu.bme.aut.adapted.framework.model.database.IParamType;
import hu.bme.aut.adapted.framework.model.database.ITypeDescriptor;
import hu.bme.aut.adapted.framework.model.dto.GameDTO;
import hu.bme.aut.adapted.framework.repository.DtoConverter;
import hu.bme.aut.adapted.framework.repository.EntityConverter;
import hu.bme.aut.adapted.framework.repository.Repository;

import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.getName;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.getParameters;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class StorageInteractor {
    @Inject
    EntityConverter entityConverter;

    @Inject
    DtoConverter dtoConverter;

    @Inject
    Repository repository;

    public StorageInteractor() {
        injector.inject(this);
    }

    private Game addGame(int gameId) {
        Game game = repository.createGame();
        game.setId(gameId);
        // TODO
        game.setName(Integer.toString(gameId));
        repository.saveGame(game);
        return game;
    }

    public Gameplay getActiveGameplay(int gameId) {
        return repository.getActiveGameplay(gameId);
    }

    public boolean isGameplayActive(int gameId) {
        return getActiveGameplay(gameId) != null;
    }

    public int startNewGameplay(int gameId, String sessionKey) {
        return repository.runInTransaction(() -> {
            Game game = repository.getGame(gameId);

            if (game == null) {
                game = addGame(gameId);
            }

            repository.setAllGameplaysInactive(gameId);

            Gameplay gameplay = repository.createGameplay();
            gameplay.setGame(game);
            gameplay.setSessionKey(sessionKey);
            gameplay.setActive(true);
            repository.saveGameplay(gameplay, false);

            return gameplay.getId();
        });
    }

    public void stopGameplay(int gameId) {
        repository.setAllGameplaysInactive(gameId);
    }

    private <E extends hu.bme.aut.adapted.commonlib.ipc.payload.TypeDescriptor,
            T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> int addTypeDescriptor(
            E typeDescriptor, int gameId, TypeDescriptorConverter<E, T, P> converter,
            Saver<T> saver, Saver<List<P>> ptlSaver, Remover<List<P>> ptlRemover) {
        return repository.runInTransaction(() -> {
            EntityConverter.TypeDescriptorResult<T, P> result =
                    converter.convert(typeDescriptor, gameId);

            saver.save(result.td, result.update);

            if (!result.ptInsert.isEmpty()) {
                ptlSaver.save(result.ptInsert, false);
            }

            if (!result.ptUpdate.isEmpty()) {
                ptlSaver.save(result.ptUpdate, true);
            }

            if (!result.ptDelete.isEmpty()) {
                ptlRemover.remove(result.ptDelete);
            }

            return result.td.getId();
        });
    }

    public int addEventType(hu.bme.aut.adapted.commonlib.ipc.payload.EventType eventType,
                            int gameId) {
        return addTypeDescriptor(eventType, gameId, entityConverter::convertEventType,
                repository::saveEventType, repository::saveEventParamTypes,
                repository::removeEventParamTypes);
    }

    public int addRewardType(hu.bme.aut.adapted.commonlib.ipc.payload.RewardType rewardType,
                             int gameId) {
        return addTypeDescriptor(rewardType, gameId, entityConverter::convertRewardType,
                repository::saveRewardType, repository::saveRewardParamTypes,
                repository::removeRewardParamTypes);
    }

    public void addDynamicEvent(GameEvent event, int gameplayId) {
        repository.runInTransaction(() -> {
            Event entity = entityConverter.convertDynamicEvent(event, gameplayId);
            repository.saveEvent(entity);
        });
    }

    public void addStaticEvent(Object object, long timeStamp, int gameplayId) {
        repository.runInTransaction(() -> {
            Event event = repository.createEvent();
            event.setEventTypeName(getName(object.getClass()));
            event.setGameplay(repository.getGameplay(gameplayId));
            event.setTimeStamp(timeStamp);
            event.setParameters(getParameters(object));
            repository.saveEvent(event);
        });
    }

    public List<GameDTO> getAllGames() {
        return repository.runInTransaction(() -> dtoConverter.convertGames(repository.getAllGames()));
    }

    public void cleanupAfterUpload(List<String> eventTypeNames, List<String> rewardTypeNames,
                                   Map<Integer, Integer> gameplayIds, List<Integer> eventIds) {
        repository.runInTransaction(() -> {
            repository.setEventTypesUploaded(eventTypeNames);
            repository.setRewardTypesUploaded(rewardTypeNames);

            for (Map.Entry<Integer, Integer> entry : gameplayIds.entrySet()) {
                Integer serverId = entry.getValue();

                if (serverId != null && serverId > 0) {
                    Gameplay gameplay = repository.getGameplay(entry.getKey());
                    gameplay.setServerId(entry.getValue());
                    repository.saveGameplay(gameplay, true);
                }
            }

            repository.removeInactiveGameplays(new ArrayList<>(gameplayIds.keySet()));
            repository.removeEvents(eventIds);
        });
    }

    private interface TypeDescriptorConverter
            <E extends hu.bme.aut.adapted.commonlib.ipc.payload.TypeDescriptor,
                    T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> {
        EntityConverter.TypeDescriptorResult<T, P> convert(E typeDescriptor, int gameId);
    }

    private interface Saver<T> {
        void save(T object, boolean update);
    }

    private interface Remover<T> {
        void remove(T object);
    }
}

package hu.bme.aut.adapted.framework.model.database;

import java.util.ArrayList;
import java.util.List;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public abstract class Gameplay implements Persistable {
    @Key
    @Generated
    int id;

    @Column(unique = true)
    Integer serverId;

    @ManyToOne
    @Column(nullable = false)
    Game game;

    String sessionKey;

    @Column(nullable = false)
    boolean active;

    @OneToMany
    List<Event> events;

    public int getId() {
        return id;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;

        if (!game.getGameplays().contains(this)) {
            game.getGameplays().add(this);
        }
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<>();
        }

        return events;
    }
}

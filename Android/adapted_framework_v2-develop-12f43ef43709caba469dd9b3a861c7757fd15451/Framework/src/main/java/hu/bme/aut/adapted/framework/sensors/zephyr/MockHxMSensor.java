package hu.bme.aut.adapted.framework.sensors.zephyr;

import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.security.SecureRandom;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.HXM;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.commonlib.util.di.Hpv;
import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.interactor.calculation.CalculationInteractor;
import hu.bme.aut.adapted.framework.model.event.sensors.SensorEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.zephyr.HeartRateChangedGameEvent;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;
import hu.bme.aut.adapted.framework.service.event.MessageEvent;

import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.OFFLINE;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.ONLINE;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class MockHxMSensor implements IHxMSensor {
    private static final String LOG_TAG = "MockHxMSensor";
    private static final long MOCK_PERIOD = 1000L;
    private static final int DEFAULT_SAMPLES_PER_SEC = 50;
    public static final int DEFAULT_WINDOW_SIZE = 38;


    private IRRIntervalCalculator irrIntervalCalculator;

    @Inject
    EventBus bus;

    @Inject
    @Network
    Handler backgroundHandler;

    @Inject
    @Hpv
    Handler hpvHandler;

    @Inject
    CalculationInteractor calculationInteractor;

    private IHrmSignalProcessor signalProcessor;

    private Status status = OFFLINE;

    private Handler mockEventsHandler;

    private SecureRandom random;

    private long counter;
    private SlidingDoubleBuffer sampledValues;
    private final int samplesPerSec = DEFAULT_SAMPLES_PER_SEC;


    public MockHxMSensor() {
        this.irrIntervalCalculator = new RRIntervalCalculator();
        this.signalProcessor = new HRMSignalProcessor();
        this.sampledValues = new SlidingDoubleBuffer(samplesPerSec * DEFAULT_WINDOW_SIZE);
        injector.inject(this);
    }

    private final Runnable sendMockEventsTask = () -> {
        sendMockEvents();
        mockEventsHandler.postDelayed(this.sendMockEventsTask, MOCK_PERIOD);
    };

    @Override
    public void connect() {
        if (mockEventsHandler == null) {
            mockEventsHandler = new Handler();
        } else {
            mockEventsHandler.removeCallbacksAndMessages(null);
        }
        random = new SecureRandom();
        mockEventsHandler.post(sendMockEventsTask);

        status = ONLINE;
        sendStatus();

        startHpvCalculationPeriodical();
    }

    private void startHpvCalculationPeriodical() {
        startHpvCalculation();
        hpvHandler.postDelayed(this::startHpvCalculationPeriodical, 1000 / DEFAULT_SAMPLES_PER_SEC);
    }

    private void startHpvCalculation() {
        counter++;
        sampledValues.put(irrIntervalCalculator.getCurrentRRInterval());
        if (counter >= sampledValues.getCapacity() && counter % samplesPerSec == 0) {
            Log.d(LOG_TAG, "Calculating PSD");
            double[] psd = signalProcessor.calcPSD(sampledValues.array());
            onPSDCalculated(psd);
        }
    }

    public void onPSDCalculated(double[] psd) {
        FrequencyMagnitude[] psdArray = new FrequencyMagnitude[psd.length];
        double mfPower = 0.0;
        for (int k = 0; k < psd.length; k++) {
            psdArray[k] = new FrequencyMagnitude((double) (k * samplesPerSec) / (double) psd.length, psd[k]);
            String logText = null;
            if (0.07 <= psdArray[k].frequency && psdArray[k].frequency <= 0.15) {
                mfPower += psdArray[k].magnitude;
                logText = "" + psdArray[k].frequency + " ok";
            } else {
                logText = "" + psdArray[k].frequency + " not ok";
            }
            if (k < 10) {
                Log.d("frequency", logText);
            }
        }

        hpvHandler.post(() -> calculationInteractor.calculate(irrIntervalCalculator.getCurrentRRInterval()));
    }


    private void sendMockEvents() {
        long timestamp = System.currentTimeMillis();

        Log.d(this.getClass().getCanonicalName(), "Logged mock hxm values.");

        ///TODO create realistic data
        int heartRate = random.nextInt(60)+60;
        double instantSpeed = 0.0;
        byte heartBeatNumber = (byte) (random.nextInt(20)-50);
        int temp = random.nextInt(42000);
        int[] heartBeatTsArray = {random.nextInt(30000)+temp,temp};
        int battery = random.nextInt(100);
        Log.d(LOG_TAG, "Mocked Heart Rate is " + heartRate);
        Log.d(LOG_TAG, "Mocked Instant Speed is " + instantSpeed);
        Log.d(LOG_TAG, "Mocked HeartBeat number is " + heartBeatNumber);
        Log.d(LOG_TAG, String.format("Mocked HeartBeatTs first: %d, last: %d", heartBeatTsArray[0], heartBeatTsArray[heartBeatTsArray.length - 1]));
        Log.d(LOG_TAG, "Mocked Battery level is " + battery);

        irrIntervalCalculator.onNewPacket(heartRate,heartBeatNumber,heartBeatTsArray, timestamp);

        HeartRateChangedGameEvent hrcge = new HeartRateChangedGameEvent(heartRate, random.nextInt(1000));
        bus.post(new FrameworkEvent(hrcge, timestamp, false));
        bus.post(new MessageEvent(g -> g.sendHeartRate(heartRate)));
    }

    @Override
    public void disconnect() {
        mockEventsHandler.removeCallbacksAndMessages(null);

        status = OFFLINE;
        sendStatus();
    }

    @Override
    public void sendStatus() {
        bus.post(new SensorEvent(HXM.ACTION_RESPONSE_STATUS, status));
    }
}

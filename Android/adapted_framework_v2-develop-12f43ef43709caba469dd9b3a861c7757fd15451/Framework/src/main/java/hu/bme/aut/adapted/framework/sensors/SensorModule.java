package hu.bme.aut.adapted.framework.sensors;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.commonlib.util.di.Mock;
import hu.bme.aut.adapted.framework.sensors.emotiv.EmotivEEG;
import hu.bme.aut.adapted.framework.sensors.emotiv.IEmotivEEG;
import hu.bme.aut.adapted.framework.sensors.emotiv.MockEmotivEEG;
import hu.bme.aut.adapted.framework.sensors.neurosky.INeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.neurosky.MockNeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.neurosky.NeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.HxMSensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.IHxMSensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.MockHxMSensor;
import hu.bme.aut.adapted.framework.util.di.Emotiv;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

@Module
public class SensorModule {
    private static final String EMOTIV_THREAD_NAME = "emotivThread";

    private Looper newLooper(String threadName) {
        HandlerThread thread = new HandlerThread(threadName, THREAD_PRIORITY_BACKGROUND);
        thread.start();
        return thread.getLooper();
    }

    @Provides
    @Singleton
    @Emotiv
    public Looper provideEmotivLooper() {
        return newLooper(EMOTIV_THREAD_NAME);
    }

    @Provides
    @Emotiv
    public Handler provideEmotivHandler(@Emotiv Looper looper) {
        return new Handler(looper);
    }

    @Provides
    @Singleton
    public INeuroskySensor provideNeuroskySensor() {
        return new NeuroskySensor();
    }

    @Provides
    @Singleton
    @Mock
    public INeuroskySensor provideMockNeuroskySensor() {
        return new MockNeuroskySensor();
    }

    @Provides
    @Singleton
    public IHxMSensor provideHxMSensor() {
        return new HxMSensor();
    }

    @Provides
    @Singleton
    @Mock
    public IHxMSensor provideMockHxMSensor() {
        return new MockHxMSensor();
    }

    @Provides
    @Singleton
    public IEmotivEEG provideEmotivEEG() {
        return new EmotivEEG();
    }

    @Provides
    @Singleton
    @Mock
    public IEmotivEEG provideMockEmotivEEG() {
        return new MockEmotivEEG();
    }
}

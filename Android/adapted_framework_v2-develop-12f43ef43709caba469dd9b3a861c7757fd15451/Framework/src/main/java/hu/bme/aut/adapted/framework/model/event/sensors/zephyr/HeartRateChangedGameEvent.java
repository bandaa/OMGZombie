package hu.bme.aut.adapted.framework.model.event.sensors.zephyr;

public class HeartRateChangedGameEvent {
    private int heartrate;
    private double hpvMfPower;

    public HeartRateChangedGameEvent(int heartrate, double hpvMfPower) {
        this.heartrate = heartrate;
        this.hpvMfPower = hpvMfPower;
    }

    public int getHeartrate() {
        return heartrate;
    }

    public double getHpvMfPower() {
        return hpvMfPower;
    }
}

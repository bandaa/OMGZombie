package hu.bme.aut.adapted.framework.interactor.data.events;

import java.util.List;

import hu.bme.aut.adapted.framework.model.data.Group;

/**
 * Created by dorka on 2017.03.11..
 */
public class UserGroupsEvent extends UserDataEvent{
    private List<Group> groups;

    public UserGroupsEvent(String id, List<Group> groups) {
        super(id);
        this.groups = groups;
    }

    public UserGroupsEvent() {
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}

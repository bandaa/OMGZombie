package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

public class ParamTypeDTO {
    @SerializedName("Name")
    private String name;

    @SerializedName("Type")
    private String type;

    @SerializedName("MinValue")
    private String minValue;

    @SerializedName("MaxValue")
    private String maxValue;

    public ParamTypeDTO() {
    }

    public ParamTypeDTO(String name, String type, String minValue, String maxValue) {
        this.name = name;
        this.type = type;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }
}

package hu.bme.aut.adapted.framework.settings;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import hu.bme.aut.adapted.framework.R;
import hu.bme.aut.adapted.framework.util.https.Store;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

@Singleton
public class Settings {
    private static final String TAG = "Settings";

    // TODO 10 * 60 * 1000 !!!
    private static final long DEFAULT_UPLOAD_PERIOD = 10 * 1000;
    private static final long DEFAULT_SCREENSHOT_PERIOD = 10 * 1000;

    private static final String KEY_SESSION_KEY = "SESSION_KEY";

    private boolean enableAdmin;
    private boolean automaticLock;
    private boolean lockTaskModeEnabled;
    private boolean blockHomeButton;
    private boolean blockBackButton;
    private boolean wakelock;
    private boolean hideNavigationBar;
    private boolean fullBrightness;
    private boolean hideActionBar;
    private boolean keepInFront;
    private boolean mockEeg;
    private boolean soundEnabled;
    private long uploadPeriod;
    private long screenshotPeriod;

    @Inject
    Context context;

    @Inject
    public Settings() {
        injector.inject(this);

        enableAdmin = Store.get(context.getString(R.string.admin_enable_admin), context.getResources().getBoolean(R.bool.pref_default_admin_enable_admin));
        automaticLock = Store.get(context.getString(R.string.admin_automatic_lock), context.getResources().getBoolean(R.bool.pref_default_admin_automatic_lock));
        lockTaskModeEnabled = Store.get(context.getString(R.string.buttons_lock_task_mode_enabled), context.getResources().getBoolean(R.bool.pref_default_buttons_lock_task_mode_enabled));
        blockHomeButton = Store.get(context.getString(R.string.buttons_block_home_button), context.getResources().getBoolean(R.bool.pref_default_buttons_block_home_button));
        blockBackButton = Store.get(context.getString(R.string.buttons_block_back_button), context.getResources().getBoolean(R.bool.pref_default_buttons_block_back_button));
        wakelock = Store.get(context.getString(R.string.screen_wakelock), context.getResources().getBoolean(R.bool.pref_default_screen_wakelock));
        hideNavigationBar = Store.get(context.getString(R.string.screen_hide_navigation_bar), context.getResources().getBoolean(R.bool.pref_default_screen_hide_navigation_bar));
        fullBrightness = Store.get(context.getString(R.string.screen_full_brightness), context.getResources().getBoolean(R.bool.pref_default_screen_full_brightness));
        hideActionBar = Store.get(context.getString(R.string.screen_hide_action_bar), context.getResources().getBoolean(R.bool.pref_default_screen_hide_action_bar));
        keepInFront = Store.get(context.getString(R.string.application_keep_in_front), context.getResources().getBoolean(R.bool.pref_default_application_keep_in_front));
        mockEeg = Store.get(context.getString(R.string.application_mock_eeg), context.getResources().getBoolean(R.bool.pref_default_application_mock_eeg));
        soundEnabled = Store.get(context.getString(R.string.application_sound_enabled), context.getResources().getBoolean(R.bool.pref_default_application_sound_enabled));
        uploadPeriod = Store.get(context.getString(R.string.framework_upload_period), DEFAULT_UPLOAD_PERIOD);
        screenshotPeriod = Store.get(context.getString(R.string.framework_screenshot_period), DEFAULT_SCREENSHOT_PERIOD);

        Store.setListener(context.getString(R.string.admin_enable_admin), (Boolean b) -> enableAdmin = b);
        Store.setListener(context.getString(R.string.admin_automatic_lock), (Boolean b) -> automaticLock = b);
        Store.setListener(context.getString(R.string.buttons_lock_task_mode_enabled), (Boolean b) -> lockTaskModeEnabled = b);
        Store.setListener(context.getString(R.string.buttons_block_home_button), (Boolean b) -> blockHomeButton = b);
        Store.setListener(context.getString(R.string.buttons_block_back_button), (Boolean b) -> blockBackButton = b);
        Store.setListener(context.getString(R.string.screen_wakelock), (Boolean b) -> wakelock = b);
        Store.setListener(context.getString(R.string.screen_hide_navigation_bar), (Boolean b) -> hideNavigationBar = b);
        Store.setListener(context.getString(R.string.screen_full_brightness), (Boolean b) -> fullBrightness = b);
        Store.setListener(context.getString(R.string.screen_hide_action_bar), (Boolean b) -> hideActionBar = b);
        Store.setListener(context.getString(R.string.application_keep_in_front), (Boolean b) -> keepInFront = b);
        Store.setListener(context.getString(R.string.application_mock_eeg), (Boolean b) -> mockEeg = b);
        Store.setListener(context.getString(R.string.application_sound_enabled), (Boolean b) -> soundEnabled = b);
        Store.setListener(context.getString(R.string.framework_upload_period), (Long l) -> uploadPeriod = l);
        Store.setListener(context.getString(R.string.framework_screenshot_period), (Long l) -> screenshotPeriod = l);
    }

    public boolean isLoggedIn() {
        return Store.get(KEY_SESSION_KEY, String.class) != null;
    }

    public boolean isEnableAdmin() {
        return enableAdmin;
    }

    public boolean isAutomaticLock() {
        return automaticLock;
    }

    public boolean isLockTaskModeEnabled() {
        return lockTaskModeEnabled;
    }

    public boolean isBlockHomeButton() {
        return blockHomeButton;
    }

    public boolean isBlockBackButton() {
        return blockBackButton;
    }

    public boolean isWakelock() {
        return wakelock;
    }

    public boolean isHideNavigationBar() {
        return hideNavigationBar;
    }

    public boolean isFullBrightness() {
        return fullBrightness;
    }

    public boolean isHideActionBar() {
        return hideActionBar;
    }

    public boolean isKeepInFront() {
        return keepInFront;
    }

    public boolean isMockEeg() {
        return mockEeg;
    }

    public boolean isSoundEnabled() {
        return soundEnabled;
    }

    public long getUploadPeriod() {
        return uploadPeriod;
    }

    public long getScreenshotPeriod() {
        return screenshotPeriod;
    }

    public String getSessionKey() {
        return Store.get(KEY_SESSION_KEY);
    }

    public void setSessionKey(String sessionKey) {
        if (sessionKey != null) {
            Store.putString(KEY_SESSION_KEY, sessionKey);
        } else {
            Store.remove(KEY_SESSION_KEY);
        }
    }
}

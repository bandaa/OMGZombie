package hu.bme.aut.adapted.framework.ipc;

import android.os.Handler;
import android.os.Parcel;
import android.os.RemoteException;

import hu.bme.aut.adapted.commonlib.ipc.IFramework;

import static android.os.IBinder.FLAG_ONEWAY;
import static android.os.IBinder.INTERFACE_TRANSACTION;
import static hu.bme.aut.adapted.commonlib.util.RemoteHelper.localExecute;

public abstract class Framework extends IFramework.Stub {
    private final Handler handler;

    public Framework(Handler handler) {
        this.handler = handler;
    }

    @Override
    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        if (code == INTERFACE_TRANSACTION) {
            return super.onTransact(code, data, reply, flags);
        }

        if ((flags & FLAG_ONEWAY) != 0) {
            return localExecute(() -> super.onTransact(code, data, reply, flags), handler);
        }

        try {
            return localExecute(() -> super.onTransact(code, data, reply, flags), handler);
        } catch (RemoteException | RuntimeException e) {
            reply.setDataPosition(0);

            try {
                reply.writeException(e);
            } catch (RuntimeException ee) {
                reply.setDataPosition(0);
                reply.writeException(new IllegalStateException());
            }

            return true;
        }
    }
}

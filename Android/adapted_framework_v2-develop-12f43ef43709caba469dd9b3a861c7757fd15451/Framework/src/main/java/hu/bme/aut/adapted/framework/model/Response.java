package hu.bme.aut.adapted.framework.model;


import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("Succeeded")
    private boolean succeeded;

    @SerializedName("Message")
    private String message;

    public Response() {
    }

    public Response(boolean succeeded, String message) {
        this.succeeded = succeeded;
        this.message = message;
    }

    public boolean isSucceeded() {
        return succeeded;
    }

    public void setSucceeded(boolean succeeded) {
        this.succeeded = succeeded;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package hu.bme.aut.adapted.framework.model.event.sensors.neurosky;

public class MeditationChangedGameEvent {
    private int value;

    public MeditationChangedGameEvent(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

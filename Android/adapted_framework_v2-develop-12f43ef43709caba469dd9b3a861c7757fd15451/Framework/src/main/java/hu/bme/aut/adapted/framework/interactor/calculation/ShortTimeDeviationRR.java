package hu.bme.aut.adapted.framework.interactor.calculation;

/**
 * Created by Laura on 2016.04.27..
 */
public class ShortTimeDeviationRR {

    int Window;

    ShortTimeDeviationRR(int window) {
        this.Window = window;
    }

    double calculateSDNN(double[] rrDataInput) {
        int WindowVariable;
        double Average = 0.0;
        double SDNNSum = 0.0;
        for (WindowVariable = 0; WindowVariable < Window; WindowVariable++) {
            Average = Average + rrDataInput[WindowVariable];
        }
        Average=Average/Window;
        for (WindowVariable = 0; WindowVariable < Window; WindowVariable++) {
            SDNNSum = SDNNSum + ((rrDataInput[WindowVariable] - Average) * (rrDataInput[WindowVariable] - Average));
        }
        SDNNSum = SDNNSum / (rrDataInput.length - 1);
        SDNNSum = Math.sqrt(SDNNSum);
        return SDNNSum;
    }

}
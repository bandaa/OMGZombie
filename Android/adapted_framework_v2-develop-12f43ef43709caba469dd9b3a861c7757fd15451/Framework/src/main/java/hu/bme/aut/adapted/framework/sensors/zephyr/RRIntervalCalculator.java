package hu.bme.aut.adapted.framework.sensors.zephyr;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.model.event.sensors.zephyr.HeartRRIntervalGameEvent;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class RRIntervalCalculator implements IRRIntervalCalculator {

    @Inject
    EventBus bus;

    private static final String LOG_TAG = "RRIntervalCalculator";

    private int currentHeartRate;
    private int lastRRInterval;

    public RRIntervalCalculator() {
        currentHeartRate = 0;
        lastRRInterval = 0;
        injector.inject(this);
    }

    @Override
    public void onNewPacket(int heartRate, byte heartBeatNumber, int[] heartBeatTsArray, long timestamp) {
        currentHeartRate = heartRate;
        lastRRInterval = heartBeatTsArray[0] - heartBeatTsArray[1];
        if (lastRRInterval < 0) {
            lastRRInterval += 65535;
        }
        Log.d(LOG_TAG, String.format("Last RR interval: %d", lastRRInterval));

        HeartRRIntervalGameEvent hrrige = new HeartRRIntervalGameEvent(lastRRInterval);
        bus.post(new FrameworkEvent(hrrige, timestamp, false));
    }

    @Override
    public int getCurrentRRInterval() {
        return lastRRInterval;
    }

    @Override
    public int getCurrentHeartRate() {
        return currentHeartRate;
    }
}

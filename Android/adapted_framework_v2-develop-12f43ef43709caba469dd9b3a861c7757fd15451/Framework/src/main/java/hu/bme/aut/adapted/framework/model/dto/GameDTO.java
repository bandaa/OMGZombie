package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GameDTO {
    @SerializedName("Id")
    private Integer id;

    @SerializedName("EventTypes")
    private List<EventTypeDTO> eventTypes;

    @SerializedName("RewardTypes")
    private List<RewardTypeDTO> rewardTypes;

    @SerializedName("GamePlays")
    private List<GamePlayDTO> gamePlays;

    public GameDTO() {
    }

    public GameDTO(Integer id, List<EventTypeDTO> eventTypes, List<RewardTypeDTO> rewardTypes, List<GamePlayDTO> gamePlays) {
        this.id = id;
        this.eventTypes = eventTypes;
        this.rewardTypes = rewardTypes;
        this.gamePlays = gamePlays;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<EventTypeDTO> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventTypeDTO> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public List<RewardTypeDTO> getRewardTypes() {
        return rewardTypes;
    }

    public void setRewardTypes(List<RewardTypeDTO> rewardTypes) {
        this.rewardTypes = rewardTypes;
    }

    public List<GamePlayDTO> getGamePlays() {
        return gamePlays;
    }

    public void setGamePlays(List<GamePlayDTO> gamePlays) {
        this.gamePlays = gamePlays;
    }
}

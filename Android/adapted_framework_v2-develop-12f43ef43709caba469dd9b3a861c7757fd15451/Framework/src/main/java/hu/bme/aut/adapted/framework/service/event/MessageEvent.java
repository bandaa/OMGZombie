package hu.bme.aut.adapted.framework.service.event;

import hu.bme.aut.adapted.framework.util.RemoteGames.Message;

public class MessageEvent {
    private final Message message;

    public MessageEvent(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }
}

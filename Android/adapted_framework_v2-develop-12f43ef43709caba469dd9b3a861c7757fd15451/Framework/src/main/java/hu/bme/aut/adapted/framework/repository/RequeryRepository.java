package hu.bme.aut.adapted.framework.repository;

import android.util.Log;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.model.database.Event;
import hu.bme.aut.adapted.framework.model.database.EventEntity;
import hu.bme.aut.adapted.framework.model.database.EventParamType;
import hu.bme.aut.adapted.framework.model.database.EventParamTypeEntity;
import hu.bme.aut.adapted.framework.model.database.EventType;
import hu.bme.aut.adapted.framework.model.database.EventTypeEntity;
import hu.bme.aut.adapted.framework.model.database.Game;
import hu.bme.aut.adapted.framework.model.database.GameEntity;
import hu.bme.aut.adapted.framework.model.database.Gameplay;
import hu.bme.aut.adapted.framework.model.database.GameplayEntity;
import hu.bme.aut.adapted.framework.model.database.RewardParamType;
import hu.bme.aut.adapted.framework.model.database.RewardParamTypeEntity;
import hu.bme.aut.adapted.framework.model.database.RewardType;
import hu.bme.aut.adapted.framework.model.database.RewardTypeEntity;
import io.requery.Persistable;
import io.requery.sql.EntityDataStore;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class RequeryRepository implements Repository {
    private static final String TAG = "Repository";

    @Inject
    EntityDataStore<Persistable> data;

    public RequeryRepository() {
        injector.inject(this);
    }

    @Override
    public Game createGame() {
        return new GameEntity();
    }

    @Override
    public Gameplay createGameplay() {
        return new GameplayEntity();
    }

    @Override
    public EventType createEventType() {
        return new EventTypeEntity();
    }

    @Override
    public RewardType createRewardType() {
        return new RewardTypeEntity();
    }

    @Override
    public EventParamType createEventParamType() {
        return new EventParamTypeEntity();
    }

    @Override
    public RewardParamType createRewardParamType() {
        return new RewardParamTypeEntity();
    }

    @Override
    public Event createEvent() {
        return new EventEntity();
    }

    @Override
    public void saveGame(Game game) {
        int id = save(game, false).getId();
        Log.d(TAG, "inserted game with id: " + id);
    }

    @Override
    public void saveGameplay(Gameplay gameplay, boolean update) {
        if (update) {
            Log.d(TAG, "trying to update gameplay with id: " + gameplay.getId() + ", server id: " + gameplay.getServerId());
        }
        Gameplay gp = save(gameplay, update);
        Log.d(TAG, (update ? "updated" : "inserted") + " gameplay with id: " + gp.getId() + ", server id: " + gp.getServerId());
    }

    @Override
    public void saveEventType(EventType eventType, boolean update) {
        int id = save(eventType, update).getId();
        Log.d(TAG, (update ? "updated" : "inserted") + " event type with id: " + id);
    }

    @Override
    public void saveRewardType(RewardType rewardType, boolean update) {
        int id = save(rewardType, update).getId();
        Log.d(TAG, (update ? "updated" : "inserted") + " reward type with id: " + id);
    }

    @Override
    public void saveEventParamTypes(List<? extends EventParamType> eventParamTypes, boolean update) {
        save(eventParamTypes, update);
    }

    @Override
    public void saveRewardParamTypes(List<? extends RewardParamType> rewardParamTypes, boolean update) {
        save(rewardParamTypes, update);
    }

    @Override
    public void saveEvent(Event event) {
        save(event, false);
        Log.d(TAG, "inserted event with id " + event.getId() + ", gameplayId: " + event.getGameplay().getId());
    }

    @Override
    public void removeEventParamTypes(List<? extends EventParamType> eventParamTypes) {
        data.delete(eventParamTypes);
    }

    @Override
    public void removeRewardParamTypes(List<? extends RewardParamType> rewardParamTypes) {
        data.delete(rewardParamTypes);
    }

    @Override
    public void removeInactiveGameplays(List<Integer> gameplayIds) {
        int count = data.delete(GameplayEntity.class)
                .where(GameplayEntity.ACTIVE.eq(false)
                        .and(GameplayEntity.ID.in(gameplayIds)))
                .get().value();

        Log.d(TAG, "removed " + count + " inactive gameplays");
    }

    @Override
    public void removeEvents(List<Integer> eventIds) {
        int count = data.delete(EventEntity.class)
                .where(EventEntity.ID.in(eventIds))
                .get().value();

        Log.d(TAG, "removed " + count + " events");
    }

    @Override
    public List<? extends Game> getAllGames() {
        return data.select(GameEntity.class).get().toList();
    }

    @Override
    public Game getGame(int gameId) {
        return data.findByKey(GameEntity.class, gameId);
    }

    @Override
    public Gameplay getGameplay(int id) {
        return data.findByKey(GameplayEntity.class, id);
    }

    @Override
    public Gameplay getActiveGameplay(int gameId) {
        return data.select(GameplayEntity.class)
                .where(GameplayEntity.GAME_ID.eq(gameId)
                        .and(GameplayEntity.ACTIVE.eq(true)))
                .get().firstOrNull();
    }

    @Override
    public EventType getEventType(int id) {
        return data.findByKey(EventTypeEntity.class, id);
    }

    @Override
    public EventType getEventType(String name, int gameId) {
        return data.select(EventTypeEntity.class)
                .where(EventTypeEntity.NAME.eq(name)
                        .and(EventTypeEntity.GAME_ID.eq(gameId)))
                .get().firstOrNull();
    }

    @Override
    public RewardType getRewardType(int id) {
        return data.findByKey(RewardTypeEntity.class, id);
    }

    @Override
    public RewardType getRewardType(String name, int gameId) {
        return data.select(RewardTypeEntity.class)
                .where(RewardTypeEntity.NAME.eq(name)
                        .and(EventTypeEntity.GAME_ID.eq(gameId)))
                .get().firstOrNull();
    }

    @Override
    public List<? extends EventType> getNotUploadedEventTypes(Game game) {
        return data.select(EventTypeEntity.class)
                .where(EventTypeEntity.GAME.eq(game)
                        .and(EventTypeEntity.UPLOADED.eq(false)))
                .get().toList();
    }

    @Override
    public List<? extends RewardType> getNotUploadedRewardTypes(Game game) {
        return data.select(RewardTypeEntity.class)
                .where(RewardTypeEntity.GAME.eq(game)
                        .and(RewardTypeEntity.UPLOADED.eq(false)))
                .get().toList();
    }

    @Override
    public List<? extends Gameplay> getGameplays(Game game) {
        return data.select(GameplayEntity.class)
                .where(GameplayEntity.GAME.eq(game))
                .get().toList();
    }

    @Override
    public List<? extends EventParamType> getEventParamTypes(EventType eventType) {
        return data.select(EventParamTypeEntity.class)
                .where(EventParamTypeEntity.TYPE_DESCRIPTOR.eq(eventType))
                .get().toList();
    }

    @Override
    public List<? extends RewardParamType> getRewardParamTypes(RewardType rewardType) {
        return data.select(RewardParamTypeEntity.class)
                .where(RewardParamTypeEntity.TYPE_DESCRIPTOR.eq(rewardType))
                .get().toList();
    }

    @Override
    public List<? extends Event> getEvents(Gameplay gameplay) {
        return data.select(EventEntity.class)
                .where(EventEntity.GAMEPLAY.eq(gameplay))
                .get().toList();
    }

    @Override
    public void setEventTypesUploaded(List<String> names) {
        int count = data.update(EventTypeEntity.class)
                .set(EventTypeEntity.UPLOADED, true)
                .where(EventTypeEntity.NAME.in(names))
                .get().value();

        Log.d(TAG, "set " + count + " event types to uploaded");
    }

    @Override
    public void setRewardTypesUploaded(List<String> names) {
        int count = data.update(RewardTypeEntity.class)
                .set(RewardTypeEntity.UPLOADED, true)
                .where(RewardTypeEntity.NAME.in(names))
                .get().value();

        Log.d(TAG, "set " + count + " reward types to uploaded");
    }

    @Override
    public void setAllGameplaysInactive(int gameId) {
        int count = data.update(GameplayEntity.class)
                .set(GameplayEntity.ACTIVE, false)
                .where(GameplayEntity.GAME_ID.eq(gameId))
                .get().value();

        Log.d(TAG, "set " + count + " gameplays to inactive");
    }

    @Override
    public void runInTransaction(Runnable runnable) {
        runInTransaction((Callable<Void>) () -> {
            runnable.run();
            return null;
        });
    }

    @Override
    public <V> V runInTransaction(Callable<V> callable) {
        return data.runInTransaction(callable);
    }

    private <T extends Persistable> T save(T entity, boolean update) {
        if (update) {
            return data.update(entity);
        } else {
            return data.insert(entity);
        }
    }

    private void save(List<? extends Persistable> entities, boolean update) {
        if (update) {
            data.update(entities);
        } else {
            data.insert(entities);
        }
    }
}

package hu.bme.aut.adapted.framework.util;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class RequestPermissionActivity extends Activity {
    public static final String EXTRA_REQUEST_CODE = "requestCode";
    public static final String EXTRA_PERMISSIONS = "permissions";

    @Inject
    EventBus bus;

    private int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injector.inject(this);

        requestCode = getIntent().getIntExtra(EXTRA_REQUEST_CODE, 0);
        String[] permissions = getIntent().getStringArrayExtra(EXTRA_PERMISSIONS);

        if (permissions == null || permissions.length == 0) {
            finish();
            return;
        }

        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == this.requestCode) {
            bus.post(new RequestPermissionEvent(requestCode, permissions, grantResults));
            finish();
        }
    }
}

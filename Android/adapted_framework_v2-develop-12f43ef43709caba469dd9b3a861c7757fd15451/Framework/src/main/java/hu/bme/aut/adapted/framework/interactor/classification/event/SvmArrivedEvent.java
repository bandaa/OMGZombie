package hu.bme.aut.adapted.framework.interactor.classification.event;

import hu.bme.aut.adapted.framework.util.NetworkEvent;

/**
 * Created by dorka on 2017.03.13..
 */

public class SvmArrivedEvent extends NetworkEvent {
    private String svmModel;

    public SvmArrivedEvent() {
    }

    public SvmArrivedEvent(int code, Throwable throwable, String svmModel) {
        super(code, throwable);
        this.svmModel = svmModel;
    }

    public String getSvmModel() {
        return svmModel;
    }

    public void setSvmModel(String svmModel) {
        this.svmModel = svmModel;
    }

}

package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventTypeDTO {
    @SerializedName("Name")
    private String name;

    @SerializedName("RewardTypeName")
    private String rewardTypeName;

    @SerializedName("ParamTypes")
    private List<ParamTypeDTO> paramTypes;

    public EventTypeDTO() {
    }

    public EventTypeDTO(String name, String rewardTypeName, List<ParamTypeDTO> paramTypes) {
        this.name = name;
        this.rewardTypeName = rewardTypeName;
        this.paramTypes = paramTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRewardTypeName() {
        return rewardTypeName;
    }

    public void setRewardTypeName(String rewardTypeName) {
        this.rewardTypeName = rewardTypeName;
    }

    public List<ParamTypeDTO> getParamTypes() {
        return paramTypes;
    }

    public void setParamTypes(List<ParamTypeDTO> paramTypes) {
        this.paramTypes = paramTypes;
    }
}

package hu.bme.aut.adapted.framework.sensors.zephyr;

import hu.bme.aut.adapted.framework.exception.HRMConnectionFailedException;
import hu.bme.aut.adapted.framework.sensors.ISensor;

public interface IHxMSensor extends ISensor {
    public void connect() throws HRMConnectionFailedException;
    public void disconnect();
}

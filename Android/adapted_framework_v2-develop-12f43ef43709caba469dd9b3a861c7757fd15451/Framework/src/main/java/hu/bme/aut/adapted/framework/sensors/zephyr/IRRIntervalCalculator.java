package hu.bme.aut.adapted.framework.sensors.zephyr;

public interface IRRIntervalCalculator {

	public abstract void onNewPacket(int heartRate, byte heartBeatNumber, int[] heartBeatTsArray, long timestamp);

	public abstract int getCurrentRRInterval();

	public abstract int getCurrentHeartRate();

}
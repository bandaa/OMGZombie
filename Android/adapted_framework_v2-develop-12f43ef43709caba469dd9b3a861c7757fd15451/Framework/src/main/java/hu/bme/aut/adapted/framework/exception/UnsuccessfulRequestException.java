package hu.bme.aut.adapted.framework.exception;

/**
 * Created by dorka on 2017.03.12..
 */

public class UnsuccessfulRequestException extends RuntimeException {
    public UnsuccessfulRequestException(String message) {
        super(message);
    }
}

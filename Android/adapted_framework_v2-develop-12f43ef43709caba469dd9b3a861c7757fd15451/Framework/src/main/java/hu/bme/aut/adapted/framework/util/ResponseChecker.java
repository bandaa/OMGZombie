package hu.bme.aut.adapted.framework.util;

import hu.bme.aut.adapted.framework.exception.UnexpectedErrorException;
import hu.bme.aut.adapted.framework.exception.UnsuccessfulRequestException;
import retrofit2.Response;

/**
 * Created by dorka on 2017.03.31..
 */
public class ResponseChecker {
    public static <T extends hu.bme.aut.adapted.framework.model.Response> void checkSuccess(Response<T> response) throws UnexpectedErrorException, UnsuccessfulRequestException {
        if(response.code()!=200) throw new UnexpectedErrorException();
        if(!response.body().isSucceeded()) throw new UnsuccessfulRequestException(response.body().getMessage());
    }
}

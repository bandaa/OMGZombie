package hu.bme.aut.adapted.framework.model.classification;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorka on 2017.03.13..
 */
public class SVMRequest{

    @SerializedName("UserId")
    String id;
    @SerializedName("SvmModel")
    String svmModel;

    public SVMRequest(String id, String svmModel) {
        this.id = id;
        this.svmModel = svmModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

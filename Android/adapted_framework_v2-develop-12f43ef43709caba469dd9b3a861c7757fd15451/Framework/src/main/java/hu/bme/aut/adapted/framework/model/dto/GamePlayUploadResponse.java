package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hu.bme.aut.adapted.framework.model.Response;

public class GamePlayUploadResponse extends Response {
    @SerializedName("GamePlays")
    private List<GamePlayResponse> gamePlays;

    @SerializedName("ExpiredGamePlayIds")
    private List<Integer> expiredGamePlayIds;

    public GamePlayUploadResponse() {
        super();
    }

    public GamePlayUploadResponse(boolean succeeded, String message, List<GamePlayResponse> gamePlays, List<Integer> expiredGamePlayIds) {
        super(succeeded, message);
        this.gamePlays = gamePlays;
        this.expiredGamePlayIds = expiredGamePlayIds;
    }

    public List<GamePlayResponse> getGamePlays() {
        return gamePlays;
    }

    public void setGamePlays(List<GamePlayResponse> gamePlays) {
        this.gamePlays = gamePlays;
    }

    public List<Integer> getExpiredGamePlayIds() {
        return expiredGamePlayIds;
    }

    public void setExpiredGamePlayIds(List<Integer> expiredGamePlayIds) {
        this.expiredGamePlayIds = expiredGamePlayIds;
    }
}

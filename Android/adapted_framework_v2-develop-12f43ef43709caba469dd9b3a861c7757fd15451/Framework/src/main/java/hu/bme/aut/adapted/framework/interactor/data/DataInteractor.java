package hu.bme.aut.adapted.framework.interactor.data;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.exception.UnexpectedErrorException;
import hu.bme.aut.adapted.framework.exception.UnsuccessfulRequestException;
import hu.bme.aut.adapted.framework.model.data.GameNameResponse;
import hu.bme.aut.adapted.framework.model.data.GroupsResponse;
import hu.bme.aut.adapted.framework.model.data.UserClassResponse;
import hu.bme.aut.adapted.framework.model.data.UserFullNameResponse;
import hu.bme.aut.adapted.framework.network.game.DataApi;
import hu.bme.aut.adapted.framework.interactor.data.events.GameNameEvent;
import hu.bme.aut.adapted.framework.interactor.data.events.UserClassEvent;
import hu.bme.aut.adapted.framework.interactor.data.events.UserFullNameEvent;
import hu.bme.aut.adapted.framework.interactor.data.events.UserGroupsEvent;
import retrofit2.Call;
import retrofit2.Response;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

/**
 * Created by dorka on 2017.03.06..
 */

public class DataInteractor {
    @Inject
    DataApi dataApi;
    @Inject
    EventBus bus;

    public DataInteractor() {
        injector.inject(this);
    }

    public void getUserFullName(String id){
        Call<UserFullNameResponse> call=dataApi.getUserFullName(id);
        Response<UserFullNameResponse> response;
        UserFullNameEvent event=new UserFullNameEvent();
        event.setUserId(id);
        try {
            response=call.execute();
            checkSuccess(response);
            event.setFullName(response.body().getName());
            event.setCode(response.code());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    public void getUserGroups(String id){
        Call<GroupsResponse> call=dataApi.getUserGroups(id);
        Response<GroupsResponse> response;
        UserGroupsEvent event=new UserGroupsEvent();
        event.setUserId(id);
        try {
            response=call.execute();
            checkSuccess(response);
            event.setGroups(response.body().getGroups());
            event.setCode(response.code());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    public void getUserClass(String id){
        Call<UserClassResponse> call=dataApi.getUserClass(id);
        Response<UserClassResponse> response;
        UserClassEvent event=new UserClassEvent();
        event.setUserId(id);
        try {
            response=call.execute();
            checkSuccess(response);
            event.setUserClass(response.body().getUserClass());
            event.setCode(response.code());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    public void getGameName(String id){
        Call<GameNameResponse> call=dataApi.getGameName(id);
        Response<GameNameResponse> response;
        GameNameEvent event=new GameNameEvent();
        event.setGameId(id);
        try {
            response=call.execute();
            checkSuccess(response);
            event.setGameName(response.body().getName());
            event.setCode(response.code());
            bus.post(event);
        } catch (Exception e) {
            event.setThrowable(e);
            bus.post(event);
        }
    }

    private <T extends hu.bme.aut.adapted.framework.model.Response> void checkSuccess(Response<T> response) throws UnexpectedErrorException, UnsuccessfulRequestException {
        if(response.code()!=200) throw new UnexpectedErrorException();
        if(!response.body().isSucceeded()) throw new UnsuccessfulRequestException(response.body().getMessage());
    }


}

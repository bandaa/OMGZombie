package hu.bme.aut.adapted.framework.model.database;

public interface IParamType<T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> {
    int getId();

    String getName();

    void setName(String name);

    String getType();

    void setType(String type);

    String getMinValue();

    void setMinValue(String minValue);

    String getMaxValue();

    void setMaxValue(String maxValue);

    T getTypeDescriptor();

    void setTypeDescriptor(T typeDescriptor);
}

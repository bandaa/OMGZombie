package hu.bme.aut.adapted.framework.sensors.neurosky;

import hu.bme.aut.adapted.framework.sensors.ISensor;

public interface INeuroskySensor extends ISensor {
    void connect();
    void disconnect();
}

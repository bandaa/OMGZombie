package hu.bme.aut.adapted.framework.util;

import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.bme.aut.adapted.commonlib.ipc.IGame;

public class RemoteGames {
    private final RemoteCallbackList<IGame> games = new RemoteCallbackList<>();
    private final Map<IGame, Object[]> gameIds = new HashMap<>();

    public void add(IGame game, int gameId) {
        Log.d("GAMES", "registered game callback with id: " + gameId);
        games.register(game);
        IBinder.DeathRecipient dr = () -> gameIds.remove(game);
        gameIds.put(game, new Object[]{gameId, dr});

        try {
            game.asBinder().linkToDeath(dr, 0);
        } catch (RemoteException e) {
            Log.e("GAMES", "remote exception when linking binder to death recipient");
        }
    }

    public void remove(IGame game) {
        Log.d("GAMES", "unregistered game callback");

        Object[] v = gameIds.remove(game);

        if (v != null) {
            IBinder.DeathRecipient dr = (IBinder.DeathRecipient) v[1];
            game.asBinder().unlinkToDeath(dr, 0);
        }

        games.unregister(game);
    }

    public void sendToAll(Message message) {
        int n = games.beginBroadcast();

        Log.d("GAMES", "game callback count: " + games.getRegisteredCallbackCount() + ", sendToAll with " + n + " games");

        for (int i = 0; i < n; i++) {
            try {
                message.send(games.getBroadcastItem(i));
            } catch (RemoteException ignored) {
            }
        }

        games.finishBroadcast();
    }

    @SuppressWarnings("Convert2streamapi")
    public List<Integer> getGameIds() {
        List<Integer> list = new ArrayList<>();

        for (Object[] v : gameIds.values()) {
            list.add((int) v[0]);
        }

        return list;
    }

    public Integer getGameId(IGame game) {
        Object[] v = gameIds.get(game);
        return v != null ? (int) v[0] : null;
    }

    public void kill() {
        Log.d("GAMES", "killing game callback list");
        games.kill();
        gameIds.clear();
    }

    public interface Message {
        void send(IGame game) throws RemoteException;
    }
}

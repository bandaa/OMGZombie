package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GamePlayDTO {
    @SerializedName("LocalId")
    private Integer localId;

    @SerializedName("ServerId")
    private Integer serverId;

    @SerializedName("SessionKey")
    private String sessionKey;

    @SerializedName("Events")
    private List<EventDTO> events;

    public GamePlayDTO() {
    }

    public GamePlayDTO(Integer localId, Integer serverId, String sessionKey, List<EventDTO> events) {
        this.localId = localId;
        this.serverId = serverId;
        this.sessionKey = sessionKey;
        this.events = events;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public List<EventDTO> getEvents() {
        return events;
    }

    public void setEvents(List<EventDTO> events) {
        this.events = events;
    }
}

package hu.bme.aut.adapted.framework.model.event.sensors.neurosky;

public class AttentionChangedGameEvent {
    private int value;

    public AttentionChangedGameEvent(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

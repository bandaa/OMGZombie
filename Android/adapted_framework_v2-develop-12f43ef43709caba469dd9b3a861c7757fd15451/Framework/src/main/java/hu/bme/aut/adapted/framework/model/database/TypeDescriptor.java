package hu.bme.aut.adapted.framework.model.database;

import io.requery.Column;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.Superclass;

@Superclass
public abstract class TypeDescriptor implements Persistable {
    @Key
    @Generated
    int id;

    @Column(nullable = false, unique = true)
    String name;

    @ManyToOne
    @Column(nullable = false)
    Game game;

    @Column(nullable = false)
    boolean uploaded;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }
}

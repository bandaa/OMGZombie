package hu.bme.aut.adapted.framework.sensors;

public interface ISensor {
    void sendStatus();
}

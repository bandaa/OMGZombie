package hu.bme.aut.adapted.framework.interactor.auth.events;


import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;
import hu.bme.aut.adapted.framework.model.Response;
import hu.bme.aut.adapted.framework.util.NetworkEvent;

public class AuthNetworkEvent<E, T extends Response> extends NetworkEvent {
    private E request;
    private T response;
    private IAuthCallback callback;

    public final E getRequest() {
        return request;
    }

    public final void setRequest(E request) {
        this.request = request;
    }

    public final T getResponse() {
        return response;
    }

    public final void setResponse(T response) {
        this.response = response;
    }

    public final IAuthCallback getCallback() {
        return callback;
    }

    public void setCallback(IAuthCallback callback) {
        this.callback = callback;
    }
}

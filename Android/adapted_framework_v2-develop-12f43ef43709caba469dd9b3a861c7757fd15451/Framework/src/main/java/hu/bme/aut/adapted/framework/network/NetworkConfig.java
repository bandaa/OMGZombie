package hu.bme.aut.adapted.framework.network;


public class NetworkConfig {
    public static final String ENDPOINT_ADDRESS = "http://nmobil.aut.bme.hu";
    public static final String ENDPOINT_PREFIX = "/AdaptEd/Service/InnoRest.svc/";
    public static final String SERVICE_ENDPOINT = ENDPOINT_ADDRESS + ENDPOINT_PREFIX;
}

package hu.bme.aut.adapted.framework.model.event.sensors.zephyr;

/**
 * Created by Laura on 2016.04.27..
 */
public class HeartBurgGameEvent {
    double burgResult;

    public HeartBurgGameEvent(double res){
        burgResult =res;
        android.util.Log.d("RR_INTERVAL","Burg event created");
    }

    public double getBurgResult(){
        return burgResult;
    }
}

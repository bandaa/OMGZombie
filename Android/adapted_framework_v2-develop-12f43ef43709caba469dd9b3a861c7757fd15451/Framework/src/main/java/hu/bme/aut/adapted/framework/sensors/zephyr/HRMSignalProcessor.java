package hu.bme.aut.adapted.framework.sensors.zephyr;

import android.util.Log;

class HRMSignalProcessor implements IHrmSignalProcessor {

	static {
//		System.loadLibrary("HRMNative");
        System.loadLibrary("gnustl_shared");
		System.loadLibrary("InnolearnSensorsLib");
		Log.d("HRMSignalProcessor","Native libraries loaded.");
	}
	
	@Override
	public double[] calcPSD(double[] values) {
		return nativeCalcPSD(values);
		//return CalcPSD(values);

	}
	
	private native double[] nativeCalcPSD(double[] values);

	private double[] CalcPSD(double[] values)
	{
		double temp[] = {3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0, 3.0};

		return temp;
	}

}
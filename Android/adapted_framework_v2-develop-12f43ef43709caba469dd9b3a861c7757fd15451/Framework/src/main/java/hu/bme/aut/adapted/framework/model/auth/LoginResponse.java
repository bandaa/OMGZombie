package hu.bme.aut.adapted.framework.model.auth;


import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

public class LoginResponse extends Response {

    @SerializedName("UserId")
    private int userId;

    @SerializedName("SessionKey")
    private String sessionKey;

    @SerializedName("Authenticated")
    private boolean authenticated;

    public LoginResponse() {
        super();
    }

    public LoginResponse(boolean succeeded, String message, int userId, String sessionKey, boolean authenticated) {
        super(succeeded, message);
        this.userId = userId;
        this.sessionKey = sessionKey;
        this.authenticated = authenticated;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}

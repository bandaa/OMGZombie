package hu.bme.aut.adapted.framework;

import android.app.Application;
import android.content.Context;

import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.framework.util.https.Store;

public class FrameworkApplication extends Application {
    public static FrameworkAppComponent injector;

    @Override
    public void onCreate() {
        super.onCreate();
        initInjector(this);
        Store.init(this);
    }

    private static void initInjector(Context context) {
        injector = DaggerFrameworkAppComponent.builder()
                .commonModule(new CommonModule(context))
                .build();
    }

    public static void setInjector(FrameworkAppComponent frameworkAppComponent) {
        injector = frameworkAppComponent;
    }
}

package hu.bme.aut.adapted.framework.interactor.classification.event;

import hu.bme.aut.adapted.framework.util.NetworkEvent;

/**
 * Created by dorka on 2017.03.13..
 */
public class SvmSentEvent extends NetworkEvent {


    public SvmSentEvent() {

    }

    public SvmSentEvent(int code, Throwable throwable) {
        super(code, throwable);
    }
}

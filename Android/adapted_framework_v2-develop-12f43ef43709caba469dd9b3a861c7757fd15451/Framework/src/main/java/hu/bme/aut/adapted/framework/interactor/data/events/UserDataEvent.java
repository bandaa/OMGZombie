package hu.bme.aut.adapted.framework.interactor.data.events;

import hu.bme.aut.adapted.framework.util.NetworkEvent;

/**
 * Created by dorka on 2017.03.06..
 */

public class UserDataEvent extends NetworkEvent {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public UserDataEvent(String userId) {

        this.userId = userId;
    }

    public UserDataEvent() {

    }

    public UserDataEvent(int code, Throwable throwable, String userId) {
        super(code, throwable);
        this.userId = userId;
    }
}

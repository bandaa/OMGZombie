package hu.bme.aut.adapted.framework.interactor.calculation;

import android.os.Handler;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.model.event.sensors.zephyr.HeartBurgGameEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.zephyr.HeartSDNNGameEvent;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class CalculationInteractor {

    private int windowSize;
    private double[] incomingRRIntervals;
    private int dataFilled;
    private float elapsedTime = 0;
    private int lastIntegerMultiply;
    private int currentIntegerMultiply;
    private double lastRR;
    private final int SIZE_OF_SAMPLE = 1000;
    private double currentValue;
    private double sdnnRes = 0.0;
    private double BurgRes = 0.0;

    ShortTimeDeviationRR sdrr;

    TestForBurg t;

    @Inject
    @Network
    Handler backgroundHandler;

    @Inject
    EventBus bus;

    public CalculationInteractor() {
        init(32);

        injector.inject(this);
    }

    public void init(int DesiredWindowSize) {
        android.util.Log.d("RR_INTERVAL", "CalculationInteractor Created");
        windowSize = DesiredWindowSize;
        incomingRRIntervals = new double[windowSize];
        dataFilled = 0;
        lastRR = 0.0;
        currentValue = 0.0;
        lastIntegerMultiply = 0;
        currentIntegerMultiply = 0;

        sdrr = new ShortTimeDeviationRR(windowSize);
        t = new TestForBurg(5, DesiredWindowSize);
    }

    public void calculate(int rrInterval) {

        android.util.Log.d("RR_INTERVAL", "RR interval event received: " + rrInterval);
        android.util.Log.d("RR_INTERVAL", "Burg every ten seconds");


        int Index = 0;


        elapsedTime = elapsedTime + rrInterval;
        android.util.Log.d("RR_INTERVAL", "elapsed time: " + elapsedTime);

        if (dataFilled == 0) {
            lastRR = rrInterval;
            android.util.Log.d("RR_INTERVAL", "DataFilled is 0");
            dataFilled = dataFilled + 1;

        }
        else if (dataFilled < windowSize) {
            android.util.Log.d("RR_INTERVAL", "Entered first if");
            currentValue = interpolate(lastRR, rrInterval, elapsedTime);
            incomingRRIntervals[dataFilled] = currentValue;
            dataFilled = dataFilled + 1;
            lastRR = rrInterval;

            android.util.Log.d("RR_INTERVAL", "sampled RR: " + lastRR);
        } else {
            android.util.Log.d("RR_INTERVAL", "Entered second if");
            for (Index = 0; Index < windowSize - 1; Index++) {
                incomingRRIntervals[Index] = incomingRRIntervals[Index + 1];
            }
            currentValue = interpolate(lastRR, rrInterval, elapsedTime);
            incomingRRIntervals[incomingRRIntervals.length - 1] = currentValue;
            lastRR = rrInterval;
            //launchRRCustomCalculations();
            android.util.Log.d("RR_INTERVAL", "sampled RR: " + lastRR);
            currentIntegerMultiply = (int) elapsedTime / SIZE_OF_SAMPLE;
            android.util.Log.d("RR_INTERVAL", "current Integer Multiply: " + currentIntegerMultiply);
            android.util.Log.d("RR_INTERVAL", "last Integer Multiply out of if: " + lastIntegerMultiply);
            if (currentIntegerMultiply > lastIntegerMultiply + 10) {
                lastIntegerMultiply = currentIntegerMultiply;
                android.util.Log.d("RR_INTERVAL", "last Integer Multiply in if: " + lastIntegerMultiply);

                BurgRes = t.doBurgCalculation(incomingRRIntervals);
                android.util.Log.d("RR_INTERVAL", "Calculated burg value: " + BurgRes);


            }
            sdnnRes = sdrr.calculateSDNN(incomingRRIntervals);

            bus.post(new HeartSDNNGameEvent(sdnnRes));
            bus.post(new HeartBurgGameEvent(BurgRes));

        }
    }

    double interpolate(double previousRR, double currentRR, double elapsedTime) {
        android.util.Log.d("RR_INTERVAL", "Entered function");
        double previousTime = elapsedTime - currentRR;
        int previousMultiply = (int) previousTime / SIZE_OF_SAMPLE;
        double result = 0.0;
        if (previousRR > currentRR) {
            result = (previousRR - currentRR) * (elapsedTime - previousMultiply) / (elapsedTime - previousRR);
            result = result + currentRR;
        } else if (previousRR < currentRR) {
            result = (currentRR - lastRR) * (previousMultiply - (elapsedTime - currentRR)) / (elapsedTime - currentRR);
            result = result + previousRR;

        } else if (previousRR == currentRR) {
            result = currentRR;
        }
        android.util.Log.d("RR_INTERVAL", "Interpolated :" + result);
        return result;
    }
}

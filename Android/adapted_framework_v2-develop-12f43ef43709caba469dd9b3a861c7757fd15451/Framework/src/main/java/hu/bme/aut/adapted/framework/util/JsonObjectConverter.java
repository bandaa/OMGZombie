package hu.bme.aut.adapted.framework.util;

import com.google.gson.JsonObject;

import io.requery.Converter;

import static hu.bme.aut.adapted.commonlib.util.GsonHelper.gson;

public class JsonObjectConverter implements Converter<JsonObject, String> {
    @Override
    public Class<JsonObject> getMappedType() {
        return JsonObject.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(JsonObject value) {
        return value != null ? gson.toJson(value) : null;
    }

    @Override
    public JsonObject convertToMapped(Class<? extends JsonObject> type, String value) {
        return value != null ? gson.fromJson(value, type) : null;
    }
}

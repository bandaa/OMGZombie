package hu.bme.aut.adapted.framework.interactor.data.events;

import hu.bme.aut.adapted.framework.util.NetworkEvent;

/**
 * Created by dorka on 2017.03.11..
 */
public class GameNameEvent extends NetworkEvent {

    private String gameId;
    private String gameName;

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public GameNameEvent() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}

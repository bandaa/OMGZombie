package hu.bme.aut.adapted.framework.sensors.zephyr;

public class SlidingDoubleBuffer {

	private final int capacity;
    private double[] array;
	/** the index where the next value will be insterted */
	private int currentPosition;
	
	public SlidingDoubleBuffer(int capacity) {
		this.capacity = capacity;
		this.array = new double[capacity];
		this.currentPosition = 0;
	}

	public void put(double value) {
		array[currentPosition] = value;
		currentPosition++;
		currentPosition = currentPosition % capacity;
	}
	
	public double[] array() {
		double[] ret = new double[capacity];
		for (int i = currentPosition; i <= array.length - 1; i++) {
			ret[i - currentPosition] = array[i];
		}
		for (int i = 0; i < currentPosition; i++) {
			ret[array.length - currentPosition + i] = array[i];
		}
		return ret;
	}
	
	public int getCapacity() {
		return capacity;
	}
}

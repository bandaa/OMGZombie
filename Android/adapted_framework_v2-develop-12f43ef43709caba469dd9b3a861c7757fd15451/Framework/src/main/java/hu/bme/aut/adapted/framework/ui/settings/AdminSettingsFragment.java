package hu.bme.aut.adapted.framework.ui.settings;

import android.os.Bundle;

import hu.bme.aut.adapted.framework.R;

public class AdminSettingsFragment extends BaseSettingsFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_admin);
    }
}

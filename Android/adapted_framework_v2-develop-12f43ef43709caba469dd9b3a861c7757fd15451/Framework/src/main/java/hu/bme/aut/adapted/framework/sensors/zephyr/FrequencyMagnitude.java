package hu.bme.aut.adapted.framework.sensors.zephyr;

public class FrequencyMagnitude {

	public double frequency;
    public double magnitude;
	
	public FrequencyMagnitude() {
		this(0.0, 0.0);
	}
	
	public FrequencyMagnitude(double frequency, double magnitude) {
		this.frequency = frequency;
		this.magnitude = magnitude;
	}

}

package hu.bme.aut.adapted.framework.model.classification;

import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

/**
 * Created by dorka on 2017.03.13..
 */
public class SVMGetResponse extends Response {
    @SerializedName("SvmModel")
    String svmModel;

    public SVMGetResponse(boolean succeeded, String message, String svmmodel) {
        super(succeeded, message);
        this.svmModel = svmmodel;
    }

    public SVMGetResponse() {
    }

    public String getSvmModel() {
        return svmModel;
    }

    public void setSvmModel(String svmModel) {
        this.svmModel = svmModel;
    }
}

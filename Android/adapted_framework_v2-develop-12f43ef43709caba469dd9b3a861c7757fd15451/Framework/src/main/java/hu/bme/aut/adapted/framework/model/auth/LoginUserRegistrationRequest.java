package hu.bme.aut.adapted.framework.model.auth;

import com.google.gson.annotations.SerializedName;

public class LoginUserRegistrationRequest extends UserRegistrationRequest {
    @SerializedName("Password")
    private String password;
    @SerializedName("Name")
    private String name;
    @SerializedName("Email")
    private String email;
    @SerializedName("PhoneNumber")
    private String phoneNumber;

    public LoginUserRegistrationRequest() {
        super();
    }

    public LoginUserRegistrationRequest(String userName, String password, String name, String email, String phoneNumber, int sex, int birthYear, int birthMonth, int birthDay, int zipCode, String disabilities) {
        super(userName, sex, birthYear, birthMonth, birthDay, zipCode, disabilities);
        this.password = password;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

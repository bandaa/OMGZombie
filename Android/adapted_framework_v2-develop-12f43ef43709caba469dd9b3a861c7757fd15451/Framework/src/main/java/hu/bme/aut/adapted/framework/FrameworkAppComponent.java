package hu.bme.aut.adapted.framework;

import javax.inject.Singleton;

import dagger.Component;
import hu.bme.aut.adapted.commonlib.common.CommonModule;
import hu.bme.aut.adapted.framework.interactor.InteractorModule;
import hu.bme.aut.adapted.framework.interactor.auth.AuthInteractor;
import hu.bme.aut.adapted.framework.interactor.calculation.CalculationInteractor;
import hu.bme.aut.adapted.framework.interactor.classification.ClassificationInteractor;
import hu.bme.aut.adapted.framework.interactor.data.DataInteractor;
import hu.bme.aut.adapted.framework.interactor.settings.SettingsInteractor;
import hu.bme.aut.adapted.framework.interactor.storage.StorageInteractor;
import hu.bme.aut.adapted.framework.interactor.upload.UploadInteractor;
import hu.bme.aut.adapted.framework.network.NetworkModule;
import hu.bme.aut.adapted.framework.repository.DtoConverter;
import hu.bme.aut.adapted.framework.repository.EntityConverter;
import hu.bme.aut.adapted.framework.repository.RepositoryModule;
import hu.bme.aut.adapted.framework.repository.RequeryRepository;
import hu.bme.aut.adapted.framework.sensors.MockSensorContainer;
import hu.bme.aut.adapted.framework.sensors.SensorContainer;
import hu.bme.aut.adapted.framework.sensors.SensorModule;
import hu.bme.aut.adapted.framework.sensors.emotiv.EmotivEEG;
import hu.bme.aut.adapted.framework.sensors.emotiv.MockEmotivEEG;
import hu.bme.aut.adapted.framework.sensors.neurosky.MockNeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.neurosky.NeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.HxMConnectedListener;
import hu.bme.aut.adapted.framework.sensors.zephyr.HxMSensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.MockHxMSensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.RRIntervalCalculator;
import hu.bme.aut.adapted.framework.service.FrameworkService;
import hu.bme.aut.adapted.framework.settings.Settings;
import hu.bme.aut.adapted.framework.util.RemoteGames;
import hu.bme.aut.adapted.framework.util.RequestPermissionActivity;
import hu.bme.aut.adapted.framework.util.ResultActivity;

@Singleton
@Component(modules = {CommonModule.class, RepositoryModule.class, InteractorModule.class, NetworkModule.class, SensorModule.class})
public interface FrameworkAppComponent {
    void inject(FrameworkService frameworkService);

    void inject(NeuroskySensor neuroskySensor);

    void inject(EmotivEEG emotivEEG);

    void inject(HxMSensor hxMSensor);

    void inject(MockNeuroskySensor mockNeuroskySensor);

    void inject(MockEmotivEEG mockEmotivEEG);

    void inject(MockHxMSensor mockHxMSensor);

    void inject(AuthInteractor authInteractor);

    void inject(ClassificationInteractor interactor);

    void inject(DataInteractor dataInteractor);

    void inject(StorageInteractor storageInteractor);

    void inject(UploadInteractor uploadInteractor);

    void inject(CalculationInteractor calculationInteractor);

    void inject(RequeryRepository requeryRepository);

    void inject(EntityConverter entityConverter);

    void inject(DtoConverter dtoConverter);

    void inject(SettingsInteractor settingsInteractor);

    void inject(Settings settings);

    void inject(HxMConnectedListener hxMConnectedListener);

    void inject(SensorContainer sensorContainer);

    void inject(MockSensorContainer mockSensorContainer);

    void inject(RRIntervalCalculator rrIntervalCalculator);

    void inject(RemoteGames remoteGames);

    void inject(RequestPermissionActivity requestPermissionActivity);

    void inject(ResultActivity resultActivity);
}

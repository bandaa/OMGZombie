package hu.bme.aut.adapted.framework.model.database;

import io.requery.Column;
import io.requery.Entity;
import io.requery.ManyToOne;

@Entity
public abstract class RewardParamType extends ParamType
        implements IParamType<RewardType, RewardParamType> {
    @ManyToOne
    @Column(nullable = false)
    RewardType typeDescriptor;

    @Override
    public RewardType getTypeDescriptor() {
        return typeDescriptor;
    }

    @Override
    public void setTypeDescriptor(RewardType typeDescriptor) {
        this.typeDescriptor = typeDescriptor;

        if (!typeDescriptor.getParamTypes().contains(this)) {
            typeDescriptor.getParamTypes().add(this);
        }
    }
}

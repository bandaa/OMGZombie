package hu.bme.aut.adapted.framework.network;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.network.auth.AuthApi;
import hu.bme.aut.adapted.framework.network.classification.ClassificationApi;
import hu.bme.aut.adapted.framework.network.game.DataApi;
import hu.bme.aut.adapted.framework.network.gameplay.GamePlayApi;
import hu.bme.aut.adapted.framework.settings.Settings;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static hu.bme.aut.adapted.commonlib.util.GsonHelper.gson;

/**
 * Created by dorka on 2017.03.31..
 */
@Module
public class NetworkModule {
    @Provides
    @Singleton
    public OkHttpClient.Builder provideOkHttpClientBuilder(final Settings settings) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        clientBuilder.interceptors().add(0, logging);
        return clientBuilder;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(OkHttpClient.Builder builder) {
        return builder.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder().baseUrl(NetworkConfig.SERVICE_ENDPOINT).client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    @Provides
    @Singleton
    public AuthApi provideAuthApi(Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

    @Provides
    @Singleton
    public DataApi provideDataApi(Retrofit retrofit) {
        return retrofit.create(DataApi.class);
    }

    @Provides
    @Singleton
    @Network
    public Executor provideNetworkExecutor() {
        return Executors.newFixedThreadPool(1);
    }

    @Provides
    @Singleton
    public GamePlayApi provideGamePlayApi(Retrofit retrofit){
        return retrofit.create(GamePlayApi.class);
    }

    @Provides
    @Singleton
    public ClassificationApi provideClassificationApi(Retrofit retrofit) {
        return retrofit.create(ClassificationApi.class);
    }

}

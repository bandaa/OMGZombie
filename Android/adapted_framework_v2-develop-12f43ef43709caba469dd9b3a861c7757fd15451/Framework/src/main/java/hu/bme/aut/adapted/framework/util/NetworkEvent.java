package hu.bme.aut.adapted.framework.util;


import hu.bme.aut.adapted.commonlib.util.Event;

public abstract class NetworkEvent extends Event {
    private int code;

    public NetworkEvent() {
        super();
    }

    public NetworkEvent(int code, Throwable throwable) {
        super(throwable);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}

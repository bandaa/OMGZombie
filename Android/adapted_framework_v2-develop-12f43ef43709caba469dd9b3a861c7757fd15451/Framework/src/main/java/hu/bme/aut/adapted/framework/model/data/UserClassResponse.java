package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

public class UserClassResponse extends Response {

    @SerializedName("Class")
    private UserClass userClass;

    public UserClassResponse() {
    }

    public UserClassResponse(boolean succeeded, String message, UserClass userClass) {
        super(succeeded, message);
        this.userClass = userClass;
    }

    public UserClass getUserClass() {
        return userClass;
    }

    public void setUserClass(UserClass userClass) {
        this.userClass = userClass;
    }
}

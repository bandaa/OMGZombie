package hu.bme.aut.adapted.framework.ui.settings;

import android.os.Bundle;
import android.preference.CheckBoxPreference;

import hu.bme.aut.adapted.framework.R;

import static hu.bme.aut.adapted.commonlib.util.DeviceInfoUtils.deviceHasSoftKeys;

public class ScreenSettingsFragment extends BaseSettingsFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_screen);

        boolean hasSoftKeys = deviceHasSoftKeys(getActivity());

        CheckBoxPreference hideNavigationBarPreference = (CheckBoxPreference) findPreference(getString(R.string.screen_hide_navigation_bar));

        if (!hasSoftKeys) {
            hideNavigationBarPreference.setChecked(true);
            hideNavigationBarPreference.setSummary(R.string.pref_description_screen_hide_navigation_bar_no_soft_keys);
        }

        hideNavigationBarPreference.setEnabled(hasSoftKeys);
    }
}

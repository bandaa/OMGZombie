package hu.bme.aut.adapted.framework.interactor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.bme.aut.adapted.framework.interactor.auth.AuthInteractor;
import hu.bme.aut.adapted.framework.interactor.calculation.CalculationInteractor;
import hu.bme.aut.adapted.framework.interactor.classification.ClassificationInteractor;
import hu.bme.aut.adapted.framework.interactor.settings.SettingsInteractor;
import hu.bme.aut.adapted.framework.interactor.storage.StorageInteractor;
import hu.bme.aut.adapted.framework.interactor.upload.UploadInteractor;

@Module
public class InteractorModule {
    @Provides
    @Singleton
    public AuthInteractor provideAuthInteractor() {
        return new AuthInteractor();
    }

    @Provides
    public ClassificationInteractor provideClassificationInteractor() {
        return new ClassificationInteractor();
    }

    @Provides
    @Singleton
    public StorageInteractor provideDataInteractor() {
        return new StorageInteractor();
    }

    @Provides
    @Singleton
    public UploadInteractor provideUploadInteractor() {
        return new UploadInteractor();
    }

    @Provides
    @Singleton
    public CalculationInteractor provideCalculationInteractor() {
        return new CalculationInteractor();
    }

    @Provides
    @Singleton
    public SettingsInteractor provideSettingsInteractor() {
        return new SettingsInteractor();
    }
}

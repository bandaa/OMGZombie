package hu.bme.aut.adapted.framework.repository;

import java.util.List;
import java.util.concurrent.Callable;

import hu.bme.aut.adapted.framework.model.database.Event;
import hu.bme.aut.adapted.framework.model.database.EventParamType;
import hu.bme.aut.adapted.framework.model.database.EventType;
import hu.bme.aut.adapted.framework.model.database.Game;
import hu.bme.aut.adapted.framework.model.database.Gameplay;
import hu.bme.aut.adapted.framework.model.database.RewardParamType;
import hu.bme.aut.adapted.framework.model.database.RewardType;

public interface Repository {
    Game createGame();

    Gameplay createGameplay();

    EventType createEventType();

    RewardType createRewardType();

    EventParamType createEventParamType();

    RewardParamType createRewardParamType();

    Event createEvent();

    void saveGame(Game game);

    void saveGameplay(Gameplay gameplay, boolean update);

    void saveEventType(EventType eventType, boolean update);

    void saveRewardType(RewardType rewardType, boolean update);

    void saveEventParamTypes(List<? extends EventParamType> eventParamType, boolean update);

    void saveRewardParamTypes(List<? extends RewardParamType> rewardParamTypes, boolean update);

    void saveEvent(Event event);

    void removeEventParamTypes(List<? extends EventParamType> eventParamTypes);

    void removeRewardParamTypes(List<? extends RewardParamType> rewardParamTypes);

    void removeInactiveGameplays(List<Integer> gameplayIds);

    void removeEvents(List<Integer> eventIds);

    List<? extends Game> getAllGames();

    Game getGame(int gameId);

    Gameplay getGameplay(int id);

    Gameplay getActiveGameplay(int gameId);

    EventType getEventType(int id);

    EventType getEventType(String name, int gameId);

    RewardType getRewardType(int id);

    RewardType getRewardType(String name, int gameId);

    List<? extends EventType> getNotUploadedEventTypes(Game game);

    List<? extends RewardType> getNotUploadedRewardTypes(Game game);

    List<? extends Gameplay> getGameplays(Game game);

    List<? extends EventParamType> getEventParamTypes(EventType eventType);

    List<? extends RewardParamType> getRewardParamTypes(RewardType rewardType);

    List<? extends Event> getEvents(Gameplay gameplay);

    void setEventTypesUploaded(List<String> names);

    void setRewardTypesUploaded(List<String> names);

    void setAllGameplaysInactive(int gameId);

    void runInTransaction(Runnable runnable);

    <V> V runInTransaction(Callable<V> callable);
}

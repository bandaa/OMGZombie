package hu.bme.aut.adapted.framework.interactor.calculation;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by Laura on 2016.04.27..
 */
public class TestForBurg {
    double[] a;
    double[] v;
    //double[] BurgRRIntervals;
    int BurgPoles;
    //int debug=0;
    int SampleFrequency = 1;
    final double PI = 3.141592653589793238460;
    int freq_len = 8;
    double[] psd;
    double[] f_out;
    int ind = 0;
    double[] prev_a;
    double[] k;
    double[] ForwardError;
    double[] BackwardError;
    double[] new_f;
    double Temporary;
    double new_crit;
    int decreaseIndex;
    double old_crit;
    double enumerator1;
    double denominator1;
    double denominator2;
    double last_k;
    double new_v;
    int p = 0;
    int n = 0;

    TestForBurg(int poles, int N) {

        BurgPoles = poles;

        a = new double[poles];
        v = new double[poles];
        psd = new double[freq_len];
        f_out = new double[freq_len];
        android.util.Log.d("RR_INTERVAL", "Burg Created");
        ind = 0;
        prev_a = new double[poles];
        k = new double[poles];
        ForwardError = new double[N - 1];
        BackwardError = new double[N - 1];
        new_f = new double[N - 1];
        Temporary = 0.0;
        new_crit = 0.0;
        decreaseIndex = ForwardError.length;
        old_crit = 0.0;
        enumerator1 = 0.0;
        denominator1 = 0.0;
        denominator2 = 0.0;
        last_k = 0.0;
        new_v = 0.0;
        p = 0;
        n = 0;
    }

    void ArBurg(int poles, double[] BurgRRIntervals) {

        //int poles;
        int N = BurgRRIntervals.length;

        for (ind = 0; ind < poles; ind++) {
            a[ind] = 0.0;
            v[ind] = 0.0;
            k[ind] = 0.0;
            prev_a[ind] = 0.0;
        }
        for (ind = 0; ind < N - 1; ind++) {
            ForwardError[ind] = BurgRRIntervals[ind + 1];
            //System.out.println(ForwardError[ind]);
            android.util.Log.d("RR_INTERVAL", "Forward Error: " + ForwardError[ind]);
            BackwardError[ind] = BurgRRIntervals[ind];
        }
        Temporary = 0.0;
        for (ind = 0; ind < N; ind++) {
            Temporary = Temporary + (BurgRRIntervals[ind] * BurgRRIntervals[ind]);
        }
        android.util.Log.d("RR_INTERVAL", "Temporary: " + Temporary);
        v[0] = Temporary / N;
        new_crit = v[0];
        old_crit = 2 * new_crit;
        for (p = 1; p <= BurgPoles; p++) {
            //System.out.println("Beleptem\n");
            enumerator1 = 0.0;
            denominator1 = 0.0;
            denominator2 = 0.0;
            //for (ind = 0; ind < N - 1-(p-1); ind++){
            for (ind = 0; ind < decreaseIndex; ind++) {
                enumerator1 = enumerator1 + ForwardError[ind] * BackwardError[ind];
                // System.out.println(enumerator1);
                android.util.Log.d("RR_INTERVAL", "Enumerator 1: " + enumerator1);
                denominator1 = denominator1 + ForwardError[ind] * ForwardError[ind];
                //System.out.println(denominator1);
                denominator2 = denominator2 + BackwardError[ind] * BackwardError[ind];
            }
            last_k = -2 * enumerator1 / (denominator1 + denominator2);
            //System.out.println(last_k);
            android.util.Log.d("RR_INTERVAL", "last_k: " + last_k);
            new_v = v[0] * (1.0 - (last_k * last_k));
            android.util.Log.d("RR_INTERVAL", "new_v: " + new_v);

            if (p > 1) {
                for (ind = 0; ind <= p - 2; ind++) {
                    a[ind] = prev_a[ind] + last_k * prev_a[p - ind - 2];
                    //System.out.println(a[ind]);
                    android.util.Log.d("RR_INTERVAL", "a: " + a[ind]);

                }
                a[p - 1] = last_k;
            }
            if (p == 1) {
                a[p - 1] = last_k;

            }
            k[p - 1] = last_k;
            v[0] = new_v;
            if (p < poles) {
                for (ind = 0; ind < poles; ind++) {
                    prev_a[ind] = a[ind];
                }
                n = N - p;


                for (ind = 1; ind < n; ind++) {
                    new_f[ind - 1] = ForwardError[ind] + last_k * BackwardError[ind];
                    //System.out.println(new_f[ind-1]);
                    android.util.Log.d("RR_INTERVAL", "new_f: " + new_f[ind - 1]);


                    BackwardError[ind - 1] = BackwardError[ind - 1] + last_k * ForwardError[ind - 1];
                    //System.out.println(BackwardError[ind-1]);
                    /*if (p==80){
                        System.out.println(new_f[ind-1]);
                        System.out.println(BackwardError[ind-1]);
                    }*/
                }
                for (ind = 0; ind < N - 1; ind++) {
                    ForwardError[ind] = new_f[ind];
                }


            }
            decreaseIndex = n - 1;
        }
        /*for (ind=0;ind<a.length;ind++){
            System.out.println(a[ind]);
        }*/

        return;
    }


    void ar_psd(int poles) {
        double temp = 1.0;
        int pad_fact = 2;
        int cnt = 0;
        int cnt2 = 0;
        int fft_len = 0;
        int len_coeffs = poles;

        double[] coefficients = new double[len_coeffs + 1];
        double[] freq = new double[freq_len];
        ComplexNumber[] auxiliarySpectrum = new ComplexNumber[freq_len];
        ComplexNumber temporary = new ComplexNumber();
        ComplexNumber[] fft_out = new ComplexNumber[freq_len];
        for (cnt = 0; cnt < fft_out.length; cnt++) {
            fft_out[cnt] = new ComplexNumber();

        }
        ComplexNumber Sum = new ComplexNumber();
        Sum.setReal(0.0);
        Sum.setImaginary(0.0);
        ComplexNumber temporaryComplex = new ComplexNumber();
        ComplexNumber localComplex1 = new ComplexNumber();
        ComplexNumber localComplex2 = new ComplexNumber();

        for (cnt = 0; cnt < psd.length; cnt++) {
            psd[cnt] = 0;
        }

        for (cnt = 0; cnt < freq_len; cnt++) {
            temp = (SampleFrequency * cnt);
            temp = temp / pad_fact;
            temp = temp / freq_len;
            freq[cnt] = temp;

        }
        fft_len = freq_len * pad_fact;
        len_coeffs = poles;
        //System.out.println(len_coeffs);
        for (cnt = 1; cnt <= len_coeffs; cnt++) {
            coefficients[coefficients.length - cnt - 1] = a[cnt - 1];
            //System.out.println(coefficients[len_coeffs - cnt]);


        }
        coefficients[coefficients.length - 1] = 1.0;
        /*for (cnt=0;cnt<coefficients.length;cnt++) {
            System.out.println(coefficients[cnt]);

        }*/
        len_coeffs = coefficients.length;
        for (cnt = 0; cnt < freq_len; cnt++) {
            Sum.setReal(0.0);
            Sum.setImaginary(0.0);
            for (cnt2 = 0; cnt2 < len_coeffs; cnt2++) {
                localComplex1.setReal(coefficients[len_coeffs - cnt2 - 1]);
                localComplex2.setImaginary(0.0);
                localComplex2.setReal(cos(((-2 * PI * freq[cnt]) / SampleFrequency) * (len_coeffs - cnt2 - 1)));
                android.util.Log.d("RR_INTERVAL", "localcomplex 2 real: " + localComplex2.getReal());

                localComplex2.setImaginary(sin(((-2 * PI * freq[cnt]) / SampleFrequency) * (len_coeffs - cnt2 - 1)));
                android.util.Log.d("RR_INTERVAL", "localcomplex 2 imag: " + localComplex2.getImaginary());
                Sum = Sum.add(localComplex1.multiply(localComplex2));


            }

            fft_out[cnt].setReal(Sum.getReal());
            fft_out[cnt].setImaginary(Sum.getImaginary());
            //System.out.println(Sum.getReal());
            android.util.Log.d("RR_INTERVAL", "sum real: " + Sum.getReal());
            //System.out.println(Sum.getImaginary());
            android.util.Log.d("RR_INTERVAL", "sum imag: " + Sum.getImaginary());


        }
        for (cnt = 0; cnt < freq_len; cnt++) {

            //System.out.println(fft_out[cnt].getReal());
            //System.out.println(fft_out[cnt].getImaginary());

            ComplexNumber tempConj = new ComplexNumber();
            //tempConj=fft_out[cnt];
            tempConj.setReal(fft_out[cnt].getReal());
            tempConj.setImaginary(fft_out[cnt].getImaginary());
            //System.out.println(tempConj.getReal());
            //System.out.println(tempConj.getImaginary());

            //System.out.println(tempConj.getReal());
            //System.out.println(tempConj.getImaginary());
            //temporaryComplex=fft_out[cnt].multiply(tempConj);

            temporaryComplex.setReal(tempConj.multiplyByConj());
            temporaryComplex.setImaginary(0.0);
            android.util.Log.d("RR_INTERVAL", "temporaryComplex real: " + temporaryComplex.getReal());
            android.util.Log.d("RR_INTERVAL", "temporaryComplex imag: " + temporaryComplex.getReal());

            //System.out.println(temporaryComplex.getReal());
            //Idáig jó
            Sum.setReal((v[0] / SampleFrequency) / (temporaryComplex.getReal()));
            android.util.Log.d("RR_INTERVAL", "v[0]: " + v[0]);
            android.util.Log.d("RR_INTERVAL", "SampleFreq : " + SampleFrequency);
            //System.out.println(Sum.getReal());
            psd[cnt] = 2 * Sum.getReal();
            if (psd[cnt] < 0) {
                psd[cnt] = psd[cnt] * (-1.0);
            }
            // System.out.println(psd[cnt]);
            android.util.Log.d("RR_INTERVAL", "psd cnt: " + psd[cnt]);


        }

        for (cnt = 0; cnt < freq_len; cnt++) {
            f_out[cnt] = freq[cnt];
            //System.out.println(f_out[cnt]);
            android.util.Log.d("RR_INTERVAL", "f_out: " + f_out[cnt]);

        }
        return;
    }

    public double doBurgCalculation(double[] RRIntervals) {

        int BurgCalcIndex1 = 0;
        int BurgCalcIndex2 = 0;
        int BurgCalcIndex3 = 0;
        double BurgCalcSum = 0.0;
        //double [] BurgRRIntervals = RRIntervals;
        ArBurg(BurgPoles, RRIntervals);
        ar_psd(BurgPoles);

       /* for (BurgCalcIndex1=0;BurgCalcIndex1<psd.length;BurgCalcIndex1++){
            System.out.println(psd[BurgCalcIndex1]);
        }*/
        for (BurgCalcIndex1 = 0; (f_out[BurgCalcIndex1] < 0.07 && BurgCalcIndex1 < freq_len); BurgCalcIndex1++)
            ;


        BurgCalcIndex1--;
        //System.out.println(BurgCalcIndex1);
        for (BurgCalcIndex2 = BurgCalcIndex1; (f_out[BurgCalcIndex2] < 0.14 && BurgCalcIndex2 < freq_len); BurgCalcIndex2++)
            ;


        //BurgCalcIndex2--;
        //System.out.println(BurgCalcIndex2);
        BurgCalcSum = 0.0;
        for (BurgCalcIndex3 = BurgCalcIndex1; BurgCalcIndex3 <= BurgCalcIndex2; BurgCalcIndex3++) {
            //System.out.println(psd[BurgCalcIndex3]);
            BurgCalcSum = BurgCalcSum + psd[BurgCalcIndex3];
            //System.out.println(BurgCalcSum);
            //System.out.println("Beleptem\n");

        }
        //if (BurgCalcIndex1!=BurgCalcIndex2) {

        //BurgCalcSum = BurgCalcSum / (BurgCalcIndex2 - BurgCalcIndex1+1);
        //}
        //System.out.println(BurgCalcSum);
        android.util.Log.d("RR_INTERVAL", "Entered Burg Calculations done AR and psd");
        return BurgCalcSum;


    }

}



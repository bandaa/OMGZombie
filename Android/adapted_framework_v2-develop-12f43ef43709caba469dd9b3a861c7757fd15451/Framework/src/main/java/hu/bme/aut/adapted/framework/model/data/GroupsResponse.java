package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hu.bme.aut.adapted.framework.model.Response;

/**
 * Created by dorka on 2017.03.12..
 */

public class GroupsResponse extends Response {
    @SerializedName("Groups")
    private List<Group> groups;

    public GroupsResponse(boolean succeeded, String message, List<Group> groups) {
        super(succeeded, message);
        this.groups = groups;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public GroupsResponse() {
    }
}

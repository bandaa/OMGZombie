package hu.bme.aut.adapted.framework.model.database;

import java.util.ArrayList;
import java.util.List;

import io.requery.Column;
import io.requery.Entity;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Persistable;

@Entity
public abstract class Game implements Persistable {
    @Key
    int id;

    @Column(nullable = false, unique = true)
    String name;

    @OneToMany
    List<EventType> eventTypes;

    @OneToMany
    List<RewardType> rewardTypes;

    @OneToMany
    List<Gameplay> gameplays;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EventType> getEventTypes() {
        if (eventTypes == null) {
            eventTypes = new ArrayList<>();
        }

        return eventTypes;
    }

    public List<RewardType> getRewardTypes() {
        if (rewardTypes == null) {
            rewardTypes = new ArrayList<>();
        }

        return rewardTypes;
    }

    public List<Gameplay> getGameplays() {
        if (gameplays == null) {
            gameplays = new ArrayList<>();
        }

        return gameplays;
    }
}

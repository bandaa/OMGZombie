package hu.bme.aut.adapted.framework.model.database;

import java.util.List;

public interface ITypeDescriptor<T extends ITypeDescriptor<T, P>, P extends IParamType<T, P>> {
    int getId();

    String getName();

    void setName(String name);

    Game getGame();

    void setGame(Game game);

    List<P> getParamTypes();

    boolean isUploaded();

    void setUploaded(boolean uploaded);
}

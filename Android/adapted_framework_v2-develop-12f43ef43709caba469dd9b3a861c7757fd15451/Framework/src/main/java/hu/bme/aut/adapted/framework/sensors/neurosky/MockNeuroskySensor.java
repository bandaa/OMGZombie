package hu.bme.aut.adapted.framework.sensors.neurosky;

import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.security.SecureRandom;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.NeuroSkyEEG;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.framework.model.event.sensors.SensorEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.neurosky.AttentionChangedGameEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.neurosky.MeditationChangedGameEvent;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;
import hu.bme.aut.adapted.framework.service.event.MessageEvent;

import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.OFFLINE;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.ONLINE;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class MockNeuroskySensor implements INeuroskySensor {
    private static final long MOCK_PERIOD = 1000L;

    @Inject
    EventBus bus;

    private Status status = OFFLINE;

    private Handler mockEventsHandler;

    private SecureRandom random;

    public MockNeuroskySensor() {
        injector.inject(this);
    }

    private final Runnable sendMockEventsTask = () -> {
        sendMockEvents();
        mockEventsHandler.postDelayed(this.sendMockEventsTask, MOCK_PERIOD);
    };

    @Override
    public void connect() {
        if (mockEventsHandler == null) {
            mockEventsHandler = new Handler();
        } else {
            mockEventsHandler.removeCallbacksAndMessages(null);
        }
        random = new SecureRandom();
        mockEventsHandler.post(sendMockEventsTask);

        status = ONLINE;
        sendStatus();
    }

    private void sendMockEvents() {
        long timestamp = System.currentTimeMillis();

        Log.d(getClass().getSimpleName(), "Logged mock eeg values.");

        int meditation = random.nextInt(100);
        int attention = random.nextInt(100);

        bus.post(new FrameworkEvent(new MeditationChangedGameEvent(meditation), timestamp, false));
        bus.post(new MessageEvent(g -> g.sendMeditation(meditation)));

        bus.post(new FrameworkEvent(new AttentionChangedGameEvent(attention), timestamp, false));
        bus.post(new MessageEvent(g -> g.sendAttention(attention)));
    }

    @Override
    public void disconnect() {
        mockEventsHandler.removeCallbacksAndMessages(null);

        status = OFFLINE;
        sendStatus();
    }

    @Override
    public void sendStatus() {
        bus.post(new SensorEvent(NeuroSkyEEG.ACTION_RESPONSE_STATUS, status));
    }
}

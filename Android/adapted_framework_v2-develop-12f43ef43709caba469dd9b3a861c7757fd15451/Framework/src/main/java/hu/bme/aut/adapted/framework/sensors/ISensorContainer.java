package hu.bme.aut.adapted.framework.sensors;

import hu.bme.aut.adapted.framework.sensors.emotiv.IEmotivEEG;
import hu.bme.aut.adapted.framework.sensors.neurosky.INeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.IHxMSensor;

public interface ISensorContainer {
    INeuroskySensor getNeuroskySensor();

    IHxMSensor getHxMSensor();

    IEmotivEEG getEmotivEEG();

    Iterable<ISensor> getAll();
}

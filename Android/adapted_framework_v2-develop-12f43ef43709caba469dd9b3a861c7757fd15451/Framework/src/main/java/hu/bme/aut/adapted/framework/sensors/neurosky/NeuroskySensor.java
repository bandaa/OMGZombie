package hu.bme.aut.adapted.framework.sensors.neurosky;

import android.bluetooth.BluetoothAdapter;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.neurosky.connection.ConnectionStates;
import com.neurosky.connection.DataType.MindDataType;
import com.neurosky.connection.EEGPower;
import com.neurosky.connection.TgStreamHandler;
import com.neurosky.connection.TgStreamReader;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.NeuroSkyEEG;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.framework.model.event.sensors.SensorEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.neurosky.AttentionChangedGameEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.neurosky.MeditationChangedGameEvent;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;
import hu.bme.aut.adapted.framework.service.event.MessageEvent;

import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.CONNECTING;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.OFFLINE;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.ONLINE;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class NeuroskySensor implements INeuroskySensor {
    private static final String TAG = "NeuroskySensor";

    private static final int MSG_UPDATE_BAD_PACKET = 1001;
    private static final int MSG_UPDATE_STATE = 1002;

    private static int badPacketCount = 0;

    @Inject
    EventBus bus;

    private Status status = OFFLINE;
    private TgStreamReader tgStreamReader;

    public NeuroskySensor() {
        injector.inject(this);
    }

    public void connect() {
        // TODO szebben megcsinálni!!!
        BluetoothAdapter bluetoothAdapter;

        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        } catch (Exception e) {
            Log.e(TAG, "error:" + e.getMessage());
            return;
        }

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            //TODO: SEND MESSAGE TO GAME TO ENABLE BLUETOOTH
            Log.d(TAG, "Bluetooth not enabled. Please enable Bluetooth and restart app!");
            return;
        }

        // Example of constructor public TgStreamReader(BluetoothAdapter ba, TgStreamHandler tgStreamHandler)
        tgStreamReader = new TgStreamReader(bluetoothAdapter, callback);
        // (2) Demo of setGetDataTimeOutTime, the default time is 5s, please call it before connect() of connectAndStart()
        tgStreamReader.setGetDataTimeOutTime(20);
        // (3) Demo of startLog, you will get more sdk log by logcat if you call this function
        tgStreamReader.startLog();

        tgStreamReader.connect();
    }

    @Override
    public void disconnect() {
        tgStreamReader.close();
    }

    private TgStreamHandler callback = new TgStreamHandler() {
        @Override
        public void onStatesChanged(int connectionStates) {
            Log.d(TAG, "connectionStates change to: " + connectionStates);
            switch (connectionStates) {
                case ConnectionStates.STATE_CONNECTING:
                    status = CONNECTING;
                    sendStatus();
                    break;
                case ConnectionStates.STATE_CONNECTED:
                    tgStreamReader.start();
                    status = ONLINE;
                    sendStatus();
                    break;
                case ConnectionStates.STATE_WORKING:
                    // Do something when working

                    //(9) demo of recording raw data , stop() will call stopRecordRawData,
                    //or you can add a button to control it.
                    //You can change the save path by calling setRecordStreamFilePath(String filePath) before startRecordRawData
                    tgStreamReader.startRecordRawData();

                    break;
                case ConnectionStates.STATE_GET_DATA_TIME_OUT:
                    // Do something when getting data timeout

                    //(9) demo of recording raw data, exception handling
                    tgStreamReader.stopRecordRawData();

                    Log.d(TAG, "Get data time out!");
                    break;
                case ConnectionStates.STATE_STOPPED:
                    // Do something when stopped
                    // We have to call tgStreamReader.stop() and tgStreamReader.close() much more than
                    // tgStreamReader.connectAndstart(), because we have to prepare for that.
                    break;
                case ConnectionStates.STATE_DISCONNECTED:
                    status = OFFLINE;
                    sendStatus();
                    break;
                case ConnectionStates.STATE_ERROR:
                    // Do something when you get error message
                    break;
                case ConnectionStates.STATE_FAILED:
                    // Do something when you get failed message
                    // It always happens when open the BluetoothSocket error or timeout
                    // Maybe the device is not working normal.
                    // Maybe you have to try again
                    break;
            }
            Message msg = linkDetectedHandler.obtainMessage();
            msg.what = MSG_UPDATE_STATE;
            msg.arg1 = connectionStates;
            linkDetectedHandler.sendMessage(msg);
        }

        @Override
        public void onRecordFail(int flag) {
            // You can handle the record error message here
            Log.e(TAG, "onRecordFail: " + flag);
        }

        @Override
        public void onChecksumFail(byte[] payload, int length, int checksum) {
            // You can handle the bad packets here.
            badPacketCount++;
            Message msg = linkDetectedHandler.obtainMessage();
            msg.what = MSG_UPDATE_BAD_PACKET;
            msg.arg1 = badPacketCount;
            linkDetectedHandler.sendMessage(msg);
        }

        @Override
        public void onDataReceived(int datatype, int data, Object obj) {
            // You can handle the received data here
            // You can feed the raw data to algo sdk here if necessary.

            Message msg = linkDetectedHandler.obtainMessage();
            msg.what = datatype;
            msg.arg1 = data;
            msg.obj = obj;
            linkDetectedHandler.sendMessage(msg);

            Log.i(TAG, "onDataReceived");
        }

    };

    @Override
    public void sendStatus() {
        bus.post(new SensorEvent(NeuroSkyEEG.ACTION_RESPONSE_STATUS, status));
    }

    private Handler linkDetectedHandler = new Handler(msg -> {
        long timestamp = System.currentTimeMillis();

        switch (msg.what) {
            case MindDataType.CODE_RAW:
                int raw = msg.arg1;
                Log.d(TAG, "Raw data? " + Integer.toString(raw));
                break;
            case MindDataType.CODE_MEDITATION:
                int meditation = msg.arg1;
                Log.d(TAG, "CODE_MEDITATION " + meditation);
                MeditationChangedGameEvent mcge = new MeditationChangedGameEvent(meditation);
                bus.post(new FrameworkEvent(mcge, timestamp, false));
                bus.post(new MessageEvent(g -> g.sendMeditation(meditation)));
                break;
            case MindDataType.CODE_ATTENTION:
                int attention = msg.arg1;
                Log.d(TAG, "CODE_ATTENTION " + attention);
                AttentionChangedGameEvent acge = new AttentionChangedGameEvent(attention);
                bus.post(new FrameworkEvent(acge, timestamp, false));
                bus.post(new MessageEvent(g -> g.sendAttention(attention)));
                break;
            case MindDataType.CODE_EEGPOWER:
                EEGPower power = (EEGPower) msg.obj;
                if (power.isValidate()) {
                    Log.d(TAG, "EEGPOWER Delta: " + Integer.toString(power.delta) +
                            " Theta: " + Integer.toString(power.theta) +
                            " LowAlpha: " + Integer.toString(power.lowAlpha) +
                            " HighAlpha: " + Integer.toString(power.highAlpha) +
                            " LowBeta: " + Integer.toString(power.lowBeta) +
                            " HighBeta: " + Integer.toString(power.highBeta) +
                            " LowGamma: " + Integer.toString(power.lowGamma) +
                            " MiddleGamma: " + Integer.toString(power.middleGamma)
                    );
                }
                break;
            case MindDataType.CODE_POOR_SIGNAL:
                int poorSignal = msg.arg1;
                Log.d(TAG, "poorSignal:" + poorSignal);
                break;
            case MSG_UPDATE_BAD_PACKET:
                break;
            default:
                break;
        }

        return true;
    });
}

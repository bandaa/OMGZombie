package hu.bme.aut.adapted.framework.model.event.sensors;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;

public class SensorEvent {
    private String action;
    private Status status;

    public SensorEvent(String action, Status status) {
        this.action = action;
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public Status getStatus() {
        return status;
    }
}

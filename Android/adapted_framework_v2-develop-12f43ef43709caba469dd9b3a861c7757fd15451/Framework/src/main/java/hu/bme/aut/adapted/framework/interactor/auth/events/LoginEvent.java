package hu.bme.aut.adapted.framework.interactor.auth.events;


import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.model.auth.LoginResponse;

public class LoginEvent extends AuthNetworkEvent<LoginRequest, LoginResponse> {
}

package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

public class GameNameResponse extends Response {

    @SerializedName("Name")
    private String name;

    public GameNameResponse(boolean succeeded, String message, String name) {
        super(succeeded, message);
        this.name = name;
    }

    public GameNameResponse() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

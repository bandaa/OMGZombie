package hu.bme.aut.adapted.framework.interactor.settings;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.settings.Settings;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class SettingsInteractor {
    @Inject
    Settings settings;

    public SettingsInteractor() {
        injector.inject(this);
    }

    public hu.bme.aut.adapted.commonlib.ipc.payload.Settings getSettings() {
        return new hu.bme.aut.adapted.commonlib.ipc.payload.Settings(
                settings.isEnableAdmin(), settings.isAutomaticLock(),
                settings.isLockTaskModeEnabled(), settings.isBlockHomeButton(),
                settings.isBlockBackButton(), settings.isWakelock(), settings.isHideNavigationBar(),
                settings.isFullBrightness(), settings.isHideActionBar(), settings.isKeepInFront(),
                settings.isSoundEnabled());
    }
}

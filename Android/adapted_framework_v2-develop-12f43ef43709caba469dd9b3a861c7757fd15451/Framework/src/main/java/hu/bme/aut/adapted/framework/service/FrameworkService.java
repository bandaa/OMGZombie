package hu.bme.aut.adapted.framework.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.ipc.IAuth;
import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;
import hu.bme.aut.adapted.commonlib.ipc.IFramework;
import hu.bme.aut.adapted.commonlib.ipc.IGame;
import hu.bme.aut.adapted.commonlib.ipc.payload.EventType;
import hu.bme.aut.adapted.commonlib.ipc.payload.GameEvent;
import hu.bme.aut.adapted.commonlib.ipc.payload.RewardType;
import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.exception.HRMConnectionFailedException;
import hu.bme.aut.adapted.framework.interactor.auth.AuthInteractor;
import hu.bme.aut.adapted.framework.interactor.auth.events.LoginEvent;
import hu.bme.aut.adapted.framework.interactor.auth.events.RegistrationEvent;
import hu.bme.aut.adapted.framework.interactor.settings.SettingsInteractor;
import hu.bme.aut.adapted.framework.interactor.storage.StorageInteractor;
import hu.bme.aut.adapted.framework.interactor.upload.UploadInteractor;
import hu.bme.aut.adapted.framework.interactor.upload.event.GamePlayUploadEvent;
import hu.bme.aut.adapted.framework.ipc.Framework;
import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.model.auth.LoginUserRegistrationRequest;
import hu.bme.aut.adapted.framework.model.database.Gameplay;
import hu.bme.aut.adapted.framework.model.dto.GameDTO;
import hu.bme.aut.adapted.framework.model.dto.GamePlayResponse;
import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadRequest;
import hu.bme.aut.adapted.framework.model.event.ScreenshotEvent;
import hu.bme.aut.adapted.framework.model.event.sensors.SensorEvent;
import hu.bme.aut.adapted.framework.sensors.ISensor;
import hu.bme.aut.adapted.framework.sensors.ISensorContainer;
import hu.bme.aut.adapted.framework.sensors.MockSensorContainer;
import hu.bme.aut.adapted.framework.sensors.SensorContainer;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;
import hu.bme.aut.adapted.framework.service.event.MessageEvent;
import hu.bme.aut.adapted.framework.settings.Settings;
import hu.bme.aut.adapted.framework.util.RemoteGames;

import static android.widget.Toast.LENGTH_SHORT;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createEventType;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createGameEvent;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.createRewardType;
import static hu.bme.aut.adapted.commonlib.util.ReflectionUtils.getRewardClass;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;
import static org.greenrobot.eventbus.ThreadMode.MAIN;

public class FrameworkService extends Service {
    private static final String TAG = FrameworkService.class.getSimpleName();

    private final Handler handler = new Handler(Looper.getMainLooper());

    @Inject
    AuthInteractor authInteractor;

    @Inject
    StorageInteractor storageInteractor;

    @Inject
    UploadInteractor uploadInteractor;

    @Inject
    SettingsInteractor settingsInteractor;

    @Inject
    EventBus bus;

    @Inject
    @Network
    Handler backgroundHandler;

    @Inject
    Settings settings;

    private RemoteGames games;
    private boolean uploading;

    private final IAuth auth = new IAuth.Stub() {
        @Override
        public void login(IAuthCallback callback, String username, String password) {
            backgroundHandler.post(() -> authInteractor.login(callback, new LoginRequest(
                    username, password)));
        }

        @Override
        public void register(IAuthCallback callback, String username, String password,
                             String email, String phoneNumber, int sex,
                             int birthYear, int birthMonth, int birthDay, int zipCode,
                             String disabilities) {
            backgroundHandler.post(() -> authInteractor.register(callback,
                    new LoginUserRegistrationRequest(username, password, username,
                            email, phoneNumber, sex,
                            birthYear, birthMonth, birthDay, zipCode,
                            disabilities)));
        }
    };

    private final Framework framework = new Framework(handler) {
        @Override
        public hu.bme.aut.adapted.commonlib.ipc.payload.Settings registerGame(IGame game, int gameId) {
            games.add(game, gameId);
            return settingsInteractor.getSettings();
        }

        @Override
        public void unregisterGame(IGame game) {
            games.remove(game);
        }

        @Override
        public int startGameplay(int gameId, String sessionKey) {
            boolean active = storageInteractor.isGameplayActive(gameId);
            int id = storageInteractor.startNewGameplay(gameId, sessionKey);

            if (active) {
                handler.post(() -> startUploadToServer());
            }

            return id;
        }

        @Override
        public void stopGameplay(int gameId) {
            storageInteractor.stopGameplay(gameId);
            handler.post(() -> startUploadToServer());
        }

        @Override
        public int registerEventType(int gameId, EventType eventType) {
            return storageInteractor.addEventType(eventType, gameId);
        }

        @Override
        public int registerRewardType(int gameId, RewardType rewardType) {
            return storageInteractor.addRewardType(rewardType, gameId);
        }

        @Override
        public void sendGameEvent(int gameplayId, GameEvent event) {
            sendDebugMessage("Event received");

            storageInteractor.addDynamicEvent(event, gameplayId);
        }

        @Override
        public void sendScreenshot(int gameplayId, byte[] screenshot, boolean large) {
            sendDebugMessage("Screenshot received");

            ScreenshotEvent eventObj = new ScreenshotEvent(screenshot);

            storageInteractor.addStaticEvent(eventObj, System.currentTimeMillis(), gameplayId);
        }

        @Override
        public void connectNeurosky() {
            getSensors().getNeuroskySensor().connect();
        }

        @Override
        public void disconnectNeurosky() {
            getSensors().getNeuroskySensor().disconnect();
        }

        @Override
        public void connectZephyr() {
            try {
                getSensors().getHxMSensor().connect();
            } catch (HRMConnectionFailedException e) {
                Log.e("FrameworkService", e.message);
            }
        }

        @Override
        public void disconnectZephyr() {
            getSensors().getHxMSensor().disconnect();
        }

        @Override
        public void connectEmotiv() {
            getSensors().getEmotivEEG().connect();
        }

        @Override
        public void disconnectEmotiv() {
            getSensors().getEmotivEEG().disconnect();
        }

        @Override
        public void startUpload() {
            startUploadToServer();
        }

        @Override
        public void updateSensorStatus() {
            for (ISensor sensor : getSensors().getAll()) {
                sensor.sendStatus();
            }
        }

        @Override
        public boolean test(Bundle bundle) {
            return true;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        injector.inject(this);

        games = new RemoteGames();
        uploading = false;

        bus.register(this);

        handler.post(this::startUploadToServerPeriodical);
        handler.post(this::takeScreenshotPeriodical);
    }

    @Override
    public IBinder onBind(Intent intent) {
        sendDebugMessage("Framework bound");

        if (IAuth.class.getName().equals(intent.getAction())) {
            return auth.asBinder();
        } else if (IFramework.class.getName().equals(intent.getAction())) {
            return framework.asBinder();
        } else {
            return null;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        sendDebugMessage("Framework unbound");

        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        backgroundHandler.removeCallbacksAndMessages(null);
        bus.unregister(this);
        games.kill();
    }

    private void sendDebugMessage(String message) {
        Log.d(TAG, message);
        Toast.makeText(getApplicationContext(), message, LENGTH_SHORT).show();
    }

    private void sendErrorMessage(String message) {
        Log.e(TAG, message);
        Toast.makeText(getApplicationContext(), message, LENGTH_SHORT).show();
    }

    private void startUploadToServer() {
        if (uploading) {
            return;
        }

        uploading = true;

        List<GameDTO> games = storageInteractor.getAllGames();

        if (!games.isEmpty()) {
            sendDebugMessage("Upload started");

            backgroundHandler.post(() -> uploadInteractor.uploadData(new GamePlayUploadRequest(games)));
        } else {
            sendDebugMessage("Upload started, nothing to upload");

            uploading = false;
        }
    }

    private void startUploadToServerPeriodical() {
        startUploadToServer();
        handler.postDelayed(this::startUploadToServerPeriodical, settings.getUploadPeriod());
    }

    private void takeScreenshot() {
        Log.d("SCREENSHOT", "method called...");

        games.sendToAll(game -> {
            int gameId = games.getGameId(game);

            if (storageInteractor.isGameplayActive(gameId)) {
                Log.d("SCREENSHOT", "\"take screenshot\" event sent...");
                game.takeScreenshot();
            } else {
                Log.e("SCREENSHOT", "GAMEPLAY NOT ACTIVE IN GAME: " + gameId);
            }
        });
    }

    private void takeScreenshotPeriodical() {
        takeScreenshot();
        handler.postDelayed(this::takeScreenshotPeriodical, settings.getScreenshotPeriod());
    }

    @Subscribe(threadMode = MAIN)
    public void onLoginEvent(LoginEvent event) {
        try {
            boolean success = event.getThrowable() == null &&
                    event.getResponse() != null &&
                    event.getResponse().isSucceeded() &&
                    event.getResponse().isAuthenticated() &&
                    event.getResponse().getUserId() > 0 &&
                    event.getResponse().getSessionKey() != null &&
                    !event.getResponse().getSessionKey().equals("N/A");

            event.getCallback().onLogin(success ? event.getResponse().getSessionKey() : null);
            Log.d(TAG, "on login: success: " + success + ", callback called");
        } catch (RemoteException e) {
            Log.e(TAG, e.getClass().getSimpleName() + " on login");
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onRegistrationEvent(RegistrationEvent event) {
        try {
            boolean success = event.getThrowable() == null;
            event.getCallback().onRegister(success,
                    event.getRequest() != null ? event.getRequest().getUserName() : null,
                    event.getResponse() != null ? event.getResponse().getMessage() : null);
            Log.d(TAG, "on register: success: " + success + ", callback called");
        } catch (RemoteException e) {
            Log.e(TAG, e.getClass().getSimpleName() + " on register");
        }
    }

    @Subscribe(threadMode = MAIN)
    public void onGamePlayUploadEvent(GamePlayUploadEvent event) {
        if (event.getThrowable() != null) {
            String message = null;

            if (event.getResponse() != null) {
                message = event.getResponse().getMessage();
            }

            Log.e("UPLOAD", "Failed to upload gameplays (" + event.getThrowable().getClass().getSimpleName() + "): " + message + "!");
            sendErrorMessage("Upload failed");
            return;
        }

        Map<Integer, Integer> gamePlayIds = new HashMap<>();
        List<Integer> eventIds = new ArrayList<>();

        boolean allSuccess = true;

        for (GamePlayResponse response : event.getResponse().getGamePlays()) {
            boolean success = response.getServerId() != null && response.getServerId() > 0;

            if (success) {
                gamePlayIds.put(response.getLocalId(), response.getServerId());
                eventIds.addAll(response.getLocalEventIds());

                Log.d("UPLOAD", "Successfully uploaded gameplay with id: " + response.getLocalId() + ", serverId: " + response.getServerId());
            } else {
                allSuccess = false;
                Log.e("UPLOAD", "Failed to upload gameplay with id: " + response.getLocalId() + ", message: " + response.getMessage());
            }
        }

        if (allSuccess) {
            sendDebugMessage("Upload finished");
        } else {
            sendErrorMessage("Upload finished with errors");
        }

        storageInteractor.cleanupAfterUpload(event.getEventTypeNames(), event.getRewardTypeNames(),
                gamePlayIds, eventIds);

        uploading = false;
    }

    @Subscribe(threadMode = MAIN)
    public void onSensorEvent(SensorEvent event) {
        games.sendToAll(g -> g.sensorStatusChanged(event.getAction(), event.getStatus().name()));
    }

    @Subscribe(threadMode = MAIN)
    public void onFrameworkEvent(FrameworkEvent event) {
        saveGameEvent(event.getGameEvent(), event.getTimestamp(), event.isDynamic());
    }

    @Subscribe(threadMode = MAIN)
    public void onMessageEvent(MessageEvent event) {
        games.sendToAll(event.getMessage());
    }

    private void saveGameEvent(Object event, long timestamp, boolean dynamic) {
        for (int gameId : games.getGameIds()) {
            Gameplay gameplay = storageInteractor.getActiveGameplay(gameId);

            if (gameplay != null) {
                if (dynamic) {
                    Class<?> eventClass = event.getClass();
                    Class<?> rewardClass = getRewardClass(eventClass);

                    Integer rewardTypeId;

                    if (rewardClass != null) {
                        RewardType rewardType = createRewardType(rewardClass);
                        rewardTypeId = storageInteractor.addRewardType(rewardType, gameId);
                    } else {
                        rewardTypeId = null;
                    }

                    EventType eventType = createEventType(eventClass, rewardTypeId);
                    int eventTypeId = storageInteractor.addEventType(eventType, gameId);

                    GameEvent gameEvent = createGameEvent(event, timestamp, eventTypeId);
                    storageInteractor.addDynamicEvent(gameEvent, gameplay.getId());
                } else {
                    storageInteractor.addStaticEvent(event, timestamp, gameplay.getId());
                }
            }
        }
    }

    private ISensorContainer getSensors() {
        return settings.isMockEeg() ? MockSensorContainer.getInstance() : SensorContainer.getInstance();
    }
}

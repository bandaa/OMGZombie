package hu.bme.aut.adapted.framework.interactor.auth.events;

import hu.bme.aut.adapted.framework.model.auth.LoginUserRegistrationRequest;
import hu.bme.aut.adapted.framework.model.auth.UserRegistrationResponse;

public class RegistrationEvent extends AuthNetworkEvent<LoginUserRegistrationRequest, UserRegistrationResponse> {
}

package hu.bme.aut.adapted.framework.network.game;

import hu.bme.aut.adapted.framework.model.data.GameNameResponse;
import hu.bme.aut.adapted.framework.model.data.GroupsResponse;
import hu.bme.aut.adapted.framework.model.data.UserClassResponse;
import hu.bme.aut.adapted.framework.model.data.UserFullNameResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface DataApi {
    @GET("User/FullName/{id}")
    Call<UserFullNameResponse> getUserFullName(@Path("id") String id);

    @GET("User/Groups/{id}")
    Call<GroupsResponse> getUserGroups(@Path("id") String id);

    @GET("User/Class/{id}")
    Call<UserClassResponse> getUserClass(@Path("id") String id);

    @GET("Game/Name/{id}")
    Call<GameNameResponse> getGameName(@Path("id") String id);

}

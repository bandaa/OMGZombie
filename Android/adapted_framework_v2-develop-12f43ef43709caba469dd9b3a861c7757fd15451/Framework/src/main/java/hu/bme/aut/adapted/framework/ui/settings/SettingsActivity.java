package hu.bme.aut.adapted.framework.ui.settings;

import android.preference.PreferenceFragment;

import java.util.List;

import hu.bme.aut.adapted.framework.R;

import static hu.bme.aut.adapted.commonlib.util.DeviceInfoUtils.deviceHasXLargeScreen;

public class SettingsActivity extends AppCompatPreferenceActivity {
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return deviceHasXLargeScreen(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || AdminSettingsFragment.class.getName().equals(fragmentName)
                || ScreenSettingsFragment.class.getName().equals(fragmentName)
                || ApplicationSettingsFragment.class.getName().equals(fragmentName);
    }
}

package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GamePlayUploadRequest {
    @SerializedName("Games")
    private List<GameDTO> games;

    public GamePlayUploadRequest() {
    }

    public GamePlayUploadRequest(List<GameDTO> games) {
        this.games = games;
    }

    public List<GameDTO> getGames() {
        return games;
    }

    public void setGames(List<GameDTO> games) {
        this.games = games;
    }
}

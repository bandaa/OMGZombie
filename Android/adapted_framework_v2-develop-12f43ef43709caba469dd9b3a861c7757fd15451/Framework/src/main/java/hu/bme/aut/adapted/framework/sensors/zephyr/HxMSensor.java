package hu.bme.aut.adapted.framework.sensors.zephyr;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.HXM;
import hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status;
import hu.bme.aut.adapted.commonlib.util.di.Hpv;
import hu.bme.aut.adapted.commonlib.util.di.Network;
import hu.bme.aut.adapted.framework.exception.HRMConnectionFailedException;
import hu.bme.aut.adapted.framework.interactor.calculation.CalculationInteractor;
import hu.bme.aut.adapted.framework.model.event.sensors.SensorEvent;
import zephyr.android.HxMBT.BTClient;

import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.CONNECTING;
import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.OFFLINE;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class HxMSensor implements IHxMSensor {

    private static final String LOG_TAG = "HxMSensor";
    private static final int DEFAULT_SAMPLES_PER_SEC = 50;
    public static final int DEFAULT_WINDOW_SIZE = 38;

    @Inject
    EventBus bus;

    @Inject
    @Network
    Handler backgroundHandler;

    @Inject
    @Hpv
    Handler hpvHandler;

    @Inject
    CalculationInteractor calculationInteractor;

    private IHrmSignalProcessor signalProcessor;

    private Status status = OFFLINE;
    private BluetoothAdapter bluetoothAdapter = null;
    private BTClient btClient;
    private String TAG = "ZEPHYRSENSOR";

    private long counter;
    private SlidingDoubleBuffer sampledValues;
    private final int samplesPerSec = DEFAULT_SAMPLES_PER_SEC;
    private HxMConnectedListener connectListener;

    public HxMSensor() {

        this.signalProcessor = new HRMSignalProcessor();
        this.sampledValues = new SlidingDoubleBuffer(samplesPerSec * DEFAULT_WINDOW_SIZE);

        injector.inject(this);
    }

    @Override
    public void connect() throws HRMConnectionFailedException {
        status = CONNECTING;
        sendStatus();

        connectListener = new HxMConnectedListener(this, backgroundHandler);

        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        } catch (Exception e) {
            Log.e(TAG, "error:" + e.getMessage());
            return;
        }

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            //TODO: SEND MESSAGE TO GAME TO ENABLE BLUETOOTH
            Log.d(TAG, "Bluetooth not enabled. Please enable Bluetooth and restart app!");
            return;
        }

        String btMAC = null;
        if (bluetoothAdapter != null) {
            for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
                if (device.getName().startsWith("HXM")) {
                    BluetoothDevice btDevice = device;
                    btMAC = btDevice.getAddress();
                    break;
                }
            }
        }
        if (btMAC == null) {
            throw new HRMConnectionFailedException("The HRM device is not paired");
        }

        btClient = new BTClient(bluetoothAdapter, btMAC);
        if (btClient.IsConnected()) {
            btClient.addConnectedEventListener(connectListener);
            btClient.start();
            ///TODO sampling thread
            counter = 0;
            startHpvCalculationPeriodical();
        } else {
            throw new HRMConnectionFailedException("The client is not connected");
        }
    }

    private void startHpvCalculationPeriodical() {
        startHpvCalculation();
        hpvHandler.postDelayed(this::startHpvCalculationPeriodical, 1000 / DEFAULT_SAMPLES_PER_SEC);
    }

    private void startHpvCalculation() {
        counter++;
        sampledValues.put(connectListener.getIrrIntervalCalculator().getCurrentRRInterval());
        if (counter >= sampledValues.getCapacity() && counter % samplesPerSec == 0) {
            Log.d(LOG_TAG, "Calculating PSD");
            double[] psd = signalProcessor.calcPSD(sampledValues.array());
            onPSDCalculated(psd);
        }
    }

    public void onPSDCalculated(double[] psd) {
        FrequencyMagnitude[] psdArray = new FrequencyMagnitude[psd.length];
        double mfPower = 0.0;
        for (int k = 0; k < psd.length; k++) {
            psdArray[k] = new FrequencyMagnitude((double) (k * samplesPerSec) / (double) psd.length, psd[k]);
            String logText = null;
            if (0.07 <= psdArray[k].frequency && psdArray[k].frequency <= 0.15) {
                mfPower += psdArray[k].magnitude;
                logText = "" + psdArray[k].frequency + " ok";
            } else {
                logText = "" + psdArray[k].frequency + " not ok";
            }
            if (k < 10) {
                Log.d("frequency", logText);
            }
        }

        hpvHandler.post(() -> calculationInteractor.calculate(connectListener.getIrrIntervalCalculator().getCurrentRRInterval()));
    }

    @Override
    public void disconnect() {
        if (btClient != null) {
            btClient.interrupt();
            status = OFFLINE;
            sendStatus();
        }
    }

    @Override
    public void sendStatus() {
        bus.post(new SensorEvent(HXM.ACTION_RESPONSE_STATUS, status));
    }

    void setStatus(Status status) {
        this.status = status;
    }
}

package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dorka on 2017.03.12..
 */

public class UserClass {

    @SerializedName("Id")
    private String id;
    @SerializedName("Name")
    private String name;

    public UserClass() {
    }

    public UserClass(String id, String name) {

        this.id = id;
        this.name = name;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

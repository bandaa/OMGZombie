package hu.bme.aut.adapted.framework.model.event;

import android.util.Base64;

import static android.util.Base64.DEFAULT;

public class ScreenshotEvent {
    private String image;

    public ScreenshotEvent(byte[] image) {
        this.image = Base64.encodeToString(image, DEFAULT);
    }

    public String getImage() {
        return image;
    }
}

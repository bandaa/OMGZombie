package hu.bme.aut.adapted.framework.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hu.bme.aut.adapted.framework.model.Response;

public class GamePlayResponse extends Response {
    @SerializedName("LocalId")
    private Integer localId;

    @SerializedName("ServerId")
    private Integer serverId;

    @SerializedName("LocalEventIds")
    private List<Integer> localEventIds;

    public GamePlayResponse() {
        super();
    }

    public GamePlayResponse(boolean succeeded, String message, Integer localId, Integer serverId, List<Integer> localEventIds) {
        super(succeeded, message);
        this.localId = localId;
        this.serverId = serverId;
        this.localEventIds = localEventIds;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Integer getServerId() {
        return serverId;
    }

    public void setServerId(Integer serverId) {
        this.serverId = serverId;
    }

    public List<Integer> getLocalEventIds() {
        return localEventIds;
    }

    public void setLocalEventIds(List<Integer> localEventIds) {
        this.localEventIds = localEventIds;
    }
}

package hu.bme.aut.adapted.framework.model.database;

import io.requery.Column;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Superclass;

@Superclass
public abstract class ParamType implements Persistable {
    @Key
    @Generated
    int id;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String type;

    String minValue;

    String maxValue;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMinValue() {
        return minValue;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }
}

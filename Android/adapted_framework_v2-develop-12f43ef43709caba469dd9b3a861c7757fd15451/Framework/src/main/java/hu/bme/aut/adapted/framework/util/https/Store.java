package hu.bme.aut.adapted.framework.util.https;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import hu.bme.aut.adapted.commonlib.exception.StoreNotInitializedException;
import hu.bme.aut.adapted.framework.R;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;
import static android.preference.PreferenceManager.setDefaultValues;

public class Store {
    private static final String TAG = "Store";
    private static final String ERROR_NOT_INITIALIZED = "Store must be initialized before use!";

    private static SharedPreferences preferences;
    private static Gson gson;

    @SuppressWarnings("rawtypes")
    private static final Map<String, Listener> listeners = new HashMap<>();

    @SuppressWarnings("unchecked")
    public static void init(Context context) {
        setDefaultValues(context, R.xml.pref_admin, false);
        setDefaultValues(context, R.xml.pref_screen, false);
        setDefaultValues(context, R.xml.pref_application, false);

        preferences = getDefaultSharedPreferences(context);
        gson = new GsonBuilder().create();

        preferences.registerOnSharedPreferenceChangeListener((sharedPreferences, key) -> {
            @SuppressWarnings("rawtypes")
            Listener listener = listeners.get(key);

            if (listener != null) {
                listener.onChange(sharedPreferences.getAll().get(key));
            }
        });
    }

    private static void checkInit() {
        if (preferences == null || gson == null) {
            Log.e(TAG, ERROR_NOT_INITIALIZED);
            throw new StoreNotInitializedException();
        }
    }

    public static <T> T get(String key) {
        return get(key, null);
    }

    public static <T> T get(String key, T defaultValue) {
        checkInit();
        return preferences.contains(key) ? (T) preferences.getAll().get(key) : defaultValue;
    }

    @SuppressLint("ApplySharedPref")
    public static void putString(String key, String value) {
        checkInit();
        preferences.edit().putString(key, value).commit();
    }

    @SuppressLint("ApplySharedPref")
    public static void remove(String key) {
        checkInit();
        preferences.edit().remove(key).commit();
    }

    public static <T> void setListener(String key, Listener<T> listener) {
        if (listener != null) {
            listeners.put(key, listener);
        } else {
            listeners.remove(key);
        }
    }

    public interface Listener<T> {
        void onChange(T newValue);
    }
}

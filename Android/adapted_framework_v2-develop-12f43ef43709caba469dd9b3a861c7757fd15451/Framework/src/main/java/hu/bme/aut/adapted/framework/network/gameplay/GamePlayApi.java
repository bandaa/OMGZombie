package hu.bme.aut.adapted.framework.network.gameplay;

import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadRequest;
import hu.bme.aut.adapted.framework.model.dto.GamePlayUploadResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface GamePlayApi {
    @POST("GamePlayUpload")
    Call<GamePlayUploadResponse> uploadGamePlay(@Body GamePlayUploadRequest request);
}

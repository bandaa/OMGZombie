package hu.bme.aut.adapted.framework.model.event.sensors.emotiv;

import hu.bme.aut.adapted.commonlib.annotation.Parameter;

public class PerformanceMetricUpdatedEvent {
    @Parameter
    private final String metric;

    @Parameter(minValue = "0.0", maxValue = "1.0")
    private final float score;

    public PerformanceMetricUpdatedEvent(String metric, float score) {
        this.metric = metric;
        this.score = score;
    }

    public String getMetric() {
        return metric;
    }

    public float getScore() {
        return score;
    }
}

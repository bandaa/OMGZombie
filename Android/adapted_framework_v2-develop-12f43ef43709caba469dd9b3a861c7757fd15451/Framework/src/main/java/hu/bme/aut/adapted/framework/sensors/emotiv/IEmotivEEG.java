package hu.bme.aut.adapted.framework.sensors.emotiv;

import hu.bme.aut.adapted.framework.sensors.ISensor;

public interface IEmotivEEG extends ISensor {
    void connect();

    void disconnect();
}

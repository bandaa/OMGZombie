package hu.bme.aut.adapted.framework.interactor.data.events;

/**
 * Created by dorka on 2017.03.06..
 */

public class UserFullNameEvent extends UserDataEvent {
    private String fullName;

    public UserFullNameEvent(String id, String fullName) {
        super(id);
        this.fullName = fullName;
    }

    public UserFullNameEvent() {
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

package hu.bme.aut.adapted.framework.model.database;

import java.util.ArrayList;
import java.util.List;

import io.requery.Entity;
import io.requery.ManyToOne;
import io.requery.OneToMany;

@Entity
public abstract class EventType extends TypeDescriptor
        implements ITypeDescriptor<EventType, EventParamType> {
    @OneToMany
    List<EventParamType> paramTypes;

    @ManyToOne
    RewardType rewardType;

    @Override
    public void setGame(Game game) {
        super.setGame(game);

        if (!game.getEventTypes().contains(this)) {
            game.getEventTypes().add(this);
        }
    }

    @Override
    public List<EventParamType> getParamTypes() {
        if (paramTypes == null) {
            paramTypes = new ArrayList<>();
        }

        return paramTypes;
    }

    public RewardType getRewardType() {
        return rewardType;
    }

    public void setRewardType(RewardType rewardType) {
        this.rewardType = rewardType;
    }
}

package hu.bme.aut.adapted.framework.interactor.data.events;

import hu.bme.aut.adapted.framework.model.data.UserClass;

/**
 * Created by dorka on 2017.03.11..
 */
public class UserClassEvent extends UserDataEvent{
    private UserClass userClass;

    public UserClassEvent() {
    }

    public UserClassEvent(String userId, UserClass userClass) {
        super(userId);
        this.userClass = userClass;
    }

    public UserClass getUserClass() {
        return userClass;
    }

    public void setUserClass(UserClass userClass) {
        this.userClass = userClass;
    }
}

package hu.bme.aut.adapted.framework.repository;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.sql.EntityDataStore;

import static hu.bme.aut.adapted.framework.model.database.Models.DEFAULT;

@Module
public class RepositoryModule {
    @Provides
    @Singleton
    public EntityDataStore<Persistable> provideDataStore(Context context) {
        DatabaseSource source = new DatabaseSource(context, DEFAULT, "adapted_requery.db", 1);
        source.setLoggingEnabled(true);
        return new EntityDataStore<>(source.getConfiguration());
    }

    @Provides
    @Singleton
    public Repository provideRepository() {
        return new RequeryRepository();
    }

    @Provides
    @Singleton
    public EntityConverter provideEntityConveter() {
        return new EntityConverter();
    }

    @Provides
    @Singleton
    public DtoConverter provideDtoConverter() {
        return new DtoConverter();
    }
}

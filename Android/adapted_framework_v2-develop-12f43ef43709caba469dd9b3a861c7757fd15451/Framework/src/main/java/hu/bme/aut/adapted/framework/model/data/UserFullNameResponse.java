package hu.bme.aut.adapted.framework.model.data;

import com.google.gson.annotations.SerializedName;

import hu.bme.aut.adapted.framework.model.Response;

public class UserFullNameResponse extends Response {
    @SerializedName("Name")
    public String name;

    public UserFullNameResponse(boolean succeeded, String message, String name) {
        super(succeeded, message);
        this.name = name;
    }

    public UserFullNameResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package hu.bme.aut.adapted.framework.sensors;

import java.util.Arrays;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.sensors.emotiv.IEmotivEEG;
import hu.bme.aut.adapted.framework.sensors.neurosky.INeuroskySensor;
import hu.bme.aut.adapted.framework.sensors.zephyr.IHxMSensor;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class SensorContainer implements ISensorContainer {
    @Inject
    INeuroskySensor neuroskySensor;

    @Inject
    IHxMSensor hxMSensor;

    @Inject
    IEmotivEEG emotivEEG;

    private static SensorContainer instance;

    private SensorContainer() {
    }

    public static SensorContainer getInstance() {
        if (instance == null) {
            instance = new SensorContainer();
        }

        return instance;
    }

    private void inject() {
        injector.inject(this);
    }

    @Override
    public INeuroskySensor getNeuroskySensor() {
        if (neuroskySensor == null) {
            inject();
        }

        return neuroskySensor;
    }

    @Override
    public IHxMSensor getHxMSensor() {
        if (hxMSensor == null) {
            inject();
        }

        return hxMSensor;
    }

    @Override
    public IEmotivEEG getEmotivEEG() {
        if (emotivEEG == null) {
            inject();
        }

        return emotivEEG;
    }

    @Override
    public Iterable<ISensor> getAll() {
        return Arrays.asList(getNeuroskySensor(), getHxMSensor(), getEmotivEEG());
    }
}

package hu.bme.aut.adapted.framework.interactor.auth;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.commonlib.ipc.IAuthCallback;
import hu.bme.aut.adapted.framework.exception.NoSessionKeyException;
import hu.bme.aut.adapted.framework.exception.UnexpectedErrorException;
import hu.bme.aut.adapted.framework.exception.UnsuccessfulRequestException;
import hu.bme.aut.adapted.framework.interactor.auth.events.AuthNetworkEvent;
import hu.bme.aut.adapted.framework.interactor.auth.events.LoginEvent;
import hu.bme.aut.adapted.framework.interactor.auth.events.RegistrationEvent;
import hu.bme.aut.adapted.framework.model.auth.LoginRequest;
import hu.bme.aut.adapted.framework.model.auth.LoginResponse;
import hu.bme.aut.adapted.framework.model.auth.LoginUserRegistrationRequest;
import hu.bme.aut.adapted.framework.model.auth.UserRegistrationResponse;
import hu.bme.aut.adapted.framework.network.auth.AuthApi;
import hu.bme.aut.adapted.framework.settings.Settings;
import retrofit2.Call;
import retrofit2.Response;

import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class AuthInteractor {
    @Inject
    Settings settings;
    @Inject
    AuthApi authApi;
    @Inject
    EventBus bus;

    public AuthInteractor() {
        injector.inject(this);
    }

    public void login(IAuthCallback callback, LoginRequest request) {
        LoginEvent event = new LoginEvent();
        event.setCallback(callback);
        event.setRequest(request);

        Call<LoginResponse> call = authApi.login(request);

        LoginResponse response = callRequest(call, event);

        if (response != null && response.getSessionKey() != null) {
            settings.setSessionKey(response.getSessionKey());
        }
    }

    public void register(IAuthCallback callback, LoginUserRegistrationRequest request) {
        RegistrationEvent event = new RegistrationEvent();
        event.setCallback(callback);
        event.setRequest(request);

        Call<UserRegistrationResponse> call = authApi.register(request);

        callRequest(call, event);
    }

    private <T, E extends hu.bme.aut.adapted.framework.model.Response>
    E callRequest(Call<E> call, AuthNetworkEvent<T, E> event) {
        Response<E> httpResponse;
        E response = null;

        try {
            httpResponse = call.execute();
            response = httpResponse.body();
            event.setResponse(response);
            event.setCode(httpResponse.code());
            checkSuccess(httpResponse);
        } catch (Exception e) {
            event.setThrowable(e);
        }

        bus.post(event);
        return response;
    }

    private <T extends hu.bme.aut.adapted.framework.model.Response> void checkSuccess(
            Response<T> response) {
        if (response.body() instanceof LoginResponse &&
                ((LoginResponse) response.body()).getSessionKey() == null) {
            throw new NoSessionKeyException();
        } else if (!response.body().isSucceeded()) {
            throw new UnsuccessfulRequestException(response.body().getMessage());
        } else if (response.code() != 200) {
            throw new UnexpectedErrorException();
        }
    }
}

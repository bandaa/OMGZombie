package hu.bme.aut.adapted.framework.model.event.sensors.zephyr;

/**
 * Created by Laura on 2016.04.27..
 */
public class HeartSDNNGameEvent {
    double sdnnResult;

    public HeartSDNNGameEvent(double res){
        sdnnResult=res;
        android.util.Log.d("RR_INTERVAL","SDNN event created");
    }

    public double getSdnnResult() {
        return sdnnResult;
    }
}

package hu.bme.aut.adapted.framework.sensors.zephyr;

import android.os.Handler;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import hu.bme.aut.adapted.framework.model.event.sensors.zephyr.HeartRateChangedGameEvent;
import hu.bme.aut.adapted.framework.service.event.FrameworkEvent;
import hu.bme.aut.adapted.framework.service.event.MessageEvent;
import zephyr.android.HxMBT.BTClient;
import zephyr.android.HxMBT.ConnectListenerImpl;
import zephyr.android.HxMBT.ConnectedEvent;
import zephyr.android.HxMBT.ZephyrPacketArgs;
import zephyr.android.HxMBT.ZephyrProtocol;

import static hu.bme.aut.adapted.commonlib.util.Constants.Broadcast.Sensors.Status.ONLINE;
import static hu.bme.aut.adapted.framework.FrameworkApplication.injector;

public class HxMConnectedListener extends ConnectListenerImpl {
    private static final String LOG_TAG = "HxMConnectedListener";

    private static final int HR_SPD_DIST_PACKET = 0x26;

    @Inject
    EventBus bus;

    private HxMSensor sensor;

    private IRRIntervalCalculator irrIntervalCalculator;

    private HRSpeedDistPacketInfo packetInfo;

    public HxMConnectedListener(HxMSensor sensor, Handler handler) {
        super(handler, null);
        this.sensor = sensor;
        this.irrIntervalCalculator = new RRIntervalCalculator();
        packetInfo = new HRSpeedDistPacketInfo();
        injector.inject(this);
    }

    public IRRIntervalCalculator getIrrIntervalCalculator() {
        return irrIntervalCalculator;
    }

    @Override
    public void Connected(ConnectedEvent<BTClient> eventArgs) {
        Log.d(LOG_TAG, "Connected to the device");
        ZephyrProtocol protocol = new ZephyrProtocol(eventArgs.getSource().getComms());

        sensor.setStatus(ONLINE);
        sensor.sendStatus();

        protocol.addZephyrPacketEventListener(arg0 -> {
            long timestamp = System.currentTimeMillis();

            ZephyrPacketArgs packetArgs = arg0.getPacket();
            if (HR_SPD_DIST_PACKET == packetArgs.getMsgID()) {

                byte[] packetBytes = packetArgs.getBytes();

                int heartRate = packetInfo.GetHeartRate(packetBytes);
                double instantSpeed = packetInfo.GetInstantSpeed(packetBytes);
                byte heartBeatNumber = packetInfo.GetHeartBeatNum(packetBytes);
                int[] heartBeatTsArray = packetInfo.GetHeartBeatTS(packetBytes);
                int battery = packetInfo.GetBatteryChargeInd(packetBytes);
                Log.d(LOG_TAG, "Heart Rate is " + heartRate);
                Log.d(LOG_TAG, "Instant Speed is " + instantSpeed);
                Log.d(LOG_TAG, "HeartBeat number is " + heartBeatNumber);
                Log.d(LOG_TAG, String.format("HeartBeatTs first: %d, last: %d", heartBeatTsArray[0], heartBeatTsArray[heartBeatTsArray.length - 1]));
                Log.d(LOG_TAG, "Battery level is " + battery);

                irrIntervalCalculator.onNewPacket(heartRate, heartBeatNumber, heartBeatTsArray, timestamp);

                HeartRateChangedGameEvent hrcge = new HeartRateChangedGameEvent(heartRate, 0);
                bus.post(new FrameworkEvent(hrcge, timestamp, false));
                bus.post(new MessageEvent(g -> g.sendHeartRate(heartRate)));
            }
        });
    }
}
